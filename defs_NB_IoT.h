#ifndef __MAC_DEFS_NB_IoT_H__
#define __MAC_DEFS_NB_IoT_H__

#include <stdio.h>
#include <stdlib.h>

#include "RRC/config.h"
#include "RLC/RLC.h"

#define TIMING_GENERATOR
//#undef TIMING_GENERATOR

//#define sim_end_time 10485760 // subframe, 10,485,750 is maximum time
#define sim_end_time 1024	//	subframe 0-40,959
#define SIMULATION_TEST	1	// For simulation, set to 1
#if SIMULATION_TEST==1
// For getting throughput
uint32_t throughput_UL;
uint32_t throughput_DL;
//the ending time for calculating throughput
uint32_t endtime_throughput;

uint32_t utilization_UL;
//uint is 1 subframe * 1 subcarrier
uint32_t num_occupied_ul_resource;
uint32_t num_ul_resource;
uint32_t num_occupied_dlsf_subframe;
uint32_t num_dlsf_subframe;
uint32_t utilization_DL;
#endif
//	MAC definition
//#define MAX_FRAME 0xfffff		//	0-fffff = 1048576
//#define MAX_SUBFRAME 10485760
#define MAX_FRAME 0xfff
#define MAX_SUBFRAME 40960

#define MAX(a, b) (((a)>(b))?(a):(b))

//	RNTI
#define P_RNTI			0xffee
#define SI_RNTI			0xffff
//	RA-RNTI: 1+SFN_id>>2
#define RA_RNTI_LOW		0x0001	//	SFN_id = 0
#define RA_RNTI_HIGH	0x0100	//  SFN_id = 1023
#define C_RNTI_LOW	0x0101
#define C_RNTI_HIGH

// ULSCH LCHAN IDs
/*!\brief LCID of extended power headroom for ULSCH */
#define EXTENDED_POWER_HEADROOM 25
/*!\brief LCID of power headroom for ULSCH */
#define POWER_HEADROOM 26
/*!\brief LCID of CRNTI for ULSCH */
#define CRNTI 27
/*!\brief LCID of truncated BSR for ULSCH */
#define TRUNCATED_BSR 28
/*!\brief LCID of short BSR for ULSCH */
#define SHORT_BSR 29
/*!\brief LCID of long BSR for ULSCH */
#define LONG_BSR 30
/*! \brief Values of CCCH LCID for DLSCH */ 
#define CCCH_LCHANID 0
/*!\brief Values of BCCH logical channel */
#define BCCH 3  // SI 
/*!\brief Values of PCCH logical channel */
#define PCCH 4  // Paging 
/*!\brief Value of CCCH / SRB0 logical channel */
#define CCCH 0  // srb0
/*!\brief DCCH / SRB1 logical channel */
#define DCCH 1  // srb1
/*!\brief DCCH1 / SRB2  logical channel */
#define DCCH1 2 // srb2
/*!\brief DTCH DRB1  logical channel */
#define DTCH 3 // LCID
/*!\brief MCCH logical channel */
#define MCCH 4 
/*!\brief MTCH logical channel */
#define MTCH 1 
// DLSCH LCHAN ID
/*!\brief LCID of UE contention resolution identity for DLSCH*/
#define UE_CONT_RES 28
/*!\brief LCID of timing advance for DLSCH */
#define TIMING_ADV_CMD 29
/*!\brief LCID of discontinous reception mode for DLSCH */
#define DRX_CMD 30
/*!\brief LCID of padding LCID for DLSCH */
#define SHORT_PADDING 31

//Type definition in OAI
typedef unsigned int	uint32_t;
typedef unsigned short	uint16_t;
typedef unsigned char	uint8_t;
typedef signed int		int32_t;
typedef signed short	int16_t;
typedef signed char		int8_t;
typedef uint16_t        rnti_t;
typedef signed char     boolean_t;
typedef uint8_t         module_id_t;
typedef uint32_t        frame_t;
typedef uint32_t        sub_frame_t;
typedef unsigned int    rb_id_t;
typedef uint32_t				tb_size_t;
typedef signed char			MBMS_flag_t;
typedef uint8_t					eNB_index_t;
typedef boolean_t       eNB_flag_t;
typedef uint32_t 				logical_chan_id_t;
typedef uint8_t         mib_flag_t ;

typedef enum tone_type_e
{
  sixtone = 0,
  threetone,
  singletone1,
  singletone2,
  singletone3
}tone_type_t;

typedef enum channel_NB_IoT_e
{
	NPDCCH = 0,
	NPUSCH,
	NPDSCH
}channel_NB_IoT_t;

typedef enum{
	UL = 0,
	DL
}message_direction_t;


#define MAX_NUMBER_OF_UE_MAX_NB_IoT 20
//#define SCH_PAYLOAD_SIZE_MAX_NB_IoT 128
#define SCH_PAYLOAD_SIZE_MAX_NB_IoT 320							//_NB_IoT
#define MAX_NUMBER_OF_SIBs_NB_IoT 16

/*!\brief Values of BCCH0 logical channel for MIB*/
#define BCCH0_NB_IoT 11 // MIB-NB_IoT
/*!\brief Values of BCCH1 logical channel for SIBs */
#define BCCH1_NB_IoT 12 // SI-SIB-NB_IoTs
/*!\brief Values of PCCH logical channel */
#define PCCH_NB_IoT 13  // Paging XXX not used for the moment
/*!\brief Value of CCCH / SRB0 logical channel */
#define CCCH_NB_IoT 0  // srb0 ---> XXX exactly the same as in LTE (commented for compilation purposes)
/*!\brief DCCH0 / SRB1bis logical channel */
#define DCCH0_NB_IoT 3  // srb1bis
/*!\brief DCCH1 / SRB1  logical channel */
#define DCCH1_NB_IoT 1 // srb1 //XXX we redefine it for the SRB1
/*!\brief DTCH0 DRB0  logical channel */
#define DTCH0_NB_IoT 4 // DRB0
/*!\brief DTCH1 DRB1  logical channel */
#define DTCH1_NB_IoT 5 // DRB1
/*Index of UE contention resoulution logical channel*/
#define UE_CONTENTION_RESOLUTION 28
/*Index of TIMING_ADVANCE logical channel*/
#define TIMING_ADVANCE 29
/*Index of DRX_COMMAND logical channel*/
#define DRX_COMMAND 30
/*Index of PADDING logical channel*/
#define PADDING 31





///  DCI Format Type 0 (180 kHz, 23 bits)
typedef struct DCIFormatN0{
  /// type = 0 => DCI Format N0, type = 1 => DCI Format N1, 1 bits
  uint8_t type;
  /// Subcarrier indication, 6 bits
  uint8_t scind;
  /// Resourse Assignment (RU Assignment), 3 bits
  uint8_t ResAssign;
  /// Modulation and Coding Scheme, 4 bits
  uint8_t mcs;
  /// New Data Indicator, 1 bits
  uint8_t ndi;
  /// Scheduling Delay, 2 bits
  uint8_t Scheddly;
  /// Repetition Number, 3 bits
  uint8_t RepNum;
  /// Redundancy version for HARQ (only use 0 and 2), 1 bits
  uint8_t rv;
  /// DCI subframe repetition Number, 2 bits
  uint8_t DCIRep;
}DCIFormatN0_t;

///  DCI Format Type N1
typedef struct DCIFormatN1{
  /// type = 0 => DCI Format N0, type = 1 => DCI Format N1,1bits
  uint8_t type;
  //NPDCCH order indicator (set to 0), 1 bits
  uint8_t orderIndicator;
  // Scheduling Delay,3 bits
  uint8_t Scheddly;
  // Resourse Assignment (RU Assignment),3 bits
  uint8_t ResAssign;
  // Modulation and Coding Scheme,4 bits
  uint8_t mcs;
  // Repetition Number,4 bits
  uint8_t RepNum;
  // New Data Indicator,1 bits
  uint8_t ndi;
  // HARQ-ACK resource,4 bits
  uint8_t HARQackRes;
  // DCI subframe repetition Number,2 bits
  uint8_t DCIRep;
}DCIFormatN1_t;


/// NPRACH-ParametersList-NB_IoT-r13 from 36.331 RRC spec
typedef struct NPRACH_Parameters_NB_IoT{

    /// the period time for nprach
    int nprach_Periodicity;
    /// for the start time for the NPRACH resource from 40ms-2560ms
    int nprach_StartTime;
    /// for the subcarrier of set to the NPRACH preamble from n0 - n34
    int nprach_SubcarrierOffset;
    ///number of subcarriers in a NPRACH resource allowed values (n12,n24,n36,n48)
    int nprach_NumSubcarriers;
    /// where is the region that in NPRACH resource to indicate if this UE support MSG3 for multi-tone or not. from 0 - 1
    int nprach_SubcarrierMSG3_RangeStart;
    /// The max preamble transmission attempt for the CE level from 1 - 128
    int maxNumPreambleAttemptCE;
    /// Number of NPRACH repetitions per attempt for each NPRACH resource
    int numRepetitionsPerPreambleAttempt;
    /// The number of the repetition for DCI use in RAR/MSG3/MSG4 from 1 - 2048 (Rmax)
    int npdcch_NumRepetitions_RA;
    /// Starting subframe for NPDCCH Common searching space for (RAR/MSG3/MSG4)
    int npdcch_StartSF_CSS_RA;
    /// Fractional period offset of starting subframe for NPDCCH common search space
    int npdcch_Offset_RA;

} nprach_parameters_NB_IoT_t;

/*! \brief Downlink SCH PDU Structure */
typedef struct {
  uint8_t payload[SCH_PAYLOAD_SIZE_MAX_NB_IoT];
  uint32_t pdu_size;
} __attribute__ ((__packed__)) DLSCH_PDU_NB_IoT;

/*! \brief eNB template for UE context information  */
typedef struct UE_TEMPLATE_NB_IoT_s{
	// C-RNTI of UE
	rnti_t rnti;
	// UE CE level
	int CE_level;
	// Direction of transmission(DL:0\UL:1\NONE:-1)
	int32_t direction;
	// DCI Reptition
	uint32_t R_dci;
	// MAX repetition
	uint32_t R_max;

	// HARQ round
	uint32_t HARQ_round;
	/*Downlink information*/

	/// DLSCH pdu
	DLSCH_PDU_NB_IoT DLSCH_pdu;
	// PDU size
	uint32_t DLSCH_pdu_size;
	// Data Reptition
	uint32_t R_dl;
	// MCS index
	uint32_t I_mcs_dl;
	// total downlink buffer DCCH0_NB_IoT
	uint32_t dl_buffer_DCCH0_NB_IoT;
	// NDI
	int oldNDI_DL;
	//HARQ ACK/NACK repetition
	uint32_t R_harq;

	/*Uplink information*/
	int oldNDI_UL;
	// Uplink data repeat, now just follow the rach repeat number
	uint32_t R_ul;
	// PHR value (0-3)
	uint32_t PHR;
	// The uplink data size from BSR or DVI
	uint32_t ul_total_buffer;
	// Determine if this UE support multi-tone transmission or not
	int multi_tone;
#if SIMULATION_TEST==1
	// the allocated DL RLC data size
	uint16_t allocated_data_size_dl;
	// the allocated UL RLC data size
	uint16_t allocated_data_size_ul;
#endif
	// Next UE_template ID
  int next;
  // Previous UE_template ID
  int prev;
  // MSG4 complete
  int RRC_connected;
  uint8_t flag_schedule_success;
  // UE active flag
  boolean_t active;
}UE_TEMPLATE_NB_IoT;											//_t delete

typedef struct available_resource_UL_s{

    ///Resource start subframe
    uint32_t start_subframe;
    ///Resource end subframe
    uint32_t end_subframe;
    // pointer to next node
    struct available_resource_UL_s *next, *prev;

}available_resource_UL_t;

typedef struct available_resource_DL_s{
	uint32_t start_subframe;
	uint32_t end_subframe;
	//uint32_t DLSF_num;

	struct available_resource_DL_s *next, *prev;
}available_resource_DL_t;

/*Structure used for scheduling*/
typedef struct{
	//resource position info.
	uint32_t sf_end,sf_start;
	//resource position info. separate by HyperSF, Frame, Subframe
	uint32_t start_h, end_h;
	uint32_t start_f, end_f;
	uint32_t start_sf, end_sf;
	//whcih available resource node is used
	available_resource_DL_t *node;
}sched_temp_DL_NB_IoT_t;

/*Structure used for UL scheduling*/
typedef struct{
	//resource position info.
	uint32_t sf_end, sf_start;
	//resource position info. separate by HyperSF, Frame, Subframe
	//uint32_t start_h, end_h;
	//uint32_t start_f, end_f;
	//uint32_t start_sf, end_sf;
	// information for allocating the resource
	int tone;
	int scheduling_delay;
	int subcarrier_indication;
	int ACK_NACK_resource_field;
	available_resource_UL_t *node;
}sched_temp_UL_NB_IoT_t;

/******Update******/
/*** the value of variable in this structure is able to be changed in Preprocessor**/
typedef struct{
	
	uint16_t TBS;
	uint8_t index_tbs;
	uint32_t total_sdu_size;
	//Repetition
	uint16_t R_dci;
	uint16_t R_dl_data;
	uint16_t R_dl_harq;
	uint16_t R_ul_data;
	//DL relative
	uint8_t dci_n1_index_R_dci;
	uint8_t dci_n1_index_R_data;
	uint8_t dci_n1_index_mcs;
	uint8_t dci_n1_index_sf;
	uint8_t dci_n1_index_delay;
	uint8_t dci_n1_index_ack_nack;
	uint32_t dci_n1_n_sf;
	//UL relative
	uint8_t dci_n0_index_R_dci;
	uint8_t dci_n0_index_R_data;
	uint8_t dci_n0_index_mcs;
	uint8_t dci_n0_index_ru;
	uint8_t dci_n0_index_delay;
	uint8_t dci_n0_index_subcarrier;
	uint32_t dci_n0_n_ru;
	uint8_t dci_n0_index_rv;
	uint8_t dci_n0_index_ndi;
	// byte
	uint16_t total_data_size_dl;
	// byte
	uint16_t total_data_size_ul;
	//0:UL 1:DL 2:both
	uint16_t transmit_direction;
	/*resource allocation*/
	// scheduling result for NPDCCH
	uint32_t NPDCCH_sf_end, NPDCCH_sf_start;
	// scheduling result for USS DL
	uint32_t NPDSCH_sf_end, NPDSCH_sf_start;
	uint32_t HARQ_sf_end, HARQ_sf_start;
	// scheduling result for USS UL
	uint32_t NPUSCH_sf_end, NPUSCH_sf_start;
	// schedule success 1 failure 0
	uint8_t flag_schedule_success;
}UE_SCHED_CTRL_NB_IoT_t;


/*****end****/

/*36331 NPDCCH-ConfigDedicated-NB_IoT*/
typedef struct{
	//npdcch-NumRepetitions-r13
	uint32_t R_max;
	//npdcch-StartSF-CSS-r13
	double G;
	//npdcch-Offset-USS-r13
	double a_offset;
	//NPDCCH period
	uint32_t T;
	//Starting subfrane of Search Space which is mod T
	uint32_t ss_start_css;
}NPDCCH_config_common_NB_IoT_t;

/*36331 NPDCCH-ConfigDedicated-NB_IoT*/
typedef struct{
	//npdcch-NumRepetitions-r13
	uint32_t R_max;
	//npdcch-StartSF-USS-r13
	double G;
	//npdcch-Offset-USS-r13
	double a_offset;
	//NPDCCH period
	uint32_t T;
	//Starting subfrane of Search Space which is mod T
	uint32_t ss_start_uss;
}NPDCCH_config_dedicated_NB_IoT_t;

/*! \brief UE list used by eNB to order UEs for scheduling*/
typedef struct{
	/// DCI template and MAC connection parameters for UEs
	UE_TEMPLATE_NB_IoT UE_template_NB_IoT[MAX_NUMBER_OF_UE_MAX_NB_IoT];
	/// UE information during scheduling
	UE_SCHED_CTRL_NB_IoT_t UE_sched_ctrl_NB_IoT[MAX_NUMBER_OF_UE_MAX_NB_IoT];
	/// NPDCCH Period and searching space info
	NPDCCH_config_dedicated_NB_IoT_t NPDCCH_config_dedicated;
	//int next[MAX_NUMBER_OF_UE_MAX_NB_IoT];
	// -1:No UE in list
	int head;
	// -1:No UE in list
	int tail;
	int num_UEs;
	//boolean_t active[MAX_NUMBER_OF_UE_MAX_NB_IoT];
}UE_list_NB_IoT_t;

typedef struct{

	// flag to indicate scheduing MIB-NB_IoT
	uint8_t flag_MIB;
	// flag to indicate scheduling SIB1-NB_IoT
	uint8_t flag_SIB1;
	// flag to indicate scheduling SIBs-NB_IoT
	uint8_t flag_SIBs[MAX_NUMBER_OF_SIBs_NB_IoT];
	// flag to indicate scheduling type2 NPDCCH CSS with different CE level
	uint8_t flag_type2_css[3];
	// flag to indicate scheduling type1 NPDCCH CSS with different CE level
	uint8_t flag_type1_css[3];
	// flag to indicate scheduling NPDCCH USS with UE list
	uint8_t flag_uss[MAX_NUMBER_OF_UE_MAX_NB_IoT];
	// flag to indicate scheduling sib1/MIB
	uint8_t flag_fix_scheduling;
	// number of the type2 css to schedule in this period
	uint8_t num_type2_css_run;
	// number of the type1 css to schedule in this period
	uint8_t num_type1_css_run;
	// number of the uss to schedule in this period
	uint8_t num_uss_run;

}scheduling_flag_t;





/*!\brief  MAC subheader short with 7bit Length field */
typedef struct {
  uint8_t LCID:5;  // octet 1 LSB
  uint8_t E:1;
  uint8_t F2:1;
  uint8_t R:1;     // octet 1 MSB
  uint8_t L:7;     // octet 2 LSB
  uint8_t F:1;     // octet 2 MSB
} __attribute__((__packed__))SCH_SUBHEADER_SHORT;							//_NB-IoT
typedef struct {
  uint8_t LCID:5;   // octet 1 LSB
  uint8_t E:1;
  uint8_t F2:1;
  uint8_t R:1;      // octet 1 MSB
  uint8_t L_MSB:7;
  uint8_t F:1;      // octet 2 MSB
  uint8_t L_LSB:8;
} __attribute__((__packed__))SCH_SUBHEADER_LONG;							//_NB-IoT
typedef struct {
  uint8_t LCID:5;   // octet 1 LSB
  uint8_t E:1;
  uint8_t F2:1;
  uint8_t R:1;      // octet 1 MSB
  uint8_t L_MSB:8;      // octet 2 MSB
  uint8_t L_LSB:8;
} __attribute__((__packed__))SCH_SUBHEADER_LONG_EXTEND;							//_NB-IoT
/*!\brief MAC subheader short without length field */
typedef struct {
  uint8_t LCID:5;
  uint8_t F2:1;
  uint8_t E:1;
  uint8_t R:1;
} __attribute__((__packed__))SCH_SUBHEADER_FIXED;							//_NB-IoT


/*! \brief Uplink SCH PDU Structure */
typedef struct {
  int8_t payload[SCH_PAYLOAD_SIZE_MAX_NB_IoT];         /*!< \brief SACH payload */
  uint16_t Pdu_size;
} __attribute__ ((__packed__)) ULSCH_PDU;

typedef struct {
  uint8_t PH:6;
  uint8_t R:2;
} __attribute__((__packed__))POWER_HEADROOM_CMD;

typedef struct {
  uint8_t RAPID:6;
  uint8_t T:1;
  uint8_t E:1;
} __attribute__((__packed__))RA_HEADER_RAPID;



typedef struct Available_available_resource_DL{

    ///Available Resoruce for sixtone
    available_resource_UL_t *sixtone_Head;//, *sixtone_npusch_frame;
	uint32_t sixtone_end_subframe;
    ///Available Resoruce for threetone
    available_resource_UL_t *threetone_Head;//, *threetone_npusch_frame;
	uint32_t threetone_end_subframe;
    ///Available Resoruce for singletone1
    available_resource_UL_t *singletone1_Head;//, *singletone1_npusch_frame;
	uint32_t singletone1_end_subframe;
    ///Available Resoruce for singletone2
    available_resource_UL_t *singletone2_Head;//, *singletone2_npusch_frame;
    uint32_t singletone2_end_subframe;
	///Available Resoruce for singletone3
    available_resource_UL_t *singletone3_Head;//, *singletone3_npusch_frame;
	uint32_t singletone3_end_subframe;
	
}available_resource_tones_UL_t;

typedef struct schedule_result{
	// The subframe read by output handler
	uint32_t output_subframe;
	// SDU length
	uint32_t sdu_length;
	// MAC PDU
	uint8_t *DLSCH_pdu;
	// The data direction indicated by this DCI
	uint8_t direction;
	// pointer to DCI
	void *DCI_pdu;															//DCI_pdu
	// when all the procedure related to this DCI, enable this flag
	boolean_t DCI_release;
	// Indicate the channel which to transmit
	channel_NB_IoT_t channel;
	// rnti
	rnti_t rnti;
	// 0 = TC-RNTI , 1 = RA-RNTI, 2 = P-RNTI, 3 = others
	uint8_t rnti_type;
	// 0 = data, 1 = ACK/NACK
	uint8_t npusch_format;
	//HARQ ACK/NACK repetition
	uint32_t R_harq;
	// pointer to next node
	struct schedule_result *next;

	uint32_t end_subframe;
	
	uint8_t *rar_buffer;
	
	uint8_t *debug_str;

	int16_t dl_sdly;
	int16_t ul_sdly;
	int16_t num_sf;
	
	//	for demo
	int16_t harq_round;
}schedule_result_t;

/*Flag structure used for trigger each scheduler*/
typedef struct{
	scheduling_flag_t scheduling_flag;
	//sched_temp_DL_NB_IoT_t sched_result_DL;
	//resource grid for Uplink
	available_resource_tones_UL_t *UL_resource;
	//scheduling result read by output handler
	schedule_result_t *schedule_result_list_UL;
	schedule_result_t *schedule_result_list_DL;
}SCHEDULE_NB_IoT_t;

typedef struct{
  uint32_t num_dlsf_per_period;
  uint16_t *sf_to_dlsf_table;
  uint16_t *dlsf_to_sf_table;
}DLSF_INFO_t;

typedef enum ce_level_e{
	ce0=0,
	ce1,
	ce2,
	ce_level_total
}ce_level_t;

typedef struct RA_template_s{
	boolean_t active;
	uint32_t msg3_retransmit_count;
	uint32_t msg4_retransmit_count;
	uint16_t ta;
	uint8_t preamble_index;
	ce_level_t ce_level;
	rnti_t ue_rnti;
	rnti_t ra_rnti;
	struct RA_template_s *next, *prev;
	boolean_t wait_msg4_ack;
	boolean_t wait_msg3_ack;
	uint8_t rar_buffer[7];
}RA_template_NB_IoT;							//_NB_IoT delete t

typedef struct simulate_rx_s{
    uint32_t rx_abs_subframe;
    rnti_t rnti;
    struct simulate_rx_s *next;
}simulate_rx_t;

typedef struct RA_template_list_s{
	RA_template_NB_IoT *head;
	RA_template_NB_IoT *tail;
}RA_template_list_t;





typedef struct mac_NB_IoT_s{
	//	main structure

	//	System
	uint32_t hyper_system_frame;
	uint32_t system_frame;
	uint32_t sub_frame;

	uint32_t current_subframe;

	//	RA
	RA_template_list_t RA_msg2_list;
	RA_template_list_t RA_msg3_list;
	RA_template_list_t RA_msg4_list;

	RA_template_NB_IoT RA_template[MAX_NUMBER_OF_UE_MAX_NB_IoT];

	

	//int32_t last_tx_subframe;

	//	for tool
	int32_t sib1_flag[64];
	int32_t sib1_count[64];
	int32_t sib1_period;
	uint16_t dlsf_table[64];
	int32_t sibs_table[256];

	//	channel config

  //USS list
	//Number of USS period is used
	int num_uss_list;
  UE_list_NB_IoT_t *UE_list_spec;

  scheduling_flag_t scheduling_flag;

	uint32_t schedule_subframe_DL;
	//uint32_t schedule_subframe_UL;
	NPDCCH_config_common_NB_IoT_t npdcch_config_common[3];
    rrc_config_NB_IoT_t rrc_config;

	FILE *schedule_result;
    FILE *schedule_log;
    FILE *schedule_log_sim;
}eNB_MAC_INST_NB_IoT;							//eNB_MAC_INST_NB_IoT

// global variables

nprach_parameters_NB_IoT_t nprach_list[3];

//SCHEDULE_NB_IoT_t *NB_IoT_schedule;

/******MAC Global Variable********/
available_resource_tones_UL_t *available_resource_UL;
available_resource_DL_t *available_resource_DL;

/*
 schedule_result_t *schedule_result_list_UL;
 schedule_result_t *schedule_result_list_DL;
*/
//DLSF Table
DLSF_INFO_t DLSF_information;



// 10 -> single-tone / 12 -> multi-tone
static uint32_t max_mcs[2] = {10, 12};

// [CE level] [0 - 3] -> single-tone / [CE level] [4-7] -> multi-tone
static uint32_t mapped_mcs[3][8]={{1,5,9,10,3,7,11,12},
                            {0,3,7,10,3,7,11,12},
                            {0,2,6,10,0,4,8,12}};

//TBS table for NPUSCH transmission TS 36.213 v14.2 table Table 16.5.1.2-2:
static int UL_TBS_Table[14][8]=
{
	{16,2,56,88,120,152,208,256},
	{24,56,88,144,176,208,256,344},
	{32,72,144,176,208,256,328,424},
	{40,104,176,208,256,328,440,568},
	{56,120,208,256,328,408,552,680},
	{72,144,224,328,424,504,680,872},
	{88,176,256,392,504,600,808,1000},
	{104,224,328,472,584,712,1000,1224},
	{120,256,392,536,680,808,1096,1384},
	{136,296,456,616,776,936,1256,1544},
	{144,328,504,680,872,1000,1384,1736},
	{176,376,584,776,1000,1192,1608,2024},
	{208,440,680,1000,1128,1352,1800,2280},
	{224,488,744,1128,1256,1544,2024,2536}
};

static uint32_t RU_table[8]={1,2,3,4,5,6,8,10};

static uint32_t scheduling_delay[4]={8,16,32,64};
static uint32_t msg3_scheduling_delay_table[4] = {12,16,32,64};

static uint32_t ack_nack_delay[4]={13,15,17,18};

static uint32_t R_dl_table[16]={1,2,4,8,16,32,64,128,192,256,384,512,768,1024,1536,2048};

//Prach parameters
static int rachperiod[8]={40,80,160,240,320,640,1280,2560};
static int rachstart[8]={8,16,32,64,128,256,512,1024};
static int rachrepeat[8]={1,2,4,8,16,32,64,128};
static int rawindow[8]={2,3,4,5,6,7,8,10}; // unit PP
static int rmax[12]={1,2,4,8,16,32,64,128,256,512,1024,2048};
static double gvalue[8]={1.5,2,4,8,16,32,48,64};
static int candidate[4]={1,2,4,8};
static double pdcchoffset[4]={0,0.125,0.25,0.375};
static int dlrepeat[16]={1,2,4,8,16,32,64,128,192,256,384,512,768,1024,1536,2048};
static int rachscofst[7]={0,12,24,36,2,18,34};
static int rachnumsc[4]={12,24,36,48};

// NB_IoT-IoT------------------

// TBS table for the case not containing SIB1-NB_IoT, Table 16.4.1.5.1-1 in TS 36.213 v14.2
static uint32_t TBStable_NB_IoT[14][8] ={ //[ITBS][ISF]
  {16,32,56,88,120.152,208,256},
  {24,56,88,144,176,208,256,344},
  {32,72,144,176,208,256,328,424},
  {40,104,176,208,256,328,440,568},
  {56,120,208,256,328,408,552,680},
  {72,144,244,328,424,504,680,872},
  {88,176,256,392,504,600,808,1032},
  {104,224,328,472,584,680,968,1224},
  {120,256,392,536,680,808,1096,1352},
  {136,296,456,616,776,936,1256,1544},
  {144,328,504,680,872,1032,1384,1736},
  {176,376,584,776,1000,1192,1608,2024},
  {208,440,680,904,1128,1352,1800,2280},
  {224,488,744,1128,1256,1544,2024,2536}
};

//TBS table for the case containing S1B1-NB_IoT, Table 16.4.1.5.2-1 in TS 36.213 v14.2 (Itbs = 12 ~ 15 is reserved field
//mapping ITBS to SIB1-NB_IoT
static unsigned int TBStable_NB_IoT_SIB1[16] = {208,208,208,328,328,328,440,440,440,680,680,680};

static int DV_table[16]={0,10,14,19,26,36,49,67,91,125,171,234,321,768,1500,1500};

static int BSR_table[64]= {0,10,12,14,17,19,22,26,31,36,42,49,57,67,78,91,
                           105,125,146,171,200,234,274,321,376,440,515,603,706,826,967,1132,
                           1326,1552,1817,2127,2490,2915,3413,3995,4677,5467,6411,7505,8787,10287,12043,14099,
                           16507,19325,22624,26487,31009,36304,42502,49759,58255,68201,79846,93479,109439,128125,150000,300000
                           };

static int dl_rep[3] = {1, 2, 4};
static uint32_t dci_rep[3] = {1, 2, 4};
static uint32_t harq_rep[3] = {1, 2, 4};
#endif


