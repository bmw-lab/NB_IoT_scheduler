#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "../defs_NB_IoT.h"
#include "../proto_NB_IoT.h"
#include "../debug.h"

extern nprach_parameters_NB_IoT_t nprach_list[3];

//SCHEDULE_NB_IoT_t *NB_IoT_schedule;

extern available_resource_tones_UL_t *available_resource_UL;

extern schedule_result_t *schedule_result_list_UL;
extern schedule_result_t *schedule_result_list_DL;
extern UE_list_NB_IoT_t *UE_list_spec;
/*
char str20[] = "UL_Data";
char str21[] = "DCI_N0";
*/
int schedule_UL_NB_IoT(eNB_MAC_INST_NB_IoT *mac_inst,UE_TEMPLATE_NB_IoT *UE_info,uint32_t subframe, uint32_t frame, uint32_t H_SFN, UE_SCHED_CTRL_NB_IoT_t *UE_sched_ctrl_info){

    int i,ndi = 0,check_DCI_result = 0,check_UL_result = 0,candidate;
    uint32_t DL_end;
    //Scheduling resource temp buffer
    sched_temp_DL_NB_IoT_t *NPDCCH_info = (sched_temp_DL_NB_IoT_t*)malloc(sizeof(sched_temp_DL_NB_IoT_t));

    candidate = UE_info->R_max/UE_sched_ctrl_info->R_dci;
    uint32_t mcs = max_mcs[UE_info->multi_tone];
    uint32_t mappedMcsIndex=UE_info->PHR+(4 * UE_info->multi_tone);
    int TBS = 0;
    int Iru = 0, Nru, I_rep,N_rep,total_ru;
    int dly = 0,uplink_time = 0;

    if(UE_info->ul_total_buffer<=0)
    {
        DEBUG("[%04d][ULSchedulerUSS][UE:%05d] No UL data in buffer\n", mac_inst->current_subframe, UE_info->rnti);
        return -1;
    }

    TBS=get_TBS_UL(mcs,UE_info->multi_tone,Iru);
    DEBUG("Initial TBS : %d UL_buffer: %d\n", TBS, UE_info->ul_total_buffer);
    sched_temp_UL_NB_IoT_t *NPUSCH_info = (sched_temp_UL_NB_IoT_t*)malloc(sizeof(sched_temp_UL_NB_IoT_t));

    //DCIFormatN0_t *DCI_N0 = (DCIFormatN0_t*)malloc(sizeof(DCIFormatN0_t));

    available_resource_DL_t *node;

    // setting of the NDI
    /*
    if(UE_info->HARQ_round == 0)
    {
        ndi = 1-UE_info->oldNDI_UL;
        UE_info->oldNDI_UL=ndi;
    }
    */
    for (i = 0; i < candidate; i++)
    {
        /*step 1 : Check DL resource is available for DCI N0 or not*/
        check_DCI_result = check_resource_NPDCCH_NB_IoT(mac_inst,H_SFN, frame, subframe, NPDCCH_info, i, UE_sched_ctrl_info->R_dci);

        //node = check_resource_DL(mac_inst,);

        //just use to check when there is no DL function
        //NPDCCH_info->sf_start = H_SFN*10240+frame*10 +subframe + i * UE_sched_ctrl_info->R_dci;
        //NPDCCH_info->sf_end = NPDCCH_info->sf_start + (i+1) * UE_sched_ctrl_info->R_dci;

        //DEBUG("UE : %5d, NPDCCH result: %d ,NPDCCH start: %d,NPDCCH end : %d\n",UE_info->rnti,check_DCI_result,NPDCCH_info->sf_start,NPDCCH_info->sf_end);
        if( check_DCI_result != -1)
        {
            /*step 2 : Determine MCS / TBS / REP / RU number*/
            /*while((mapped_mcs[UE_info->CE_level][mappedMcsIndex]< mcs)||((TBS>UE_info->ul_total_buffer)&&(mcs>=0)))
                {
                    --mcs;
                    TBS=get_TBS_UL(mcs,UE_info->multi_tone,Iru);
                }*/
            mcs = mapped_mcs[UE_info->CE_level][mappedMcsIndex];

            if(UE_info->HARQ_round==0)
            {
                while((TBS<UE_info->ul_total_buffer)&&(Iru<7))
                {
                    Iru++;
                    TBS=get_TBS_UL(mcs,UE_info->multi_tone,Iru);
                }
            }
            else
            {
                while((TBS<UE_info->allocated_data_size_ul)&&(Iru<7))
                {
                    Iru++;
                    TBS=get_TBS_UL(mcs,UE_info->multi_tone,Iru);
                }
            }
            
            DEBUG("TBS : %d UL_buffer: %d MCS %d I_RU %d\n", TBS, UE_info->ul_total_buffer, mcs, Iru);

            Nru = RU_table[Iru];
            DL_end = NPDCCH_info->sf_end;
            N_rep = get_N_REP(UE_info->CE_level);
            I_rep = get_I_REP(N_rep);
            total_ru = Nru * N_rep;

            DEBUG("[%04d][ULSchedulerUSS][UE:%05d] Multi-tone:%d,MCS:%d,TBS:%d,UL_buffer:%d,DL_start:%d,DL_end:%d,N_rep:%d,N_ru:%d,Total_ru:%d\n", mac_inst->current_subframe,UE_info->rnti,UE_info->multi_tone,mcs,TBS,UE_info->ul_total_buffer,NPDCCH_info->sf_start,DL_end,N_rep,Nru,total_ru);
            /*step 3 Check UL resource for Uplink data*/
            // we will loop the scheduling delay here
            for(dly=0;dly<4;dly++)
            {
                uplink_time = DL_end +scheduling_delay[dly]+1;
                check_UL_result = Check_UL_resource(uplink_time,total_ru, NPUSCH_info, UE_info->multi_tone, 0);
                if (check_UL_result != -1)
                {
                    //DEBUG(L_RED"[%04d][UL scheduler][UE:%05d] DCI content = scind : %d ResAssign : %d mcs : %d ndi : %d scheddly : %d RepNum : %d rv : %d DCIRep : %d\n"NONE, mac_inst->current_subframe,UE_info->rnti,DCI_N0->scind,DCI_N0->ResAssign,DCI_N0->mcs,DCI_N0->ndi,DCI_N0->Scheddly,DCI_N0->RepNum,DCI_N0->rv,DCI_N0->DCIRep);
                    SCHEDULE_LOG("[%04d][ULSchedulerUSS][%d][Success] complete scheduling with data size %d\n", mac_inst->current_subframe, UE_info->rnti, UE_info->ul_total_buffer);
                    SCHEDULE_LOG("[%04d][ULSchedulerUSS][%d] Multi-tone:%d,MCS:%d,TBS:%d,UL_buffer:%d,DL_start:%d,DL_end:%d,N_rep:%d,N_ru:%d,Total_ru:%d\n", mac_inst->current_subframe,UE_info->rnti,UE_info->multi_tone,mcs,TBS,UE_info->ul_total_buffer,NPDCCH_info->sf_start,DL_end,N_rep,Nru,total_ru);
                    //SCHEDULE_LOG("[%04d][ULSchedulerUSS][%d][Success] DCI content = scind : %d ResAssign : %d mcs : %d ndi : %d scheddly : %d RepNum : %d rv : %d DCIRep : %d\n", mac_inst->current_subframe, UE_info->rnti, DCI_N0->scind,DCI_N0->ResAssign,DCI_N0->mcs,DCI_N0->ndi,DCI_N0->Scheddly,DCI_N0->RepNum,DCI_N0->rv,DCI_N0->DCIRep);
                    // step 5 resource allocation and generate scheduling result
                    DEBUG("[%04d][ULSchedulerUSS][UE:%05d] Generate result\n", mac_inst->current_subframe, UE_info->rnti);
                    //generate_scheduling_result_UL(NPDCCH_info->sf_start, NPDCCH_info->sf_end,NPUSCH_info->sf_start, NPUSCH_info->sf_end,DCI_N0,UE_info->rnti, str20, str21);
                    DEBUG("[%04d][ULSchedulerUSS][UE:%05d] Maintain resource\n", mac_inst->current_subframe, UE_info->rnti);
                    maintain_resource_DL(mac_inst,NPDCCH_info,NULL);

                    adjust_UL_resource_list(NPUSCH_info);
                    //Fill result to Output structure
                    UE_sched_ctrl_info->NPDCCH_sf_end=NPDCCH_info->sf_end;
                    UE_sched_ctrl_info->NPDCCH_sf_start=NPDCCH_info->sf_start;
                    UE_sched_ctrl_info->NPUSCH_sf_end=NPUSCH_info->sf_end;
                    UE_sched_ctrl_info->NPUSCH_sf_start=NPUSCH_info->sf_start;
                    UE_sched_ctrl_info->TBS=TBS;
                    UE_sched_ctrl_info->dci_n0_index_mcs=mcs;
                    UE_sched_ctrl_info->index_tbs=mcs;
                    UE_sched_ctrl_info->dci_n0_index_ru=Iru;
                    UE_sched_ctrl_info->dci_n0_n_ru=Nru;
                    UE_sched_ctrl_info->dci_n0_index_delay=dly;
                    UE_sched_ctrl_info->dci_n0_index_subcarrier=NPUSCH_info->subcarrier_indication;
                    UE_sched_ctrl_info->dci_n0_index_ndi=ndi;
                    //UE_sched_ctrl_info->dci_n0_index_R_dci=get_DCI_REP(UE_info->R_dci,UE_info->R_max);
                    UE_sched_ctrl_info->dci_n0_index_R_data=I_rep;
                    
                    //UE_sched_ctrl_info->dci_n0_index_rv=(UE_info->HARQ_round%2==0)?0:1; // rv will loop 0 & 2
#if SIMULATION_TEST==1
                    num_occupied_dlsf_subframe+=UE_sched_ctrl_info->R_dci;
                    if(NPUSCH_info->sf_end>endtime_throughput)
                    {
                        endtime_throughput=NPUSCH_info->sf_end;
                    }

                    if(UE_info->HARQ_round==0)
                    {
                        if(TBS>=UE_info->ul_total_buffer)
                        {
                            UE_sched_ctrl_info->total_data_size_ul=UE_info->ul_total_buffer;
                        }
                        else
                        {
                            UE_sched_ctrl_info->total_data_size_ul=TBS;
                        }
                    }
                    //resource utilization
                    int num_tone, num_subframe_ru;
                    if(11>=UE_sched_ctrl_info->dci_n0_index_subcarrier)
                    {
                        num_tone=1;
                        num_subframe_ru=8;
                    }
                    else if(15>=UE_sched_ctrl_info->dci_n0_index_subcarrier)
                    {
                        num_tone=3;
                        num_subframe_ru=4;
                    }
                    else if(17>=UE_sched_ctrl_info->dci_n0_index_subcarrier)
                    {
                        num_tone=6;
                        num_subframe_ru=2;
                    }
                    else if(18==UE_sched_ctrl_info->dci_n0_index_subcarrier)
                    {
                        num_tone=12;
                        num_subframe_ru=1;
                    }
                    else
                    {
                        num_tone=0;
                    }
                    num_occupied_ul_resource+=num_tone*num_subframe_ru*total_ru;
#endif
                    SCHEDULE_LOG("[%04d][ULSchedulerUSS][%d][Success] Finish UL USS scheduling \n", mac_inst->current_subframe, UE_info->rnti);

                    return 0;
                }
            }
        }
        /*break now, we only loop one candidiate*/
        //break;
    }
    UE_sched_ctrl_info->flag_schedule_success=0;
    SCHEDULE_LOG("[%04d][ULSchedulerUSS][%d][Fail] UL scheduling USS fail\n", mac_inst->current_subframe, UE_info->rnti);
    DEBUG("[%04d][UL scheduler][UE:%05d] there is no available UL resource\n", mac_inst->current_subframe, UE_info->rnti);
    //free(NPUSCH_info);
    //free(NPDCCH_info);
    return -1;
}
void fill_DCI_N0(DCIFormatN0_t *DCI_N0, UE_TEMPLATE_NB_IoT *UE_info, UE_SCHED_CTRL_NB_IoT_t *UE_sched_ctrl_info)
{
    DCI_N0->type = 0;
    DCI_N0->scind = UE_sched_ctrl_info->dci_n0_index_subcarrier;
    DCI_N0->ResAssign = UE_sched_ctrl_info->dci_n0_index_ru;
    DCI_N0->mcs = UE_sched_ctrl_info->dci_n0_index_mcs;
    DCI_N0->ndi = UE_sched_ctrl_info->dci_n0_index_ndi;
    DCI_N0->Scheddly = UE_sched_ctrl_info->dci_n0_index_delay;
    DCI_N0->RepNum = UE_sched_ctrl_info->dci_n0_index_R_data;
    DCI_N0->rv = (UE_info->HARQ_round%2==0)?0:1; // rv will loop 0 & 2
    DCI_N0->DCIRep = get_DCI_REP(UE_sched_ctrl_info->R_dci,UE_info->R_max);
    //DCI_N0->DCIRep = UE_sched_ctrl_info->dci_n0_index_R_dci;
    DEBUG("[fill_DCI_N0] Type %d scind %d I_ru %d I_mcs %d ndi %d I_delay %d I_rep %d RV %d I_dci %d\n", DCI_N0->type, DCI_N0->scind, DCI_N0->ResAssign, DCI_N0->mcs, DCI_N0->ndi, DCI_N0->Scheddly, DCI_N0->RepNum, DCI_N0->rv, DCI_N0->DCIRep);
}
