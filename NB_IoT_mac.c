#include <stdio.h>
#include <stdlib.h>
#include "defs_NB_IoT.h"
#include "debug.h"
#define NUM_USS_PP 3
#define USER_NUM_USS 10

extern schedule_result_t *schedule_result_list_UL;
extern schedule_result_t *schedule_result_list_DL;
extern available_resource_DL_t *available_resource_DL;
available_resource_DL_t *available_resource_DL_last;

void init_mac_NB_IoT(eNB_MAC_INST_NB_IoT *mac_inst, rrc_config_NB_IoT_t *rrc_inst){
	int32_t i, j, k;	
	
	for(i=0;i<64;++i)
	{
		mac_inst->sib1_flag[i] = 0;
		mac_inst->sib1_count[i] = 0;
		//mac_inst->sib1_NB_IoT_sched_config.sib1_flag[i] = 0;
		//mac_inst->sib1_NB_IoT_sched_config.sib1_count[i] = 0;
	}
	
	rrc_mac_config_req(mac_inst, &mac_inst->rrc_config, 1, 1, 0, 0, 0);
	init_tool_sib1(mac_inst);

	//	output handler 
	mac_inst->schedule_result = fopen("out/schedule_result.txt", "w");
    mac_inst->schedule_log = fopen("out/schedule_log.txt", "w");
	//	RA
	mac_inst->RA_msg2_list.head = (RA_template_NB_IoT *)0;
	mac_inst->RA_msg3_list.head = (RA_template_NB_IoT *)0;
	mac_inst->RA_msg4_list.head = (RA_template_NB_IoT *)0;
	mac_inst->RA_msg2_list.tail = (RA_template_NB_IoT *)0;
	mac_inst->RA_msg3_list.tail = (RA_template_NB_IoT *)0;
	mac_inst->RA_msg4_list.tail = (RA_template_NB_IoT *)0;

	sib1_NB_IoT_sched_t *config = &mac_inst->rrc_config.sib1_NB_IoT_sched_config;
	
	// DLSF Table
	init_dlsf_info(mac_inst, &DLSF_information);

	//	init sib1 tool
	//int repetition_pattern = 1;//	1:every2frame, 2:every4frame, 3:every8frame, 4:every16frame
	for(i=0;i<8;++i){
		mac_inst->sib1_flag[(i<<1)+config->starting_rf] = 1;
	}

	for(i=0, j=0;i<64;++i){
		if(mac_inst->sib1_flag[i]==1){
			++j;
		}
		mac_inst->sib1_count[i]=j;
	}
//printf("%d", mac_inst->sib1_period);
	for(i=0, j=0;i<640;++i){
		//printf("*%d", i);
		if(is_dlsf(mac_inst, i)){
			++j;
		}
		//printf("-");
		if(i%10==9){
			mac_inst->dlsf_table[i/10] = j;
		}
	}

	for(i=0;i<256;++i){
		mac_inst->sibs_table[i] = -1;
	}
	for(j=0;j<6;++j){
		if(0x0 != mac_inst->rrc_config.sibs_NB_IoT_sched[j].sib_mapping_info){
			k = mac_inst->rrc_config.sibs_NB_IoT_sched[j].si_periodicity / mac_inst->rrc_config.si_window_length;
			for(i=0;i<(256/k);++i){
				mac_inst->sibs_table[(i*k)+j] = j;
			}
		}
	}

	mac_inst->schedule_subframe_DL = 0;
	//mac_inst->schedule_subframe_UL = 0;

	available_resource_DL = available_resource_DL_last = (available_resource_DL_t *)0;

	//	init downlink list 0-100
	init_dl_list(mac_inst);

	for(i=0; i<MAX_NUMBER_OF_UE_MAX_NB_IoT; ++i){
		mac_inst->RA_template[i].active = 0;
		mac_inst->RA_template[i].msg3_retransmit_count = 0;
		mac_inst->RA_template[i].msg4_retransmit_count = 0;
		mac_inst->RA_template[i].ta = 0;
		mac_inst->RA_template[i].preamble_index = 0;
		mac_inst->RA_template[i].ue_rnti = 0x0;
		mac_inst->RA_template[i].ra_rnti = 0x0;
		mac_inst->RA_template[i].next = (RA_template_NB_IoT *)0;
		mac_inst->RA_template[i].prev = (RA_template_NB_IoT *)0;
		mac_inst->RA_template[i].wait_msg4_ack = 0;
		mac_inst->RA_template[i].wait_msg3_ack = 0;
	}
	
	SCHEDULE_LOG("-------------------Configuration of NPDCCH for CSS scheduling--------------\n");
	//3 CE level USS list

	for(i=0;i<3;++i)
	{
		rrc_mac_config_req(mac_inst, &mac_inst->rrc_config, 0, 0, 1, 0, i);

		SCHEDULE_LOG("[%04d][init_mac_NB_IoT][CE%d] R_max=%d, G=%.1f, a_offset=%.1f\n", mac_inst->current_subframe, i, mac_inst->npdcch_config_common[i].R_max, mac_inst->npdcch_config_common[i].G, mac_inst->npdcch_config_common[i].a_offset);
	}

#if SIMULATION_TEST==0
	SCHEDULE_LOG("-------------------Configuration of NPDCCH for USS scheduling--------------\n");
	//3 CE level USS list
	mac_inst->UE_list_spec = (UE_list_NB_IoT_t*)malloc(NUM_USS_PP*sizeof(UE_list_NB_IoT_t));
	//initial UE list
    printf("[init_mac_NB_IoT] Initial UE list\n");

	mac_inst->num_uss_list = NUM_USS_PP;
	for(i=0;i<NUM_USS_PP;++i)
	{
		rrc_mac_config_req(mac_inst, &mac_inst->rrc_config, 0, 0, 0, 1, i);

		(mac_inst->UE_list_spec+i)->head = -1;
		(mac_inst->UE_list_spec+i)->tail = -1;
		(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max = mac_inst->rrc_config.npdcch_ConfigDedicated[i].R_max;
		(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G = mac_inst->rrc_config.npdcch_ConfigDedicated[i].G;
		(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset = mac_inst->rrc_config.npdcch_ConfigDedicated[i].a_offset;
		//(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max = 8;
		//(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G = 1;
		//(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset = 0;
		(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T = (uint32_t)((double)(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G);
		(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss = (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset;
		//SCHEDULE_LOG("[init_mac_NB_IoT][CE%d] Rmax %d G %d, a_offset %d, PP %d search space start %d\n", i, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss);
		for(j=0;j<MAX_NUMBER_OF_UE_MAX_NB_IoT;++j)
		{
		  (mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].active=0;
		  (mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].RRC_connected=0;
		  (mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].direction = -1;
		}
		//SCHEDULE_LOG("[%04d][init_mac_NB_IoT][CE%d] R_max=%d, G=%.1f, a_offset=%.1f, T=%d, SS_start=%d\n", mac_inst->current_subframe, i, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss);
		//printf("[init_mac_NB_IoT] List_number %d R_max %d G %.1f a_offset %.1f T %d SS_start %d\n", i, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss);
	}
#endif
    //UL initial
	//Setting nprach configuration
	setting_nprach();
    
    //Initialize uplink resource from nprach configuration
    Initialize_Resource();
    
    //  extend DL available resource + SIBs scheduling
	extend_available_resource_DL(mac_inst, mac_inst->current_subframe + 1 + 160);
}
