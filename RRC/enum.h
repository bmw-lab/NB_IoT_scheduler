#ifndef _ENUM_H_
#define _ENUM_H_

/* Dependencies */
typedef enum PeriodicBSR_Timer_NB_IoT_r13 {
	PeriodicBSR_Timer_NB_IoT_r13_pp2	    = 2,
	PeriodicBSR_Timer_NB_IoT_r13_pp4	    = 4,
	PeriodicBSR_Timer_NB_IoT_r13_pp8	    = 8,
	PeriodicBSR_Timer_NB_IoT_r13_pp16	    = 16,
	PeriodicBSR_Timer_NB_IoT_r13_pp64	    = 64,
	PeriodicBSR_Timer_NB_IoT_r13_pp128	    = 128,
	PeriodicBSR_Timer_NB_IoT_r13_infinity	= 10000000,
	PeriodicBSR_Timer_NB_IoT_r13_spare	    = 0
} PeriodicBSR_Timer_NB_IoT_r13;

/* Dependencies */
typedef enum RetxBSR_Timer_NB_IoT_r13 {
	RetxBSR_Timer_NB_IoT_r13_pp4	    = 4,
	RetxBSR_Timer_NB_IoT_r13_pp16	    = 16,
	RetxBSR_Timer_NB_IoT_r13_pp64	    = 64,
	RetxBSR_Timer_NB_IoT_r13_pp128	    = 128,
	RetxBSR_Timer_NB_IoT_r13_pp256	    = 256,
	RetxBSR_Timer_NB_IoT_r13_pp512	    = 512,
	RetxBSR_Timer_NB_IoT_r13_infinity	= 10000000,
	RetxBSR_Timer_NB_IoT_r13_spare	    = 0
} RetxBSR_Timer_NB_IoT_r13;

/* Dependencies */
typedef enum MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR {
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR_NOTHING,	/* No components present */
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR_release,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR_setup
} MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR;

typedef enum MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13 {
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp2	    = 2,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp8	    = 8,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp32	    = 32,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp128 	= 128,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp512 	= 512,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp1024	= 1024,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_pp2048	= 2048,
	MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13_spare	    = 0
} MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13;

typedef enum DRX_Config_NB_IoT_r13_PR {
	DRX_Config_NB_IoT_r13_PR_NOTHING,	/* No components present */
	DRX_Config_NB_IoT_r13_PR_release,
	DRX_Config_NB_IoT_r13_PR_setup
} DRX_Config_NB_IoT_r13_PR;

typedef enum DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13 {
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp1	= 1,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp2	= 2,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp3	= 3,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp4	= 4,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp8	= 8,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp16	= 16,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_pp32	= 32,
	DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13_spare	= 0
} DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13;

typedef enum DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13 {
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp0	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp1	= 1,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp2	= 2,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp3	= 3,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp4	= 4,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp8	= 8,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp16	= 16,
	DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13_pp32	= 32
} DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13;

typedef enum DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13 {
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp0	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp1	= 1,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp2	= 2,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp4	= 4,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp6	= 6,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp8	= 8,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp16	= 16,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp24	= 24,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_pp33	= 33,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare7	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare6	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare5	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare4	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare3	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare2	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13_spare1	= 0
} DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13;

typedef enum DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13 {
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf256	= 256,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf512	= 512,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf1024	= 1024,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf1536	= 1536,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf2048	= 2048,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf3072	= 3072,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf4096	= 4096,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf4608	= 4608,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf6144	= 6144,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf7680	= 7680,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf8192	= 8192,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_sf9216	= 9216,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_spare4	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_spare3	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_spare2	= 0,
	DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13_spare1	= 0
} DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13;

typedef enum DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13 {
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp0	    = 0,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp1	    = 1,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp2	    = 2,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp4	    = 4,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp6	    = 6,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp8	    = 8,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp16	= 16,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp24	= 24,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp33	= 33,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp40	= 40,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp64	= 64,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp80	= 80,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp96	= 96,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp112	= 112,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp128	= 128,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp160	= 160,
	DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13_pp320	= 320
} DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13;

typedef enum DL_AM_RLC_NB_IoT_r13__enableStatusReportSN_Gap_r13 {
	DL_AM_RLC_NB_IoT_r13__enableStatusReportSN_Gap_r13_true	= 0
} DL_AM_RLC_NB_IoT_r13__enableStatusReportSN_Gap_r13;

typedef enum T_PollRetransmit_NB_IoT_r13 {
	T_PollRetransmit_NB_IoT_r13_ms250	    = 250,
	T_PollRetransmit_NB_IoT_r13_ms500	    = 500,
	T_PollRetransmit_NB_IoT_r13_ms1000	    = 1000,
	T_PollRetransmit_NB_IoT_r13_ms2000	    = 2000,
	T_PollRetransmit_NB_IoT_r13_ms3000	    = 3000,
	T_PollRetransmit_NB_IoT_r13_ms4000	    = 4000,
	T_PollRetransmit_NB_IoT_r13_ms6000	    = 6000,
	T_PollRetransmit_NB_IoT_r13_ms10000	    = 10000,
	T_PollRetransmit_NB_IoT_r13_ms15000	    = 15000,
	T_PollRetransmit_NB_IoT_r13_ms25000	    = 25000,
	T_PollRetransmit_NB_IoT_r13_ms40000	    = 40000,
	T_PollRetransmit_NB_IoT_r13_ms60000	    = 60000,
	T_PollRetransmit_NB_IoT_r13_ms90000	    = 90000	,
	T_PollRetransmit_NB_IoT_r13_ms120000	= 120000,
	T_PollRetransmit_NB_IoT_r13_ms180000	= 180000,
	T_PollRetransmit_NB_IoT_r13_spare1	    = 0
} T_PollRetransmit_NB_IoT_r13;

typedef enum UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13 {
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t1	= 1,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t2	= 2,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t3	= 3,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t4	= 4,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t6	= 6,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t8	= 8,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t16	= 16,
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13_t32	= 32
} UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13;

typedef enum RLC_Config_NB_IoT_r13_PR {
	RLC_Config_NB_IoT_r13_PR_NOTHING,	/* No components present */
	RLC_Config_NB_IoT_r13_PR_am,
	/* Extensions may appear below */
} RLC_Config_NB_IoT_r13_PR;

typedef enum SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR {
	SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR_NOTHING,	/* No components present */
	SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR_explicitValue,
	SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR_defaultValue
} SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR;

typedef enum SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR {
	SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR_NOTHING,	/* No components present */
	SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR_explicitValue,
	SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR_defaultValue
} SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR;

typedef enum PDCP_Config_NB_IoT_r13__discardTimer_r13 {
	PDCP_Config_NB_IoT_r13__discardTimer_r13_ms5120	    = 5120,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_ms10240	= 10240,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_ms20480	= 20480,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_ms40960	= 40960,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_ms81920	= 81920,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_infinity	= 10000000,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_spare2	    = 0,
	PDCP_Config_NB_IoT_r13__discardTimer_r13_spare1	    = 0
} PDCP_Config_NB_IoT_r13__discardTimer_r13;

typedef enum PDCP_Config_NB_IoT_r13__headerCompression_r13_PR {
	PDCP_Config_NB_IoT_r13__headerCompression_r13_PR_NOTHING,	/* No components present */
	PDCP_Config_NB_IoT_r13__headerCompression_r13_PR_notUsed,
	PDCP_Config_NB_IoT_r13__headerCompression_r13_PR_rohc
} PDCP_Config_NB_IoT_r13__headerCompression_r13_PR;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR {
	DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR_NOTHING,	/* No components present */
	DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR_useNoBitmap_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR_useAnchorBitmap_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR_explicitBitmapConfiguration_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR_spare
} DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR {
	DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR_NOTHING,	/* No components present */
	DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR_useNoGap_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR_useAnchorGapConfig_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR_explicitGapConfiguration_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR_spare
} DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR {
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR_NOTHING,	/* No components present */
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR_samePCI_r13,
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR_differentPCI_r13
} DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__differentPCI_r13__eutra_NumCRS_Ports_r13 {
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__differentPCI_r13__eutra_NumCRS_Ports_r13_same	= 0,
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__differentPCI_r13__eutra_NumCRS_Ports_r13_four	= 1
} DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__differentPCI_r13__eutra_NumCRS_Ports_r13;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__eutraControlRegionSize_r13 {
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__eutraControlRegionSize_r13_n1	= 0,
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__eutraControlRegionSize_r13_n2	= 1,
	DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__eutraControlRegionSize_r13_n3	= 2
} DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__eutraControlRegionSize_r13;

typedef enum DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330 {
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_12	= 12,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_10	= 10,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_8	= 8,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_6	= 6,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_4	= 4,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB_2	= 2,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB0	= 0,
	DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330_dB3	= 3
} DL_CarrierConfigDedicated_NB_IoT_r13__ext1__nrs_PowerOffsetNonAnchor_v1330;

typedef enum CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13 {
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_10  	= -10,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_9   	= -9,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_8   	= -8,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_7   	= -7,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_6	    = -6,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_5	    = -5,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_4	    = -4,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_3	    = -3,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_2	    = -2,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_1	    = -1,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v_0dot5	= -1/2,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v0	= 0,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v1	= 1,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v2	= 2,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v3	= 3,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v4	= 4,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v5	= 5,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v6	= 6,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v7	= 7,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v8	= 8,
	CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13_v9	= 9
} CarrierFreq_NB_IoT_r13__carrierFreqOffset_r13;

typedef enum DL_Bitmap_NB_IoT_r13_PR {
	DL_Bitmap_NB_IoT_r13_PR_NOTHING,	/* No components present */
	DL_Bitmap_NB_IoT_r13_PR_subframePattern10_r13,
	DL_Bitmap_NB_IoT_r13_PR_subframePattern40_r13
} DL_Bitmap_NB_IoT_r13_PR;

typedef enum DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13 {
	DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13_n32	= 32,
	DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13_n64	= 64,
	DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13_n128	= 128,
	DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13_n256	= 256
} DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13;

typedef enum DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13 {
	DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13_sf64 	= 64,
	DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13_sf128	= 128,
	DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13_sf256	= 256,
	DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13_sf512	= 512
} DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13;

typedef enum DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13 {
	DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13_oneEighth	    = 1/8,
	DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13_oneFourth	    = 1/4,
	DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13_threeEighth	= 3/8,
	DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13_oneHalf	    = 1/2
} DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13;

typedef enum ACK_NACK_NumRepetitions_NB_IoT_r13 {
	ACK_NACK_NumRepetitions_NB_IoT_r13_r1	= 1,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r2	= 2,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r4	= 4,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r8	= 8,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r16	= 16,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r32	= 32,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r64	= 64,
	ACK_NACK_NumRepetitions_NB_IoT_r13_r128	= 128
} ACK_NACK_NumRepetitions_NB_IoT_r13;

typedef enum NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13 {
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r1	= 1,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r2	= 2,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r4	= 4,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r8	= 8,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r16	= 16,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r32	= 32,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r64	= 64,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r128	= 128,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r256	= 256,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r512	= 512,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r1024	= 1024,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_r2048	= 2048,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_spare4	= 0,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_spare3	= 0,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_spare2	= 0,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13_spare1	= 0
}NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13;

typedef enum NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13 {
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v1dot5	= 3/2,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v2	    = 2,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v4	    = 4,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v8	    = 8,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v16	    = 16,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v32	    = 32,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v48	    = 48,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13_v64	    = 64
}NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13;

typedef enum NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13 {
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13_zero	        = 0,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13_oneEighth	    = 1/8,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13_oneFourth	    = 1/4,
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13_threeEighth	= 3/8
}NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13;

typedef enum NPUSCH_ConfigDedicated_NB_IoT_r13__groupHoppingDisabled_r13 {
	NPUSCH_ConfigDedicated_NB_IoT_r13__groupHoppingDisabled_r13_true	= 0
}NPUSCH_ConfigDedicated_NB_IoT_r13__groupHoppingDisabled_r13;

typedef enum RLF_TimersAndConstants_NB_IoT_r13_PR {
	RLF_TimersAndConstants_NB_IoT_r13_PR_NOTHING,	/* No components present */
	RLF_TimersAndConstants_NB_IoT_r13_PR_release,
	RLF_TimersAndConstants_NB_IoT_r13_PR_setup
} RLF_TimersAndConstants_NB_IoT_r13_PR;

typedef enum RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13 {
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms2500	= 2500,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms4000	= 4000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms6000	= 6000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms10000	= 10000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms15000	= 15000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms25000	= 25000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms40000	= 40000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13_ms60000	= 60000
} RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13;

typedef enum RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13 {
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms0	    = 0,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms200	= 200,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms500	= 500,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms1000	= 1000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms2000	= 2000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms4000	= 4000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13_ms8000	= 8000
} RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13;

typedef enum RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13 {
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n1	= 1,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n2	= 2,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n3	= 3,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n4	= 4,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n6	= 6,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n8	= 8,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n10	= 10,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13_n20	= 20
} RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13;

typedef enum RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13 {
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms1000	= 1000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms3000	= 3000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms5000	= 5000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms10000	= 10000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms15000	= 15000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms20000	= 20000,
	RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13_ms30000	= 30000
} RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13;

typedef enum RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13 {
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n1	= 1,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n2	= 2,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n3	= 3,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n4	= 4,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n5	= 5,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n6	= 6,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n8	= 8,
	RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13_n10	= 10
} RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13;

typedef enum RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR {
	RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR_NOTHING,	/* No components present */
	RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR_explicitValue_r13,
	RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR_defaultValue_r13
} RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR;

typedef enum PreambleTransMax {
	PreambleTransMax_n3	    = 3,
	PreambleTransMax_n4	    = 4,
	PreambleTransMax_n5	    = 5,
	PreambleTransMax_n6	    = 6,
	PreambleTransMax_n7	    = 7,
	PreambleTransMax_n8	    = 8,
	PreambleTransMax_n10	= 10,
	PreambleTransMax_n20	= 20,
	PreambleTransMax_n50	= 50,
	PreambleTransMax_n100	= 100,
	PreambleTransMax_n200	= 200
} PreambleTransMax;

typedef enum PowerRampingParameters__powerRampingStep {
	PowerRampingParameters__powerRampingStep_dB0	= 0,
	PowerRampingParameters__powerRampingStep_dB2	= 2,
	PowerRampingParameters__powerRampingStep_dB4	= 4,
	PowerRampingParameters__powerRampingStep_dB6	= 6
} PowerRampingParameters__powerRampingStep;

typedef enum PowerRampingParameters__preambleInitialReceivedTargetPower {
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_120	= 120,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_118	= 118,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_116	= 116,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_114	= 114,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_112	= 112,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_110	= 110,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_108	= 108,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_106	= 106,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_104	= 104,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_102	= 102,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_100	= 100,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_98	= 98,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_96	= 96,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_94	= 94,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_92	= 92,
	PowerRampingParameters__preambleInitialReceivedTargetPower_dBm_90	= 90
} PowerRampingParameters__preambleInitialReceivedTargetPower;

typedef enum RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13 {
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp2	    = 2,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp3	    = 3,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp4	    = 4,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp5	    = 5,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp6	    = 6,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp7	    = 7,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp8	    = 8,
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13_pp10	= 10
} RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13;

typedef enum RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13 {
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp1	    = 1,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp2	    = 2,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp3	    = 3,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp4	    = 4,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp8	    = 8,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp16	= 16,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp32	= 32,
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13_pp64	= 64
} RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13;

typedef enum BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13 {
	BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13_n16	    = 16,
	BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13_n32	    = 32,
	BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13_n64	    = 64,
	BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13_n128	= 128
} BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13;

typedef enum PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13 {
	PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13_rf128	= 128,
	PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13_rf256	= 256,
	PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13_rf512	= 512,
	PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13_rf1024	= 1024
} PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13;

typedef enum PCCH_Config_NB_IoT_r13__NB_IoT_r13 {
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_fourT	    = 0,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_twoT	        = 1,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_oneT     	= 2,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_halfT	    = 3,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_quarterT 	= 4,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one8thT	    = 5,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one16thT 	= 6,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one32ndT	    = 7,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one64thT	    = 8,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one128thT	= 9,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one256thT	= 10,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one512thT	= 11,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_one1024thT	= 12,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_spare3	    = 13,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_spare2	    = 14,
	PCCH_Config_NB_IoT_r13__NB_IoT_r13_spare1	    = 15
} PCCH_Config_NB_IoT_r13__NB_IoT_r13;

typedef enum PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13 {
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r1	    = 0,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r2	    = 1,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r4	    = 2,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r8	    = 3,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r16	    = 4,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r32	    = 5,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r64	    = 6,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r128	    = 7,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r256	    = 8,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r512 	= 9,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r1024	= 10,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_r2048	= 11,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_spare4	= 12,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_spare3	= 13,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_spare2	= 14,
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13_spare1	= 15
} PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms40	= 40,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms80	= 80,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms160	= 160,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms240	= 240,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms320	= 320,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms640	= 640,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms1280	= 1280,
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13_ms2560	= 2560
} NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms8	    = 8,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms16	    = 16,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms32 	= 32,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms64	    = 64,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms128	= 128,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms256	= 256,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms512	= 512,
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13_ms1024	= 1024
} NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n0	    = 0,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n12	    = 12,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n24	    = 24,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n36	    = 36,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n2	    = 2,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n18	    = 18,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_n34	    = 34,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13_spare1	= 0
} NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13_n12	= 12,
	NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13_n24	= 24,
	NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13_n36	= 36,
	NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13_n48	= 48
} NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13_zero	    = 0,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13_oneThird	= 1/3,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13_twoThird	= 2/3,
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13_one	    = 1
} NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13 {
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n3	    = 3,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n4	    = 4,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n5	    = 5,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n6	    = 6,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n7	    = 7,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n8	    = 8,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_n10	    = 10,
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13_spare1	= 0
} NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13 {
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n1	= 1,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n2	= 2,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n4	= 4,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n8	= 8,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n16	= 16,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n32	= 32,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n64	= 64,
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13_n128	= 128
} NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13 {
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r1	    = 1,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r2	    = 2,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r4	    = 4,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r8	    = 8,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r16	    = 16,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r32	    = 32,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r64	    = 64,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r128	    = 128,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r256	    = 256,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r512	    = 512,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r1024	= 1024,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_r2048	= 2048,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_spare4	= 0,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_spare3	= 0,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_spare2	= 0,
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13_spare1	= 0
} NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13 {
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v1dot5	= 3/2,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v2	    = 2,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v4	    = 4,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v8	    = 8,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v16 	= 16,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v32	    = 32,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v48	    = 48,
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13_v64	    = 64
} NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13;

typedef enum NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13 {
	NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13_zero	        = 0,
	NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13_oneEighth	= 1,
	NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13_oneFourth	= 2,
	NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13_threeEighth	= 3
} NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13;

typedef enum NPRACH_ConfigSIB_NB_IoT_r13__nprach_CP_Length_r13 {
	NPRACH_ConfigSIB_NB_IoT_r13__nprach_CP_Length_r13_us66dot7	= 0,
	NPRACH_ConfigSIB_NB_IoT_r13__nprach_CP_Length_r13_us266dot7	= 1
} NPRACH_ConfigSIB_NB_IoT_r13__nprach_CP_Length_r13;

typedef enum NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13 {
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc0	= 0,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc1	= 1,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc2	= 2,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc3	= 3,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc4	= 4,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc5	= 5,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc6	= 6,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc7	= 7,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc8	= 8,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc9	= 9,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc10	= 10,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc11	= 11,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc12	= 12,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc13	= 13,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc14	= 14,
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13_sc15	= 15
} NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13;

typedef enum UplinkPowerControlCommon_NB_IoT_r13__alpha_r13 {
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al0	= 0,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al04	= 1,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al05	= 2,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al06	= 3,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al07	= 4,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al08	= 5,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al09	= 6,
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13_al1	= 7
} UplinkPowerControlCommon_NB_IoT_r13__alpha_r13;

typedef enum NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13 {
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n8	= 8,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n10	= 10,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n11	= 11,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n12	= 12,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n20	= 20,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n22	= 22,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n23	= 23,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n24	= 24,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n32	= 32,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n34	= 34,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n35	= 35,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n36	= 36,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n40	= 40,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n44	= 44,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n46	= 46,
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13_n48	= 48
} NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__t300_r13 {
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms2500	= 2500,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms4000	= 4000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms6000	= 6000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms10000	= 10000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms15000	= 15000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms25000	= 25000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms40000	= 40000,
	UE_TimersAndConstants_NB_IoT_r13__t300_r13_ms60000	= 60000
} UE_TimersAndConstants_NB_IoT_r13__t300_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__t301_r13 {
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms2500	= 2500,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms4000	= 4000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms6000	= 6000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms10000	= 10000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms15000	= 15000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms25000	= 25000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms40000	= 40000,
	UE_TimersAndConstants_NB_IoT_r13__t301_r13_ms60000	= 60000
} UE_TimersAndConstants_NB_IoT_r13__t301_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__t310_r13 {
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms0	    = 0,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms200	= 200,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms500	= 500,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms1000	= 1000,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms2000	= 2000,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms4000	= 4000,
	UE_TimersAndConstants_NB_IoT_r13__t310_r13_ms8000	= 8000
} UE_TimersAndConstants_NB_IoT_r13__t310_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__n310_r13 {
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n1	= 1,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n2	= 2,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n3	= 3,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n4	= 4,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n6	= 6,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n8	= 8,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n10	= 10,
	UE_TimersAndConstants_NB_IoT_r13__n310_r13_n20	= 20
} UE_TimersAndConstants_NB_IoT_r13__n310_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__t311_r13 {
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms1000	= 1000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms3000	= 3000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms5000	= 5000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms10000	= 10000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms15000	= 15000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms20000	= 20000,
	UE_TimersAndConstants_NB_IoT_r13__t311_r13_ms30000	= 30000
} UE_TimersAndConstants_NB_IoT_r13__t311_r13;

typedef enum UE_TimersAndConstants_NB_IoT_r13__n311_r13 {
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n1	= 1,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n2	= 2,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n3	= 3,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n4	= 4,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n5	= 5,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n6	= 6,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n8	= 8,
	UE_TimersAndConstants_NB_IoT_r13__n311_r13_n10	= 10
} UE_TimersAndConstants_NB_IoT_r13__n311_r13;

typedef enum TimeAlignmentTimer {
	TimeAlignmentTimer_sf500	= 500,
	TimeAlignmentTimer_sf750	= 750,
	TimeAlignmentTimer_sf1280	= 1280,
	TimeAlignmentTimer_sf1920	= 1920,
	TimeAlignmentTimer_sf2560	= 2560,
	TimeAlignmentTimer_sf5120	= 5120,
	TimeAlignmentTimer_sf10240	= 10240,
	TimeAlignmentTimer_infinity	= 10000000
} TimeAlignmentTimer;

/* Dependencies */
typedef enum SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13 {
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB0	= 0,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB1	= 1,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB2	= 2,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB3	= 3,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB4	= 4,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB5	= 5,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB6	= 6,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB8	= 8,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB10	= 10,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB12	= 12,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB14	= 14,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB16	= 16,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB18	= 18,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB20	= 20,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB22	= 22,
	SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13_dB24	= 24
} SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13__q_Hyst_r13;

/* Dependencies */
typedef enum T_Reselection_NB_IoT_r13 {
	T_Reselection_NB_IoT_r13_s0	    = 0,
	T_Reselection_NB_IoT_r13_s3	    = 3,
	T_Reselection_NB_IoT_r13_s6	    = 6,
	T_Reselection_NB_IoT_r13_s9	    = 9,
	T_Reselection_NB_IoT_r13_s12	= 12,
	T_Reselection_NB_IoT_r13_s15	= 15,
	T_Reselection_NB_IoT_r13_s18	= 18,
	T_Reselection_NB_IoT_r13_s21	= 21
} T_Reselection_NB_IoT_r13;

typedef enum SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR {
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_NOTHING,	/* No components present */
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib2_r13,
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib3_r13,
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib4_r13,
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib5_r13,
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib14_r13,
	SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR_sib16_r13,
	/* Extensions may appear below */
} SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR;

typedef enum PLMN_IdentityInfo_NB_IoT_r13__cellReservedForOperatorUse_r13 {
	PLMN_IdentityInfo_NB_IoT_r13__cellReservedForOperatorUse_r13_reserved	= 0,
	PLMN_IdentityInfo_NB_IoT_r13__cellReservedForOperatorUse_r13_notReserved	= 1
}PLMN_IdentityInfo_NB_IoT_r13__cellReservedForOperatorUse_r13;

typedef enum PLMN_IdentityInfo_NB_IoT_r13__attachWithoutPDN_Connectivity_r13 {
	PLMN_IdentityInfo_NB_IoT_r13__attachWithoutPDN_Connectivity_r13_true	= 0
}PLMN_IdentityInfo_NB_IoT_r13__attachWithoutPDN_Connectivity_r13;

typedef enum SIB_Type_NB_IoT_r13 {
	SIB_Type_NB_IoT_r13_sibType2_NB_IoT_r13     = 0x1,
	SIB_Type_NB_IoT_r13_sibType3_NB_IoT_r13	    = 0x2,
	SIB_Type_NB_IoT_r13_sibType4_NB_IoT_r13	    = 0x4,
	SIB_Type_NB_IoT_r13_sibType5_NB_IoT_r13	    = 0x8,
	SIB_Type_NB_IoT_r13_sibType14_NB_IoT_r13	= 0x10,
	SIB_Type_NB_IoT_r13_sibType16_NB_IoT_r13	= 0x20,
	SIB_Type_NB_IoT_r13_spare3	            = 0,
	SIB_Type_NB_IoT_r13_spare2	            = 0,
	SIB_Type_NB_IoT_r13_spare1	            = 0
}SIB_Type_NB_IoT_r13;

typedef enum SchedulingInfo_NB_IoT_r13__si_Periodicity_r13 {
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf64	    = 640,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf128	    = 1280,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf256	    = 2560,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf512	    = 5120,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf1024	= 10240,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf2048	= 20480,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_rf4096	= 40960,
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13_spare	    = 0
}SchedulingInfo_NB_IoT_r13__si_Periodicity_r13;

typedef enum SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13 {
	SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13_every2ndRF	= 20,
	SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13_every4thRF	= 40,
	SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13_every8thRF	= 80,
	SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13_every16thRF	= 160
}SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13;

typedef enum SchedulingInfo_NB_IoT_r13__si_TB_r13 {
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b56	= 2,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b120	= 2,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b208	= 8,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b256	= 8,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b328	= 8,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b440	= 8,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b552	= 8,
	SchedulingInfo_NB_IoT_r13__si_TB_r13_b680	= 8
}SchedulingInfo_NB_IoT_r13__si_TB_r13;

typedef enum SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__cellBarred_r13 {
	SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__cellBarred_r13_barred	= 0,
	SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__cellBarred_r13_notBarred	= 1
} SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__cellBarred_r13;

typedef enum SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__intraFreqReselection_r13 {
	SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__intraFreqReselection_r13_allowed	    = 0,
	SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__intraFreqReselection_r13_notAllowed	= 1
} SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__intraFreqReselection_r13;

typedef enum SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13 {
	SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13_n1	= 0,
	SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13_n2	= 1,
	SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13_n3	= 2
} SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13;

typedef enum SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13 {
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB_6	    	= 0,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB_4dot77	= 1,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB_3	    	= 2,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB_1dot77	= 3,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB0	        = 4,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB1	        = 5,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB1dot23	= 6,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB2	        = 7,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB3	        = 8,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB4	        = 9,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB4dot23	= 10,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB5	        = 11,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB6	        = 12,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB7	        = 13,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB8	        = 14,
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13_dB9	        = 15
} SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13;

typedef enum SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13 {
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms160	= 160,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms320	= 320,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms480	= 480,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms640	= 640,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms960	= 960,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms1280	= 1280,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_ms1600	= 1600,
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13_spare1	= 0
} SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13;

typedef enum SystemInformation_NB_IoT__criticalExtensions_PR {
	SystemInformation_NB_IoT__criticalExtensions_PR_NOTHING,	/* No components present */
	SystemInformation_NB_IoT__criticalExtensions_PR_systemInformation_r13,
	SystemInformation_NB_IoT__criticalExtensions_PR_criticalExtensionsFuture
} SystemInformation_NB_IoT__criticalExtensions_PR;

typedef enum BCCH_DL_SCH_MessageType_NB_IoT_PR {
	BCCH_DL_SCH_MessageType_NB_IoT_PR_NOTHING,	/* No components present */
	BCCH_DL_SCH_MessageType_NB_IoT_PR_c1,
	BCCH_DL_SCH_MessageType_NB_IoT_PR_messageClassExtension
} BCCH_DL_SCH_MessageType_NB_IoT_PR;

typedef enum BCCH_DL_SCH_MessageType_NB_IoT__c1_PR {
	BCCH_DL_SCH_MessageType_NB_IoT__c1_PR_NOTHING,	/* No components present */
	BCCH_DL_SCH_MessageType_NB_IoT__c1_PR_systemInformation_r13,
	BCCH_DL_SCH_MessageType_NB_IoT__c1_PR_systemInformatioNB_IoTlockType1_r13
} BCCH_DL_SCH_MessageType_NB_IoT__c1_PR;

/* Dependencies */
typedef enum ChannelRasterOffset_NB_IoT_r13 {
	ChannelRasterOffset_NB_IoT_r13_khz_7dot5	= -15/2,
	ChannelRasterOffset_NB_IoT_r13_khz_2dot5	= -5/2,
	ChannelRasterOffset_NB_IoT_r13_khz2dot5 	= 5/2,
	ChannelRasterOffset_NB_IoT_r13_khz7dot5	= 15/2
} ChannelRasterOffset_NB_IoT_r13;

typedef enum INB_IoTand_DifferentPCI_NB_IoT_r13__eutra_NumCRS_Ports_r13 {
	INB_IoTand_DifferentPCI_NB_IoT_r13__eutra_NumCRS_Ports_r13_same	= 0,
	INB_IoTand_DifferentPCI_NB_IoT_r13__eutra_NumCRS_Ports_r13_four	= 1
} INB_IoTand_DifferentPCI_NB_IoT_r13__eutra_NumCRS_Ports_r13;

typedef enum MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR {
	MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR_NOTHING,	/* No components present */
	MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR_iNB_IoTand_SamePCI_r13,
	MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR_iNB_IoTand_DifferentPCI_r13,
	MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR_guardband_r13,
	MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR_standalone_r13
} MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR;


#endif
