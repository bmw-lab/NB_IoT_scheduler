#include "enum.h"

#define	A_SEQUENCE_OF(type)	A_SET_OF(type)

#define	A_SET_OF(type)					\
	struct {					\
		type **array;				\
		int count;	/* Meaningful size */	\
		int size;	/* Allocated size */	\
		void (*free)(type *);			\
	}

typedef unsigned int	uint32_t;
typedef unsigned short	uint16_t;
typedef unsigned char	uint8_t;
typedef signed int		int32_t;
typedef signed short	int16_t;
typedef signed char		int8_t;
typedef int BOOLEAN_t;
typedef int NULL_t;

typedef struct BIT_STRING_s {
	uint8_t *buf;	/* BIT STRING body */
	int size;	/* Size of the above buffer */
	int bits_unused;/* Unused trailing bits in the last octet (0..7) */

} BIT_STRING_t;

typedef struct OCTET_STRING {
	uint8_t *buf;	/* Buffer with consecutive OCTET_STRING bits */
	int size;	/* Size of the buffer */

} OCTET_STRING_t;

/* AdditionalSpectrumEmission */
typedef long	 AdditionalSpectrumEmission_t;
/* FreqBandIndicator-NB_IoT-r13 */
typedef long	 FreqBandIndicator_NB_IoT_r13_t;
/* MCC-MNC-Digit */
typedef long	 MCC_MNC_Digit_t;
/* TrackingAreaCode */
typedef BIT_STRING_t	 TrackingAreaCode_t;
/* CellIdentity */
typedef BIT_STRING_t	 CellIdentity_t;
/* Q-RxLevMin */
typedef long	 Q_RxLevMin_t;
/* Q-QualMin-r9 */
typedef long	 Q_QualMin_r9_t;
/* P-Max */
typedef long	 P_Max_t;
/* ARFCN-ValueEUTRA-r9 */
typedef long	 ARFCN_ValueEUTRA_r9_t;
/* TimeAlignmentTimer */
typedef long	 TimeAlignmentTimer_t;
/* RSRP-Range */
typedef long	 RSRP_Range_t;
/* DRB-Identity */
typedef long	 DRB_Identity_t;
/* T-PollRetransmit-NB_IoT-r13 */
typedef long	 T_PollRetransmit_NB_IoT_r13_t;
/* RetxBSR-Timer-NB_IoT-r13 */
typedef long	 ReselectionThreshold_t;
/* SystemInfoValueTagSI-r13 */
typedef long	 SystemInfoValueTagSI_r13_t;

///************Forward declarations************/
struct DRX_Config_NB_IoT_r13;
struct SRB_ToAddMod_NB_IoT_r13;
struct PDCP_Config_NB_IoT_r13;
struct RLC_Config_NB_IoT_r13;
struct LogicalChannelConfig_NB_IoT_r13;
struct DRB_ToAddMod_NB_IoT_r13;
struct CarrierConfigDedicated_NB_IoT_r13;
struct NPDCCH_ConfigDedicated_NB_IoT_r13;
struct NPUSCH_ConfigDedicated_NB_IoT_r13;
struct UplinkPowerControlDedicated_NB_IoT_r13;
struct CarrierFreq_NB_IoT_r13;
struct SRB_ToAddModList_NB_IoT_r13;
struct DRB_ToAddModList_NB_IoT_r13;
struct DRB_ToReleaseList_NB_IoT_r13;
struct PhysicalConfigDedicated_NB_IoT_r13;
struct RLF_TimersAndConstants_NB_IoT_r13;
struct RACH_Info_NB_IoT_r13;
struct NPRACH_Parameters_NB_IoT_r13;
struct RSRP_ThresholdsNPRACH_InfoList_NB_IoT_r13;
struct DL_GapConfig_NB_IoT_r13;
struct NPRACH_ConfigSIB_NB_IoT_v1330;
struct NPRACH_Parameters_NB_IoT_v1330;
struct CarrierFreq_NB_IoT_r13;
struct PLMN_IdentityInfo_NB_IoT_r13;
struct SchedulingInfo_NB_IoT_r13;
struct MultiBandInfoList_NB_IoT_r13;
struct DL_Bitmap_NB_IoT_r13;
struct SystemInfoValueTagList_NB_IoT_r13;
struct MultiBandInfo_NB_IoT_r13;
struct NS_PmaxList_NB_IoT_r13;
struct NS_PmaxValue_NB_IoT_r13;
struct MCC;
///-----------------------------------Below---RadioResourceConfigDedicated ---Structure-----------------------------------///


/* DRX-Config-NB_IoT-r13 */
typedef struct DRX_Config_NB_IoT_r13 {
	DRX_Config_NB_IoT_r13_PR present;
	union DRX_Config_NB_IoT_r13_u {
		NULL_t	 release;
		struct DRX_Config_NB_IoT_r13__setup {
			DRX_Config_NB_IoT_r13__setup__onDurationTimer_r13	 onDurationTimer_r13;
			DRX_Config_NB_IoT_r13__setup__drx_InactivityTimer_r13	 drx_InactivityTimer_r13;
			DRX_Config_NB_IoT_r13__setup__drx_RetransmissionTimer_r13	 drx_RetransmissionTimer_r13;
			DRX_Config_NB_IoT_r13__setup__drx_Cycle_r13	 drx_Cycle_r13;
			long	 drx_StartOffset_r13;
			DRX_Config_NB_IoT_r13__setup__drx_ULRetransmissionTimer_r13	 drx_ULRetransmissionTimer_r13;
		} setup;
	} choice;
}DRX_Config_NB_IoT_r13_t;

/* MAC-MainConfig-NB_IoT-r13 */
typedef struct MAC_MainConfig_NB_IoT_r13 {
	struct MAC_MainConfig_NB_IoT_r13__ul_SCH_Config_r13 {
		PeriodicBSR_Timer_NB_IoT_r13	*periodicBSR_Timer_r13	/* OPTIONAL */;
		RetxBSR_Timer_NB_IoT_r13	 retxBSR_Timer_r13;
	} *ul_SCH_Config_r13;
	struct DRX_Config_NB_IoT_r13	*drx_Config_r13	/* OPTIONAL */;
	TimeAlignmentTimer_t	 timeAlignmentTimerDedicated_r13;
	struct MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13 {
		MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_PR present;
		union MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13_u {
			NULL_t	 release;
			struct MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup {
				MAC_MainConfig_NB_IoT_r13__logicalChannelSR_Config_r13__setup__logicalChannelSR_ProhibitTimer_r13	 logicalChannelSR_ProhibitTimer_r13;
			} setup;
		} choice;
	} *logicalChannelSR_Config_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
}MAC_MainConfig_NB_IoT_r13_t;

/* LogicalChannelConfig-NB_IoT-r13 */
typedef struct LogicalChannelConfig_NB_IoT_r13 {
	long	*priority_r13	/* OPTIONAL */;
	BOOLEAN_t	*logicalChannelSR_Prohibit_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
}LogicalChannelConfig_NB_IoT_r13_t;

/* DL-AM-RLC-NB_IoT-r13 */
typedef struct DL_AM_RLC_NB_IoT_r13 {
	long	*enableStatusReportSN_Gap_r13	/* OPTIONAL */;
} DL_AM_RLC_NB_IoT_r13_t;

/* UL-AM-RLC-NB_IoT-r13 */
typedef struct UL_AM_RLC_NB_IoT_r13 {
	T_PollRetransmit_NB_IoT_r13	 t_PollRetransmit_r13;
	UL_AM_RLC_NB_IoT_r13__maxRetxThreshold_r13	 maxRetxThreshold_r13;
} UL_AM_RLC_NB_IoT_r13_t;

/* RLC-Config-NB_IoT-r13 */
typedef struct RLC_Config_NB_IoT_r13 {
	RLC_Config_NB_IoT_r13_PR present;
	union RLC_Config_NB_IoT_r13_u {
		struct RLC_Config_NB_IoT_r13__am {
			UL_AM_RLC_NB_IoT_r13_t	 ul_AM_RLC_r13;
			DL_AM_RLC_NB_IoT_r13_t	 dl_AM_RLC_r13;
		} am;
		/*
		 * This type is extensible,
		 * possible extensions are below.
		 */
	} choice;
} RLC_Config_NB_IoT_r13_t;

/* SRB-ToAddMod-NB_IoT-r13 */
typedef struct SRB_ToAddMod_NB_IoT_r13 {
	struct SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13 {
		SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_PR present;
		union SRB_ToAddMod_NB_IoT_r13__rlc_Config_r13_u {
			RLC_Config_NB_IoT_r13_t	 explicitValue;
			NULL_t	 defaultValue;
		} choice;

	} *rlc_Config_r13;
	struct SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13 {
		SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_PR present;
		union SRB_ToAddMod_NB_IoT_r13__logicalChannelConfig_r13_u {
			LogicalChannelConfig_NB_IoT_r13_t	 explicitValue;
			NULL_t	 defaultValue;
		} choice;
	} *logicalChannelConfig_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
}SRB_ToAddMod_NB_IoT_r13_t;

/* SRB-ToAddModList-NB_IoT-r13 */
typedef struct SRB_ToAddModList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct SRB_ToAddMod_NB_IoT_r13) list;
}SRB_ToAddModList_NB_IoT_r13_t;

/* PDCP-Config-NB_IoT-r13 */
typedef struct PDCP_Config_NB_IoT_r13 {
	PDCP_Config_NB_IoT_r13__discardTimer_r13	discardTimer_r13	/* OPTIONAL */;
	struct PDCP_Config_NB_IoT_r13__headerCompression_r13 {
		PDCP_Config_NB_IoT_r13__headerCompression_r13_PR present;
		union PDCP_Config_NB_IoT_r13__headerCompression_r13_u {
			NULL_t	 notUsed;
			struct PDCP_Config_NB_IoT_r13__headerCompression_r13__rohc {
				long	*maxCID_r13	/* DEFAULT 15 */;
				struct PDCP_Config_NB_IoT_r13__headerCompression_r13__rohc__profiles_r13 {
					BOOLEAN_t	 profile0x0002;
					BOOLEAN_t	 profile0x0003;
					BOOLEAN_t	 profile0x0004;
					BOOLEAN_t	 profile0x0006;
					BOOLEAN_t	 profile0x0102;
					BOOLEAN_t	 profile0x0103;
					BOOLEAN_t	 profile0x0104;
				} profiles_r13;
				/*
				 * This type is extensible,
				 * possible extensions are below.
				 */
			} rohc;
		} choice;
	} headerCompression_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
}PDCP_Config_NB_IoT_r13_t;

/* DRB-ToAddMod-NB_IoT-r13 */
typedef struct DRB_ToAddMod_NB_IoT_r13 {
	long	*eps_BearerIdentity_r13	/* OPTIONAL */;
	DRB_Identity_t	 drb_Identity_r13;
	struct PDCP_Config_NB_IoT_r13	*pdcp_Config_r13	/* OPTIONAL */;
	struct RLC_Config_NB_IoT_r13	*rlc_Config_r13	/* OPTIONAL */;
	long	*logicalChannelIdentity_r13	/* OPTIONAL */;
	struct LogicalChannelConfig_NB_IoT_r13	*logicalChannelConfig_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
}DRB_ToAddMod_NB_IoT_r13_t;

/* DRB-ToAddModList-NB_IoT-r13 */
typedef struct DRB_ToAddModList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct DRB_ToAddMod_NB_IoT_r13) list;
}DRB_ToAddModList_NB_IoT_r13_t;

/* DRB-ToReleaseList-NB_IoT-r13 */
typedef struct DRB_ToReleaseList_NB_IoT_r13 {
	A_SEQUENCE_OF(DRB_Identity_t) list;
}DRB_ToReleaseList_NB_IoT_r13_t;

/* CarrierFreq-NB_IoT-r13 */
typedef struct CarrierFreq_NB_IoT_r13 {
	ARFCN_ValueEUTRA_r9_t	 carrierFreq_r13;
	long	*carrierFreqOffset_r13	/* OPTIONAL */;
}CarrierFreq_NB_IoT_r13_t;

/* DL-Bitmap-NB_IoT-r13 */
typedef struct DL_Bitmap_NB_IoT_r13 {
	DL_Bitmap_NB_IoT_r13_PR present;
	union DL_Bitmap_NB_IoT_r13_u {
		BIT_STRING_t	 subframePattern10_r13;
		BIT_STRING_t	 subframePattern40_r13;
	} choice;
}DL_Bitmap_NB_IoT_r13_t;

/* DL-GapConfig-NB_IoT-r13 */
typedef struct DL_GapConfig_NB_IoT_r13 {
	DL_GapConfig_NB_IoT_r13__dl_GapThreshold_r13	 dl_GapThreshold_r13;
	DL_GapConfig_NB_IoT_r13__dl_GapPeriodicity_r13	 dl_GapPeriodicity_r13;
	DL_GapConfig_NB_IoT_r13__dl_GapDurationCoeff_r13	 dl_GapDurationCoeff_r13;
}DL_GapConfig_NB_IoT_r13_t;

/* DL-CarrierConfigDedicated-NB_IoT-r13 */
typedef struct DL_CarrierConfigDedicated_NB_IoT_r13 {
	CarrierFreq_NB_IoT_r13_t	 dl_CarrierFreq_r13;
	struct DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13 {
		DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_PR present;
		union DL_CarrierConfigDedicated_NB_IoT_r13__downlinkBitmapNonAnchor_r13_u {
			NULL_t	 useNoBitmap_r13;
			NULL_t	 useAnchorBitmap_r13;
			DL_Bitmap_NB_IoT_r13_t	 explicitBitmapConfiguration_r13;
			NULL_t	 spare;
		} choice;
	} *downlinkBitmapNonAnchor_r13;
	struct DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13 {
		DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_PR present;
		union DL_CarrierConfigDedicated_NB_IoT_r13__dl_GapNonAnchor_r13_u {
			NULL_t	 useNoGap_r13;
			NULL_t	 useAnchorGapConfig_r13;
			DL_GapConfig_NB_IoT_r13_t	 explicitGapConfiguration_r13;
			NULL_t	 spare;
		} choice;
	} *dl_GapNonAnchor_r13;
	struct DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13 {
		struct DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13 {
			DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_PR present;
			union DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13_u {
				struct DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__samePCI_r13 {
					long	 indexToMidPRB_r13;
				} samePCI_r13;
				struct DL_CarrierConfigDedicated_NB_IoT_r13__iNB_IoTandCarrierInfo_r13__samePCI_Indicator_r13__differentPCI_r13 {
					long	 eutra_NumCRS_Ports_r13;
				} differentPCI_r13;
			} choice;
		} *samePCI_Indicator_r13;
		long	 eutraControlRegionSize_r13;
	} *iNB_IoTandCarrierInfo_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
	struct DL_CarrierConfigDedicated_NB_IoT_r13__ext1 {
		long	*nrs_PowerOffsetNonAnchor_v1330	/* OPTIONAL */; ///INTEGER (-60..50)///
	} *ext1;
} DL_CarrierConfigDedicated_NB_IoT_r13_t;

/* UL-CarrierConfigDedicated-NB_IoT-r13 */
typedef struct UL_CarrierConfigDedicated_NB_IoT_r13 {
	struct CarrierFreq_NB_IoT_r13	*ul_CarrierFreq_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} UL_CarrierConfigDedicated_NB_IoT_r13_t;

/* CarrierConfigDedicated-NB_IoT-r13 */
typedef struct CarrierConfigDedicated_NB_IoT_r13 {
	DL_CarrierConfigDedicated_NB_IoT_r13_t	 dl_CarrierConfig_r13;
	UL_CarrierConfigDedicated_NB_IoT_r13_t	 ul_CarrierConfig_r13;
} CarrierConfigDedicated_NB_IoT_r13_t;

/* NPDCCH-ConfigDedicated-NB_IoT-r13 */
typedef struct NPDCCH_ConfigDedicated_NB_IoT_r13 {
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_NumRepetitions_r13    npdcch_NumRepetitions_r13;
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_StartSF_USS_r13	    npdcch_StartSF_USS_r13;
	NPDCCH_ConfigDedicated_NB_IoT_r13__npdcch_Offset_USS_r13	    npdcch_Offset_USS_r13;
}NPDCCH_ConfigDedicated_NB_IoT_r13_t;

/* NPUSCH-ConfigDedicated-NB_IoT-r13 */
typedef struct NPUSCH_ConfigDedicated_NB_IoT_r13 {
	ACK_NACK_NumRepetitions_NB_IoT_r13	*ack_NACK_NumRepetitions_r13	/* OPTIONAL */;
	BOOLEAN_t	*npusch_AllSymbols_r13	/* OPTIONAL */;
	long	*groupHoppingDisabled_r13	/* OPTIONAL */;
}NPUSCH_ConfigDedicated_NB_IoT_r13_t;

/* UplinkPowerControlDedicated-NB_IoT-r13 */
typedef struct UplinkPowerControlDedicated_NB_IoT_r13 {
	long	 p0_UE_NPUSCH_r13;
} UplinkPowerControlDedicated_NB_IoT_r13_t;

/* PhysicalConfigDedicated-NB_IoT-r13 */
typedef struct PhysicalConfigDedicated_NB_IoT_r13 {
	struct CarrierConfigDedicated_NB_IoT_r13	    *carrierConfigDedicated_r13	/* OPTIONAL */;
	struct NPDCCH_ConfigDedicated_NB_IoT_r13	    *npdcch_ConfigDedicated_r13	/* OPTIONAL */;
	struct NPUSCH_ConfigDedicated_NB_IoT_r13	    *npusch_ConfigDedicated_r13	/* OPTIONAL */;
	struct UplinkPowerControlDedicated_NB_IoT_r13	*uplinkPowerControlDedicated_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} PhysicalConfigDedicated_NB_IoT_r13_t;

/* RLF-TimersAndConstants-NB_IoT-r13 */
typedef struct RLF_TimersAndConstants_NB_IoT_r13 {
	RLF_TimersAndConstants_NB_IoT_r13_PR present;
	union RLF_TimersAndConstants_NB_IoT_r13_u {
		NULL_t	 release;
		struct RLF_TimersAndConstants_NB_IoT_r13__setup {
			RLF_TimersAndConstants_NB_IoT_r13__setup__t301_r13	 t301_r13;
			RLF_TimersAndConstants_NB_IoT_r13__setup__t310_r13	 t310_r13;
			RLF_TimersAndConstants_NB_IoT_r13__setup__n310_r13	 n310_r13;
			RLF_TimersAndConstants_NB_IoT_r13__setup__t311_r13	 t311_r13;
			RLF_TimersAndConstants_NB_IoT_r13__setup__n311_r13	 n311_r13;
			/*
			 * This type is extensible,
			 * possible extensions are below.
			 */
		} setup;
	} choice;
} RLF_TimersAndConstants_NB_IoT_r13_t;

/* RadioResourceConfigDedicated-NB_IoT-r13 */
typedef struct RadioResourceConfigDedicated_NB_IoT_r13 {
	struct SRB_ToAddModList_NB_IoT_r13	*srb_ToAddModList_r13	/* OPTIONAL */;
	struct DRB_ToAddModList_NB_IoT_r13	*drb_ToAddModList_r13	/* OPTIONAL */;
	struct DRB_ToReleaseList_NB_IoT_r13	*drb_ToReleaseList_r13	/* OPTIONAL */;
	struct RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13 {
		RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_PR present;
		union RadioResourceConfigDedicated_NB_IoT_r13__mac_MainConfig_r13_u {
			MAC_MainConfig_NB_IoT_r13_t	 explicitValue_r13;
			NULL_t	 defaultValue_r13;
		} choice;
	} *mac_MainConfig_r13;
	struct PhysicalConfigDedicated_NB_IoT_r13	*physicalConfigDedicated_r13	/* OPTIONAL */;
	struct RLF_TimersAndConstants_NB_IoT_r13	*rlf_TimersAndConstants_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} RadioResourceConfigDedicated_NB_IoT_r13_t;

/* RRCConnectionSetup-NB_IoT-r13-IEs */
typedef struct RRCConnectionSetup_NB_IoT_r13_IEs {
	RadioResourceConfigDedicated_NB_IoT_r13_t	 radioResourceConfigDedicated_r13;
	OCTET_STRING_t	*lateNonCriticalExtension	/* OPTIONAL */;
	struct RRCConnectionSetup_NB_IoT_r13_IEs__nonCriticalExtension {
	} *nonCriticalExtension;
} RRCConnectionSetup_NB_IoT_r13_IEs_t;








/* PowerRampingParameters */
typedef struct PowerRampingParameters {
	PowerRampingParameters__powerRampingStep	                 powerRampingStep;
	PowerRampingParameters__preambleInitialReceivedTargetPower	 preambleInitialReceivedTargetPower;
} PowerRampingParameters_t;

/* RACH-Info-NB_IoT-r13 */
typedef struct RACH_Info_NB_IoT_r13 {
	RACH_Info_NB_IoT_r13__ra_ResponseWindowSize_r13	            ra_ResponseWindowSize_r13;
	RACH_Info_NB_IoT_r13__mac_ContentionResolutionTimer_r13	    mac_ContentionResolutionTimer_r13;
} RACH_Info_NB_IoT_r13_t;

/* RACH-InfoList-NB_IoT-r13 */
typedef struct RACH_InfoList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct RACH_Info_NB_IoT_r13) list;
} RACH_InfoList_NB_IoT_r13_t;

/* RACH-ConfigCommon-NB_IoT-r13 */
typedef struct RACH_ConfigCommon_NB_IoT_r13 {
	PreambleTransMax	         preambleTransMax_CE_r13;
	PowerRampingParameters_t	 powerRampingParameters_r13;
	RACH_InfoList_NB_IoT_r13_t	     rach_InfoList_r13;
	long	                     *connEstFailOffset_r13	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} RACH_ConfigCommon_NB_IoT_r13_t;

/* BCCH-Config-NB_IoT-r13 */
typedef struct BCCH_Config_NB_IoT_r13 {
	BCCH_Config_NB_IoT_r13__modificationPeriodCoeff_r13	 modificationPeriodCoeff_r13;
} BCCH_Config_NB_IoT_r13_t;

/* PCCH-Config-NB_IoT-r13 */
typedef struct PCCH_Config_NB_IoT_r13 {
	PCCH_Config_NB_IoT_r13__defaultPagingCycle_r13	         defaultPagingCycle_r13;
	PCCH_Config_NB_IoT_r13__NB_IoT_r13	                         NB_IoT_r13;
	PCCH_Config_NB_IoT_r13__npdcch_NumRepetitionPaging_r13	 npdcch_NumRepetitionPaging_r13;
} PCCH_Config_NB_IoT_r13_t;

/* NPRACH-Parameters-NB_IoT-r13 */
typedef struct NPRACH_Parameters_NB_IoT_r13 {
	NPRACH_Parameters_NB_IoT_r13__nprach_Periodicity_r13	             nprach_Periodicity_r13;
	NPRACH_Parameters_NB_IoT_r13__nprach_StartTime_r13	                 nprach_StartTime_r13;
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierOffset_r13	         nprach_SubcarrierOffset_r13;
	NPRACH_Parameters_NB_IoT_r13__nprach_NumSubcarriers_r13	             nprach_NumSubcarriers_r13;
	NPRACH_Parameters_NB_IoT_r13__nprach_SubcarrierMSG3_RangeStart_r13	 nprach_SubcarrierMSG3_RangeStart_r13;
	NPRACH_Parameters_NB_IoT_r13__maxNumPreambleAttemptCE_r13	         maxNumPreambleAttemptCE_r13;
	NPRACH_Parameters_NB_IoT_r13__numRepetitionsPerPreambleAttempt_r13	 numRepetitionsPerPreambleAttempt_r13;
	NPRACH_Parameters_NB_IoT_r13__npdcch_NumRepetitions_RA_r13	         npdcch_NumRepetitions_RA_r13;
	NPRACH_Parameters_NB_IoT_r13__npdcch_StartSF_CSS_RA_r13	             npdcch_StartSF_CSS_RA_r13;
	NPRACH_Parameters_NB_IoT_r13__npdcch_Offset_RA_r13	                 npdcch_Offset_RA_r13;
} NPRACH_Parameters_NB_IoT_r13_t;

/* NPRACH-ParametersList-NB_IoT-r13 */
typedef struct NPRACH_ParametersList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct NPRACH_Parameters_NB_IoT_r13) list;
} NPRACH_ParametersList_NB_IoT_r13_t;

/* RSRP-ThresholdsNPRACH-InfoList-NB_IoT-r13 */
typedef struct RSRP_ThresholdsNPRACH_InfoList_NB_IoT_r13 {
	A_SEQUENCE_OF(RSRP_Range_t) list;
} RSRP_ThresholdsNPRACH_InfoList_NB_IoT_r13_t;

/* NPRACH-ConfigSIB-NB_IoT-r13 */
typedef struct NPRACH_ConfigSIB_NB_IoT_r13 {
	NPRACH_ConfigSIB_NB_IoT_r13__nprach_CP_Length_r13	 nprach_CP_Length_r13;
	struct RSRP_ThresholdsNPRACH_InfoList_NB_IoT_r13	*rsrp_ThresholdsPrachInfoList_r13	/* OPTIONAL */;
	NPRACH_ParametersList_NB_IoT_r13_t	                 nprach_ParametersList_r13;
} NPRACH_ConfigSIB_NB_IoT_r13_t;

/* NPDSCH-ConfigCommon-NB_IoT-r13 */
typedef struct NPDSCH_ConfigCommon_NB_IoT_r13 {
	long	 nrs_Power_r13;
} NPDSCH_ConfigCommon_NB_IoT_r13_t;

/* UL-ReferenceSignalsNPUSCH-NB_IoT-r13 */
typedef struct UL_ReferenceSignalsNPUSCH_NB_IoT_r13 {
	BOOLEAN_t	 groupHoppingEnabled_r13;
	long	     groupAssignmentNPUSCH_r13;
} UL_ReferenceSignalsNPUSCH_NB_IoT_r13_t;

/* NPUSCH-ConfigCommon-NB_IoT-r13 */
typedef struct NPUSCH_ConfigCommon_NB_IoT_r13 {
	struct NPUSCH_ConfigCommon_NB_IoT_r13__ack_NACK_NumRepetitions_Msg4_r13 {
		A_SEQUENCE_OF(ACK_NACK_NumRepetitions_NB_IoT_r13) list;
	} ack_NACK_NumRepetitions_Msg4_r13;
	NPUSCH_ConfigCommon_NB_IoT_r13__srs_SubframeConfig_r13	*srs_SubframeConfig_r13	/* OPTIONAL */;
	struct NPUSCH_ConfigCommon_NB_IoT_r13__dmrs_Config_r13 {
		long	*threeTone_BaseSequence_r13	/* OPTIONAL */;
		long	 threeTone_CyclicShift_r13;
		long	*sixTone_BaseSequence_r13	/* OPTIONAL */;
		long	 sixTone_CyclicShift_r13;
		long	*twelveTone_BaseSequence_r13	/* OPTIONAL */;
	} *dmrs_Config_r13;
	UL_ReferenceSignalsNPUSCH_NB_IoT_r13_t	 ul_ReferenceSignalsNPUSCH_r13;
} NPUSCH_ConfigCommon_NB_IoT_r13_t;

/* UplinkPowerControlCommon-NB_IoT-r13 */
typedef struct UplinkPowerControlCommon_NB_IoT_r13 {
	long	 p0_NominalNPUSCH_r13;
	UplinkPowerControlCommon_NB_IoT_r13__alpha_r13	 alpha_r13;
	long	 deltaPreambleMsg3_r13;
} UplinkPowerControlCommon_NB_IoT_r13_t;

/* NPRACH-Parameters-NB_IoT-v1330 */
typedef struct NPRACH_Parameters_NB_IoT_v1330 {
	NPRACH_Parameters_NB_IoT_v1330__nprach_NumCBRA_StartSubcarriers_r13	 nprach_NumCBRA_StartSubcarriers_r13;
} NPRACH_Parameters_NB_IoT_v1330_t;

/* NPRACH-ParametersList-NB_IoT-v1330 */
typedef struct NPRACH_ParametersList_NB_IoT_v1330 {
	A_SEQUENCE_OF(struct NPRACH_Parameters_NB_IoT_v1330) list;
} NPRACH_ParametersList_NB_IoT_v1330_t;

/* NPRACH-ConfigSIB-NB_IoT-v1330 */
typedef struct NPRACH_ConfigSIB_NB_IoT_v1330 {
	NPRACH_ParametersList_NB_IoT_v1330_t	 nprach_ParametersList_v1330;
} NPRACH_ConfigSIB_NB_IoT_v1330_t;

/* RadioResourceConfigCommonSIB-NB_IoT-r13 */
typedef struct RadioResourceConfigCommonSIB_NB_IoT_r13 {
	RACH_ConfigCommon_NB_IoT_r13_t	     rach_ConfigCommon_r13;
	BCCH_Config_NB_IoT_r13_t	         bcch_Config_r13;
	PCCH_Config_NB_IoT_r13_t	         pcch_Config_r13;
	NPRACH_ConfigSIB_NB_IoT_r13_t	     nprach_Config_r13;
	NPDSCH_ConfigCommon_NB_IoT_r13_t	 npdsch_ConfigCommon_r13;
	NPUSCH_ConfigCommon_NB_IoT_r13_t	 npusch_ConfigCommon_r13;
	struct DL_GapConfig_NB_IoT_r13	     *dl_Gap_r13	/* OPTIONAL */;
	UplinkPowerControlCommon_NB_IoT_r13_t	 uplinkPowerControlCommon_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
	struct RadioResourceConfigCommonSIB_NB_IoT_r13__ext1 {
		struct NPRACH_ConfigSIB_NB_IoT_v1330	*nprach_Config_v1330	/* OPTIONAL */;
	} *ext1;
} RadioResourceConfigCommonSIB_NB_IoT_r13_t;

/* UE-TimersAndConstants-NB_IoT-r13 */
typedef struct UE_TimersAndConstants_NB_IoT_r13 {
	UE_TimersAndConstants_NB_IoT_r13__t300_r13	 t300_r13;
	UE_TimersAndConstants_NB_IoT_r13__t301_r13	 t301_r13;
	UE_TimersAndConstants_NB_IoT_r13__t310_r13	 t310_r13;
	UE_TimersAndConstants_NB_IoT_r13__n310_r13	 n310_r13;
	UE_TimersAndConstants_NB_IoT_r13__t311_r13	 t311_r13;
	UE_TimersAndConstants_NB_IoT_r13__n311_r13	 n311_r13;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} UE_TimersAndConstants_NB_IoT_r13_t;

/* NS-PmaxValue-NB_IoT-r13 */
typedef struct NS_PmaxValue_NB_IoT_r13 {
	P_Max_t	*additionalPmax_r13	/* OPTIONAL */;
	AdditionalSpectrumEmission_t	 additionalSpectrumEmission_r13;
} NS_PmaxValue_NB_IoT_r13_t;

/* NS-PmaxList-NB_IoT-r13 */
typedef struct NS_PmaxList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct NS_PmaxValue_NB_IoT_r13) list;
} NS_PmaxList_NB_IoT_r13_t;

/* SystemInformatioNB_IoTlockType3-NB_IoT-r13 */
typedef struct SystemInformatioNB_IoTlockType3_NB_IoT_r13 {
	struct SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionInfoCommon_r13 {
		long	 q_Hyst_r13;
	} cellReselectionInfoCommon_r13;
	struct SystemInformatioNB_IoTlockType3_NB_IoT_r13__cellReselectionServingFreqInfo_r13 {
		ReselectionThreshold_t	 s_NonIntraSearch_r13;
	} cellReselectionServingFreqInfo_r13;
	struct SystemInformatioNB_IoTlockType3_NB_IoT_r13__intraFreqCellReselectionInfo_r13 {
		Q_RxLevMin_t	 q_RxLevMin_r13;
		Q_QualMin_r9_t	*q_QualMin_r13	/* OPTIONAL */;
		P_Max_t	*p_Max_r13	/* OPTIONAL */;
		ReselectionThreshold_t	 s_IntraSearchP_r13;
		T_Reselection_NB_IoT_r13	 t_Reselection_r13;
	} intraFreqCellReselectionInfo_r13;
	struct NS_PmaxList_NB_IoT_r13	*freqBandInfo_r13	/* OPTIONAL */;
	struct SystemInformatioNB_IoTlockType3_NB_IoT_r13__multiBandInfoList_r13 {
		A_SEQUENCE_OF(struct NS_PmaxList_NB_IoT_r13) list;
	} *multiBandInfoList_r13;
	OCTET_STRING_t	*lateNonCriticalExtension	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} SystemInformatioNB_IoTlockType3_NB_IoT_r13_t;

/* SystemInformatioNB_IoTlockType2-NB_IoT-r13 */
typedef struct SystemInformatioNB_IoTlockType2_NB_IoT_r13 {
	RadioResourceConfigCommonSIB_NB_IoT_r13_t	 radioResourceConfigCommon_r13;
	UE_TimersAndConstants_NB_IoT_r13_t	 ue_TimersAndConstants_r13;
	struct SystemInformatioNB_IoTlockType2_NB_IoT_r13__freqInfo_r13 {
		struct CarrierFreq_NB_IoT_r13	*ul_CarrierFreq_r13	/* OPTIONAL */;
		AdditionalSpectrumEmission_t	 additionalSpectrumEmission_r13;
	} freqInfo_r13;
	TimeAlignmentTimer_t	 timeAlignmentTimerCommon_r13;
	struct SystemInformatioNB_IoTlockType2_NB_IoT_r13__multiBandInfoList_r13 {
		A_SEQUENCE_OF(AdditionalSpectrumEmission_t) list;
	} *multiBandInfoList_r13;
	OCTET_STRING_t	*lateNonCriticalExtension	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
} SystemInformatioNB_IoTlockType2_NB_IoT_r13_t;

/* SystemInformation-NB_IoT-r13-IEs */
typedef struct SystemInformation_NB_IoT_r13_IEs {
	struct SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13 {
		//A_SEQUENCE_OF(struct SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member {
			SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_PR present;
			union SystemInformation_NB_IoT_r13_IEs__sib_TypeAndInfo_r13__Member_u {
				SystemInformatioNB_IoTlockType2_NB_IoT_r13_t	 sib2_r13;
				SystemInformatioNB_IoTlockType3_NB_IoT_r13_t	 sib3_r13;
                //SystemInformatioNB_IoTlockType4_NB_IoT_r13_t	 sib4_r13;
				//SystemInformatioNB_IoTlockType5_NB_IoT_r13_t	 sib5_r13;
				//SystemInformatioNB_IoTlockType14_NB_IoT_r13_t	 sib14_r13;
				//SystemInformatioNB_IoTlockType16_NB_IoT_r13_t	 sib16_r13;
				/*
				 * This type is extensible,
				 * possible extensions are below.
				 */
			} choice;
		//} ) list;
	} sib_TypeAndInfo_r13;
	OCTET_STRING_t	*lateNonCriticalExtension	/* OPTIONAL */;
	struct SystemInformation_NB_IoT_r13_IEs__nonCriticalExtension {
	} *nonCriticalExtension;
} SystemInformation_NB_IoT_r13_IEs_t;

///-----------------------------------------Below---S I B 1 ---Structure-----------------------------------------///

/* MNC */
typedef struct MNC {
	A_SEQUENCE_OF(MCC_MNC_Digit_t) list;
} MNC_t;

/* MCC */
typedef struct MCC {
	A_SEQUENCE_OF(MCC_MNC_Digit_t) list;
} MCC_t;

/* PLMN-Identity */
typedef struct PLMN_Identity {
	struct MCC	*mcc	/* OPTIONAL */;
	MNC_t	 mnc;
} PLMN_Identity_t;

/* PLMN-IdentityInfo-NB_IoT-r13 */
typedef struct PLMN_IdentityInfo_NB_IoT_r13 {
	PLMN_Identity_t	 plmn_Identity_r13;
	PLMN_IdentityInfo_NB_IoT_r13__cellReservedForOperatorUse_r13	 cellReservedForOperatorUse_r13;
	PLMN_IdentityInfo_NB_IoT_r13__attachWithoutPDN_Connectivity_r13	 *attachWithoutPDN_Connectivity_r13	/* OPTIONAL */;
} PLMN_IdentityInfo_NB_IoT_r13_t;

/* PLMN-IdentityList-NB_IoT-r13 */
typedef struct PLMN_IdentityList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct PLMN_IdentityInfo_NB_IoT_r13) list;
} PLMN_IdentityList_NB_IoT_r13_t;

/* SIB-MappingInfo-NB_IoT-r13 */
typedef struct SIB_MappingInfo_NB_IoT_r13 {
	A_SEQUENCE_OF(SIB_Type_NB_IoT_r13) list;
} SIB_MappingInfo_NB_IoT_r13_t;

/* SchedulingInfo-NB_IoT-r13 */
typedef struct SchedulingInfo_NB_IoT_r13 {
	SchedulingInfo_NB_IoT_r13__si_Periodicity_r13	     si_Periodicity_r13;
	SchedulingInfo_NB_IoT_r13__si_RepetitionPattern_r13	 si_RepetitionPattern_r13;
	SIB_MappingInfo_NB_IoT_r13_t	                     sib_MappingInfo_r13;
	SchedulingInfo_NB_IoT_r13__si_TB_r13	             si_TB_r13;
} SchedulingInfo_NB_IoT_r13_t;

/* SchedulingInfoList-NB_IoT-r13 */
typedef struct SchedulingInfoList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct SchedulingInfo_NB_IoT_r13) list;
} SchedulingInfoList_NB_IoT_r13_t;

/* MultiBandInfo-NB_IoT-r13 */
typedef struct MultiBandInfo_NB_IoT_r13 {
	FreqBandIndicator_NB_IoT_r13_t	*freqBandIndicator_r13	/* OPTIONAL */;
	struct NS_PmaxList_NB_IoT_r13	*freqBandInfo_r13	/* OPTIONAL */;
} MultiBandInfo_NB_IoT_r13_t;

/* MultiBandInfoList-NB_IoT-r13 */
typedef struct MultiBandInfoList_NB_IoT_r13 {
	A_SEQUENCE_OF(struct MultiBandInfo_NB_IoT_r13) list;
} MultiBandInfoList_NB_IoT_r13_t;

/* SystemInfoValueTagList-NB_IoT-r13 */
typedef struct SystemInfoValueTagList_NB_IoT_r13 {
	A_SEQUENCE_OF(SystemInfoValueTagSI_r13_t) list;
} SystemInfoValueTagList_NB_IoT_r13_t;


///-----------------------------------------Below---BCCH_DL_SCH---Structure-----------------------------------------///

/* SystemInformatioNB_IoTlockType1-NB_IoT */
typedef struct SystemInformatioNB_IoTlockType1_NB_IoT {
	BIT_STRING_t	 hyperSFN_MSB_r13;
	struct SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13 {
		PLMN_IdentityList_NB_IoT_r13_t	 plmn_IdentityList_r13;
		TrackingAreaCode_t	 trackingAreaCode_r13;
		CellIdentity_t	 cellIdentity_r13;
		SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__cellBarred_r13       	 cellBarred_r13;
		SystemInformatioNB_IoTlockType1_NB_IoT__cellAccessRelatedInfo_r13__intraFreqReselection_r13	 intraFreqReselection_r13;
	} cellAccessRelatedInfo_r13;
	struct SystemInformatioNB_IoTlockType1_NB_IoT__cellSelectionInfo_r13 {
		Q_RxLevMin_t	 q_RxLevMin_r13;
		Q_QualMin_r9_t	 q_QualMin_r13;
	} cellSelectionInfo_r13;
	P_Max_t	*p_Max_r13	/* OPTIONAL */;
	FreqBandIndicator_NB_IoT_r13_t	 freqBandIndicator_r13;
	struct NS_PmaxList_NB_IoT_r13	*freqBandInfo_r13	/* OPTIONAL */;
	struct MultiBandInfoList_NB_IoT_r13	*multiBandInfoList_r13	/* OPTIONAL */;
	struct DL_Bitmap_NB_IoT_r13	*downlinkBitmap_r13	/* OPTIONAL */;

	SystemInformatioNB_IoTlockType1_NB_IoT__eutraControlRegionSize_r13	*eutraControlRegionSize_r13	/* OPTIONAL */;
	SystemInformatioNB_IoTlockType1_NB_IoT__nrs_CRS_PowerOffset_r13	    *nrs_CRS_PowerOffset_r13	/* OPTIONAL */;
	SchedulingInfoList_NB_IoT_r13_t	                                *schedulingInfoList_r13;
	SystemInformatioNB_IoTlockType1_NB_IoT__si_WindowLength_r13	        si_WindowLength_r13;

	long	*si_RadioFrameOffset_r13	/* OPTIONAL */;
	struct SystemInfoValueTagList_NB_IoT_r13	*systemInfoValueTagList_r13	/* OPTIONAL */;
	OCTET_STRING_t	*lateNonCriticalExtension	/* OPTIONAL */;
} SystemInformatioNB_IoTlockType1_NB_IoT_t;

/* SystemInformation-NB_IoT */
typedef struct SystemInformation_NB_IoT {
	struct SystemInformation_NB_IoT__criticalExtensions {
		SystemInformation_NB_IoT__criticalExtensions_PR present;
		union SystemInformation_NB_IoT__criticalExtensions_u {
			SystemInformation_NB_IoT_r13_IEs_t	 systemInformation_r13;
            struct SystemInformation_NB_IoT__criticalExtensions__criticalExtensionsFuture {
			} criticalExtensionsFuture;
		} choice;
	} criticalExtensions;
} SystemInformation_NB_IoT_t;

/* BCCH-DL-SCH-MessageType-NB_IoT */
typedef struct BCCH_DL_SCH_MessageType_NB_IoT {
	BCCH_DL_SCH_MessageType_NB_IoT_PR present;
	union BCCH_DL_SCH_MessageType_NB_IoT_u {
		struct BCCH_DL_SCH_MessageType_NB_IoT__c1 {
			BCCH_DL_SCH_MessageType_NB_IoT__c1_PR present;
			union BCCH_DL_SCH_MessageType_NB_IoT__c1_u {
				SystemInformation_NB_IoT_t	 systemInformation_r13;
				SystemInformatioNB_IoTlockType1_NB_IoT_t	 systemInformatioNB_IoTlockType1_r13;
			}choice;
		}c1;
        struct BCCH_DL_SCH_MessageType_NB_IoT__messageClassExtension {
		} messageClassExtension;
	} choice;
} BCCH_DL_SCH_MessageType_NB_IoT_t;

/* BCCH-DL-SCH-Message-NB_IoT */
typedef struct BCCH_DL_SCH_Message_NB_IoT {
	BCCH_DL_SCH_MessageType_NB_IoT_t	 message;
} BCCH_DL_SCH_Message_NB_IoT_t;

///-----------------------------------------Below---M I B---Structure-----------------------------------------///

/* Standalone-NB_IoT-r13 */
typedef struct Standalone_NB_IoT_r13 {
	BIT_STRING_t	 spare;
} Standalone_NB_IoT_r13_t;

typedef struct Guardband_NB_IoT_r13 {
	ChannelRasterOffset_NB_IoT_r13	 rasterOffset_r13;
	BIT_STRING_t	 spare;
} Guardband_NB_IoT_r13_t;

/* INB_IoTand-DifferentPCI-NB_IoT-r13 */
typedef struct INB_IoTand_DifferentPCI_NB_IoT_r13 {
	INB_IoTand_DifferentPCI_NB_IoT_r13__eutra_NumCRS_Ports_r13	 eutra_NumCRS_Ports_r13;
	ChannelRasterOffset_NB_IoT_r13	 rasterOffset_r13;
	BIT_STRING_t	 spare;
} INB_IoTand_DifferentPCI_NB_IoT_r13_t;

/* INB_IoTand-SamePCI-NB_IoT-r13 */
typedef struct INB_IoTand_SamePCI_NB_IoT_r13 {
	long	 eutra_CRS_SequenceInfo_r13;
} INB_IoTand_SamePCI_NB_IoT_r13_t;

/* MasterInformatioNB_IoTlock-NB_IoT */
typedef struct MasterInformatioNB_IoTlock_NB_IoT {
	BIT_STRING_t	 systemFrameNumber_MSB_r13;
	BIT_STRING_t	 hyperSFN_LSB_r13;
	long	 schedulingInfoSIB1_r13;
	long	 systemInfoValueTag_r13;
	BOOLEAN_t	 ab_Enabled_r13;
	struct MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13 {
		MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_PR present;
		union MasterInformatioNB_IoTlock_NB_IoT__operationModeInfo_r13_u {
			INB_IoTand_SamePCI_NB_IoT_r13_t	 iNB_IoTand_SamePCI_r13;
			INB_IoTand_DifferentPCI_NB_IoT_r13_t	 iNB_IoTand_DifferentPCI_r13;
			Guardband_NB_IoT_r13_t	 guardband_r13;
			Standalone_NB_IoT_r13_t	 standalone_r13;
		} choice;
	} operationModeInfo_r13;
	BIT_STRING_t	 spare;
} MasterInformatioNB_IoTlock_NB_IoT_t;

///-----------------------------------------Below---BCCH_BCH---Structure-----------------------------------------///

typedef MasterInformatioNB_IoTlock_NB_IoT_t	 BCCH_BCH_MessageType_NB_IoT_t;

/* BCCH-BCH-Message-NB_IoT */
typedef struct BCCH_BCH_Message_NB_IoT {
	BCCH_BCH_MessageType_NB_IoT_t	message;
} BCCH_BCH_Message_NB_IoT_t;


BCCH_BCH_Message_NB_IoT_t               MIB;
BCCH_DL_SCH_Message_NB_IoT_t            SIB;
RRCConnectionSetup_NB_IoT_r13_IEs_t     DED_Config;



