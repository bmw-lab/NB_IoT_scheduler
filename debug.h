#ifndef _DEBUG_H_
#define _DEBUG_H_

eNB_MAC_INST_NB_IoT *___mac_inst;
typedef struct {
	uint32_t arrive_time;
	uint32_t ul_or_dl;//0:UL 1:DL
	uint32_t data_size;
	uint32_t CE_level;
	uint32_t preamble_index;
	uint16_t tc_rnti;
	uint32_t clean_flag;
}UE_info_sim_t;
#define debug_period_start	0
#define debug_period_end	100000

#define	DEBUG_FUNCTION
#undef	DEBUG_FUNCTION

//#define	DEBUG_TIMING_GENERATOR
#undef	DEBUG_TIMING_GENERATOR

#define current_time_stamp ___mac_inst->current_subframe

#define DEBUG(str_, ...)	if(current_time_stamp >= debug_period_start && current_time_stamp <= debug_period_end)  printf((str_), ##__VA_ARGS__ )

#define SCHEDULE_LOG(str_, ...)	fprintf(___mac_inst->schedule_log, (str_), ##__VA_ARGS__ )
	
#ifdef DEBUG_FUNCTION
	#define DEBUG_FUNCTION_IN(str_, ...)	printf("[%04d][FUNCTION]"str_"[IN]\n", current_time_stamp, ##__VA_ARGS__ )
	#define DEBUG_FUNCTION_OUT(str_, ...)	printf("[%04d][FUNCTION]"str_"[OUT]\n", current_time_stamp, ##__VA_ARGS__ )
#else
	#define DEBUG_FUNCTION_IN(str_, ...)
	#define DEBUG_FUNCTION_OUT(str_, ...)
#endif
/*
#ifdef DEBUG_TIMING_GENERATOR
	#define TIMING_DIAGRAM_DL dl_scheduled
#else
	#define TIMING_DIAGRAM_DL 
#endif*/


#endif
