﻿NB-IoT MAC Scheduler implementation

BMW lab./NTUST

Advisor : Prof.Ray Guang, Cheng 	crg@mail.ntust.edu.tw

Gitlab maintain : Nick (nick133371@gmail.com)
If you have any question related to this project, please do not hestitate to discuss with us!

Team member : 

	Xavier (sephiroth7277@gmail.com)
	Calvin (calvin820119@gmail.com)
	Kenny (kroempa@gmail.com)
	Nick (nick133371@gmail.com)
	Alan (rely4679@gmail.com)

Responsible block:

	Fixed Schedule MIB SIB1 – Kenny
	Architecture, RA, Schedule SIs – Calvin
	Schedule DL – Xavier、Calvin
	Schedule UL – Nick、Kenny
	Output handler – Nick
	Test environment / verification – Alan	

File description:
	[implementation files]
		DL_scheduler/scheduler_dlsch.c		
		[USS DL scheduler]
		UL_scheduler/scheduler_ulsch.c		
		[USS UL scheduler]
		RA_scheduler/schedule_ra.c			
		[RA scheduler(including MSG2, MSG3, MSG4, HARQ)]
		SIBs_scheduler/schedule_sibs.c		
		[SI message scheduler]
		RLC/RLC.c						
		[MAC RLC primitives]
		RRC/config.c		
		[MAC RRC primitives]

		IF_Module/IF_Module.c
		[Interface Module contains primitives]

		eNB_scheduler_nb_iot.c				
		[MAC scheudler]
		output_handler.c					
		[MAC output handler]
		tool.c								
		[MAC tools function(including functions used by each scheduler)]
		debug.c								
		[develop tools]
		nb_iot_mac.c						
		[MAC entity functions]
		testbench.c							
		[MAC testbench]

	[header files]
		defs_nb_iot.h				
		[main type definition]
		proto_nb_iot.h				
		[functions definition]
		debug.h						
		[develop tools definition]

Test:
	make && make run 

Test result:
	/out/timing_diagram/index.html visualize the timing diagram
	/out/schedule_result the text file to show simulation result

For git testing.