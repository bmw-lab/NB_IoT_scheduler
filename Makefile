obj-m:=nb_iot_mac.o
PWD_DIR	:= $(shell pwd)
RA_DIR	:= $(PWD)/RA_scheduler
UL_DIR  := $(PWD)/UL_scheduler
DL_DIR  := $(PWD)/DL_scheduler
SIBS_DIR:= $(PWD)/SIBs_scheduler
OUT_DIR	:= $(PWD)/out
TIME_DIR:= $(OUT_DIR)/timing_diagram

all:
	gcc -o $(OUT_DIR)/nb_iot_mac.o $(PWD_DIR)/debug.c  $(PWD_DIR)/testbench.c  NB_IoT_mac.c tool.c $(UL_DIR)/scheduler_ulsch.c $(DL_DIR)/scheduler_dlsch.c $(RA_DIR)/schedule_ra.c $(SIBS_DIR)/schedule_sibs.c $(PWD_DIR)/eNB_scheduler_NB_IoT.c $(PWD_DIR)/output_handler.c $(TIME_DIR)/src/render.c $(PWD_DIR)/RRC/config.c -lm
run:
	$(OUT_DIR)/nb_iot_mac.o
clean:
	rm -rf $(OUT_DIR)/*.o
