#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "defs_NB_IoT.h"
#include "proto_NB_IoT.h"
#include "debug.h"

#define RA_PERIOD	320
#define RA_START_TIME	8
uint32_t UE_NUM_SIM=10;

extern simulate_rx_t *simulate_rx_msg3_list;
extern simulate_rx_t *simulate_rx_msg4_list;

////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////
#ifdef TIMING_GENERATOR
#include "out/timing_diagram/src/types.h"
#include "out/timing_diagram/src/render.h"
#define BV(X) (0x1<<X)
uint8_t number_char[17] = "0123456789ABCDEF";
//	user define sibs message for UI
uint8_t sibs_str[6][20] = { "LIST1", "LIST2", "LIST3", "LIST4", "LIST5", "LIST6"};
extern resource_t *dl_scheduled_bitmap;
extern resource_t *ul_scheduled_bitmap[48];
uint8_t output_file_name[] = "out/timing_diagram/output.html";
uint8_t template_file_name[] = "out/timing_diagram/ref/template.html";
#endif
////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////

#define UE_LOG(str_, ...)	fprintf(ue_file, (str_), ##__VA_ARGS__ )

////////////Global Variable//////////////////
uint8_t ue_file_name[] = "out/ue_info.txt";
//UE_info_sim_t *UE_info_sim;
// numeber of USS list for simulation
#define NUM_UE_LIST 3
// numeber of UE
#define NUM_TOTAL_UE 10
//Function Decalartion for simulation
void simulation_initialization(eNB_MAC_INST_NB_IoT *mac_inst);
void simulation_init_schedule_success_list();
void simulation_add_ue_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID, uint32_t start_sf, uint32_t end_sf);
void simulation_remove_ue_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID, uint8_t CE_level);
void simulation_print_UE_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst);
void simulation_check_retransmission_timing(eNB_MAC_INST_NB_IoT *mac_inst);
void simulation_USS_HARQ_feedback(eNB_MAC_INST_NB_IoT *mac_inst, uint8_t flag_ack_nack, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID);
void simulation_add_ue(eNB_MAC_INST_NB_IoT *mac_inst, int input_ue_id, int request_times);
void simulation_check_request(eNB_MAC_INST_NB_IoT *mac_inst, uint32_t current_subframe);
void simulation_new_ue(UE_list_NB_IoT_t *UE_list_target, int input_ue_id, uint16_t data_size, int UE_template_id_prev);
///////////Structure////////////////////
// input structure
// UE information 
typedef struct UE_info{
	//UE ID
	int unique_id;
	// Connection time
	int request_subframe[10];
	// single tone 0/multitone 1
	uint8_t multi_tone;
	// index of data volume [0-15] for each request
	uint8_t data_volume[10];
	// size of data for each request
	uint16_t data_size[10];
	// Coverage enhancement level [0-2]
	uint8_t CE_level;
	// mAh [0-100000]
	uint16_t battery_report_format1;
	// Percentage [0-100] 
	uint8_t battery_report_format2; 
	// number of connection time
	uint8_t num_connection;
	// path loss
	float path_loss;
}UE_INFO_t;

// Transmission success array;
typedef struct transmission_success_array{
	
}TRANSMISSION_SUCCESS_ARRAY_t;

typedef struct input_struct{
	// UE info
	UE_INFO_t UE_information[NUM_TOTAL_UE];
	// successful rate
	int** successful_rate_ul;
	int* successful_rate_dl;
}INPUT_STRUCTURE_t;

INPUT_STRUCTURE_t input_struct;

#if SIMULATION_TEST==1
typedef struct schedule_success_ue{
	uint32_t UE_template_id;
	uint32_t crnti;
	uint8_t CE_level;
	//0:UL 1:DL
	uint8_t transmission_direction;
	//starting subframe of harq feedback DL
	uint32_t ack_nack_start_sf_dl;
	//ending subframe of harq feedback DL
	uint32_t ack_nack_end_sf_dl;
	//ending subframe of UL packet
	uint32_t data_end_sf_ul;
	struct schedule_success_ue *next;
	struct schedule_success_ue *prev;
}SCHEDULE_SUCCESS_UE_t;

typedef struct schedule_success_ue_list{
	SCHEDULE_SUCCESS_UE_t *head;
	SCHEDULE_SUCCESS_UE_t *tail;
	uint32_t number_ue_in_list;
}SCHEDULE_SUCCESS_UE_LIST_t;

SCHEDULE_SUCCESS_UE_LIST_t schedule_success_list;
#endif

int total_UE_count;

FILE *ue_file;

int main(int argc, char *argv[]){
	uint8_t str[20];
	FILE *fo, *fi;
	
	uint32_t i, ii;
	
	printf("NB_IoT-IoT simulator\n");

	eNB_MAC_INST_NB_IoT mac_inst;
	uint32_t current_time;
	simulate_rx_t *iterator, *iterator1;

    srand(time(NULL));

////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////
#ifdef TIMING_GENERATOR
	render_t render;
	render.printer[0] = (printer_t *)0;
	render.printer[1] = (printer_t *)0;
	render.printer[2] = (printer_t *)0;
	render.printer[3] = (printer_t *)0;
	
	if( (FILE *)0 == (fo = fopen(output_file_name, "w"))){
		printf("[0x1]failed to open %s!\n", output_file_name);
		exit(1);
	}
	if( (FILE *)0 == (fi = fopen(template_file_name, "r"))){
		printf("[0x2]failed to open %s!\n", template_file_name);
		exit(1);
	}
	
	dl_scheduled_bitmap = (resource_t *)calloc(sim_end_time, sizeof(resource_t));
	for(ii=0;ii<48;++ii){
		ul_scheduled_bitmap[ii] = (resource_t *)calloc(sim_end_time, sizeof(resource_t));
	}	
#endif
////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////

    if( (FILE *)0 == (ue_file = fopen(ue_file_name, "w"))){
    	printf("[0x2]failed to open %s!\n", ue_file_name);
    	exit(1);
    }

    //UE_LOG("------UE Information-------\n");
	//UE_LOG("UE_ID\tarrive_time\tUL_DL\tCE_level\tpreamble_index\tdata_size(bytes)\tT-CRNTI\n");

	rrc_config_NB_IoT_t rrc_inst;
	mac_inst.current_subframe = 0;
	// Initailization
	init_rrc_NB_IoT();
	init_debug(&mac_inst);
	init_mac_NB_IoT(&mac_inst, &rrc_inst);
	register_mac_inst(&mac_inst, 0);
	simulation_initialization(&mac_inst);
	printf("finished initialize process\n");
#if 0
	//initialize UE info for simulation
	UE_info_sim = (UE_info_sim_t*)malloc(UE_NUM_SIM*sizeof(UE_info_sim_t));
	int ue_index;

	for(ue_index=0;ue_index<UE_NUM_SIM;++ue_index)
	{
		UE_info_sim[ue_index].arrive_time = 2;//2;
		UE_info_sim[ue_index].ul_or_dl = (uint32_t)rand()%2;
		UE_info_sim[ue_index].CE_level = 0;
		UE_info_sim[ue_index].data_size = (uint32_t)(rand()%681/8);
		UE_info_sim[ue_index].preamble_index=1+2*ue_index;
		UE_info_sim[ue_index].clean_flag = 0;
		UE_info_sim[ue_index].tc_rnti = 0x0101+ue_index;
		UE_LOG("%d\t%d\t\t%d\t%d\t\t%d\t\t%d\t\t\t%d\n", ue_index, UE_info_sim[ue_index].arrive_time, UE_info_sim[ue_index].ul_or_dl, UE_info_sim[ue_index].CE_level, UE_info_sim[ue_index].preamble_index, UE_info_sim[ue_index].data_size, UE_info_sim[ue_index].tc_rnti);
	}
#endif

	for(current_time=0; current_time<=sim_end_time; ++current_time){

		//	subframe
		i = current_time%MAX_SUBFRAME;

		//	20 bit frame
		mac_inst.current_subframe = i;
		
////////////////////////NTUST DEBUGGING USAGE///////////////////////////
	#if 0
		//	simulate UE behavior
		if((i%RA_PERIOD)==RA_START_TIME){
			for(ue_index=0;ue_index<UE_NUM_SIM;++ue_index)
			{
				if(UE_info_sim[ue_index].clean_flag==1)
					continue;
				if(UE_info_sim[ue_index].arrive_time<=i)
				{
					UE_info_sim[ue_index].clean_flag = 1;
					init_RA_NB_IoT(&mac_inst, UE_info_sim[ue_index].preamble_index, UE_info_sim[ue_index].CE_level, i/10, 24);
				}
			}
		}
		
		//	simulate UE transmit msg3
		iterator = simulate_rx_msg3_list;
		if((simulate_rx_t *)0 != simulate_rx_msg3_list){
            while(i==iterator->rx_abs_subframe){
				iterator1 = iterator;
				//if((rand()&0x7)>4){
				if(1){
					// successful receive msg3 prob.
					// function should be put in rx_sdu
					DEBUG("[%d][simulate] tx msg3 success!\n", mac_inst.current_subframe);

					for(ue_index=0;ue_index<UE_NUM_SIM;++ue_index)
					{
						if(iterator->rnti==UE_info_sim[ue_index].tc_rnti)
							break;
						if(ue_index==3)
							printf("none of this rnti\n");
					}
					if(UE_info_sim[ue_index].ul_or_dl==0)
					{
						receive_msg3_NB_IoT(&mac_inst, iterator->rnti, 0, UE_info_sim[ue_index].data_size);
					}
					else
					{
						receive_msg3_NB_IoT(&mac_inst, iterator->rnti, 0, 0);
					}
				}else{
				    msg3_do_retransmit_NB_IoT(&mac_inst, iterator->rnti);
				    DEBUG("[%d][simulate] tx msg3 failed!\n", mac_inst.current_subframe);
                }
				iterator = iterator->next;
				free(iterator1);
				if((simulate_rx_t *)0 == iterator)
					break;
			}
			simulate_rx_msg3_list = iterator;
		}
		
        //	simulate UE Receive msg4, then transmit ack
		iterator = simulate_rx_msg4_list;
		if((simulate_rx_t *)0 != simulate_rx_msg4_list){
            while(i==iterator->rx_abs_subframe){
                
				iterator1 = iterator;
				//if((rand()&0x7)>4){
				if(1){
					// successful receive msg4 ack prob.
					// function should be put in rx_sdu
					receive_msg4_ack_NB_IoT(&mac_inst, iterator->rnti);
					DEBUG("[%d][simulate] tx msg4 success!\n", mac_inst.current_subframe);
				}else{
				    msg4_do_retransmit_NB_IoT(&mac_inst, iterator->rnti);
					DEBUG("[%d][simulate] tx msg4 failed!\n", mac_inst.current_subframe);
				}
				iterator = iterator->next;
				free(iterator1);
				if((simulate_rx_t *)0 == iterator)
					break;
			}
			simulate_rx_msg4_list = iterator;
		}
////////////////////////NTUST DEBUGGING USAGE///////////////////////////
	#endif
        //DEBUG("[%d][simulate] In USS_scheduling_module\n", mac_inst.current_subframe);
		//eNB_dlsch_ulsch_scheduler_NB_IoT(&mac_inst, i);
		USS_scheduling_module(&mac_inst, i, NUM_UE_LIST);
		simulation_check_retransmission_timing(&mac_inst);
		simulation_print_UE_schedule_success_list(&mac_inst);
		//in check request function
		//DEBUG("[%d][simulate] In check request function\n", mac_inst.current_subframe);
		if(mac_inst.current_subframe!=0)
		{
			simulation_check_request(&mac_inst, mac_inst.current_subframe);
		}
		//print_UE_schedule_success_list(&mac_inst);
		//eNB_dlsch_ulsch_scheduler_NB_IoT(&mac_inst, i);
		//print_available_UL_resource();
#ifdef TIMING_GENERATOR
        //  PHY
        if(i%10==5){
            dl_scheduled(i, 0, _NPSS, 0x0, (char *)0, 0, -1, -1, -1);
        }
        if(i%10==9 && ((i/10)&0x1)==0){
            dl_scheduled(i, 0, _NSSS, 0x0, (char *)0, 0, -1, -1, -1);
        }
#endif
	fflush(mac_inst.schedule_result);
	fflush(mac_inst.schedule_log);
	fflush(ue_file);
	

	}
	UE_LOG("\n\nUL Throughput\n");
	if(sim_end_time<endtime_throughput)
	{
		UE_LOG("Total transmit UL TBS %d bytes | Ending time %d | UL throughput %d bytes/mS\n", throughput_UL, endtime_throughput, throughput_UL/endtime_throughput);
	}
	else
	{
		UE_LOG("Total transmit UL TBS %d bytes | Ending time %d | UL throughput %d bytes/mS\n", throughput_UL, sim_end_time, throughput_UL/endtime_throughput);
	}

	UE_LOG("\n\nDL Throughput\n");
	if(sim_end_time<endtime_throughput)
	{
		UE_LOG("Total transmit DL TBS %d bytes | Ending time %d | DL throughput %d bytes/mS\n", throughput_DL, endtime_throughput, throughput_DL/endtime_throughput);
	}
	else
	{
		UE_LOG("Total transmit DL TBS %d bytes | Ending time %d | DL throughput %d bytes/mS\n", throughput_DL, sim_end_time, throughput_DL/endtime_throughput);
	}
	UE_LOG("\n\nUL resource utilization\n");
	uint32_t total_num_ul_resource, times_of_prach, total_prach_occupy;
	if(sim_end_time<endtime_throughput)
	{
		total_num_ul_resource=12*endtime_throughput;
		times_of_prach=(endtime_throughput-nprach_list->nprach_StartTime)/nprach_list->nprach_Periodicity;
	}
	else
	{
		total_num_ul_resource=12*sim_end_time;
		times_of_prach=(sim_end_time-nprach_list->nprach_StartTime)/nprach_list->nprach_Periodicity;
	}
	for(i=0;i<3;++i)
	{
		total_prach_occupy+=times_of_prach*ceil( (nprach_list+i)->nprach_StartTime + 1.4*4*((nprach_list+i)->numRepetitionsPerPreambleAttempt) );
	}
	//UE_LOG("total prach occupy %d\n", total_prach_occupy);
	UE_LOG("Total UL resource %d |number of occupied UL subframe %d | UL resource utilization %f %\n", total_num_ul_resource-total_prach_occupy, num_occupied_ul_resource, (double)num_occupied_ul_resource/(double)total_num_ul_resource*100);

	UE_LOG("\n\nDL resource utilization\n");
	uint32_t num_dlsf=0;
	if(sim_end_time<endtime_throughput)
	{
		for(i=0;i<=endtime_throughput;++i)
		{
			if(is_dlsf(&mac_inst, i)==1)
			{
				num_dlsf++;
			}
		}
	}
	else
	{
		for(i=0;i<=sim_end_time;++i)
		{
			if(is_dlsf(&mac_inst, i)==1)
			{
				num_dlsf++;
			}
		}
	}
	UE_LOG("Total NB-IoT DL subframe %d |number of occupied NB-IoT DL subframe %d | DL resource utilization %f %\n", num_dlsf, num_occupied_dlsf_subframe, (double)num_occupied_dlsf_subframe/(double)num_dlsf*100);
	
	print_available_resource_DL(&mac_inst);
	
	printf("finished run time simulation\n");
	
	fflush(mac_inst.schedule_result);
	fflush(mac_inst.schedule_log);
	fclose(mac_inst.schedule_result);
    fclose(mac_inst.schedule_log);
	////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////
	#ifdef TIMING_GENERATOR
	sprintf(str, "%d\n", 30*sim_end_time);
	printf("init render ..\n");
	init_render(&render);
	printf("rendering into .html ..\n");
	render_html(&render, PRINTER_1, str);
	printf("output to .html ..\n");
	output_html(&render, sim_end_time, fi, fo);//, dl_scheduled_bitmap, ul_scheduled_bitmap);
	printf("finished ..\n");
	fclose(fo);
	//fclose(fi);
	#endif
	////////////////////////CALVIN TIMING DIAGRAM GENERATOR///////////////////////////
	
	return 0;
}
#if SIMULATION_TEST==1
void simulation_initialization(eNB_MAC_INST_NB_IoT *mac_inst)
{
	int i,j,k;
	//UE_TEMPLATE_NB_IoT *UE_template_temp1, *UE_template_temp2;
	UE_TEMPLATE_NB_IoT *UE_template_temp1;
	UE_TEMPLATE_NB_IoT **UE_template_prev=(UE_TEMPLATE_NB_IoT**)malloc(NUM_UE_LIST*sizeof(UE_TEMPLATE_NB_IoT));
	//int ue_id_temp; 
	//int *ue_id_temp=(int*)malloc(NUM_UE_LIST*sizeof(int);

	uint16_t num_ue_in_list[3]={0};

	mac_inst->schedule_log_sim = fopen("out/schedule_log_sim.txt", "w");
	//totoal 3 CE level, generate 3 UE lists
	mac_inst->UE_list_spec=(UE_list_NB_IoT_t*)malloc(NUM_UE_LIST*sizeof(UE_list_NB_IoT_t));
	// start up the scheduling UE list for simulate HARQ feedback
	simulation_init_schedule_success_list();
	//UE_LOG("UE_ID\tC-RNTI\tmulti-tone\tdata volume\tCE_level\t\t\tConnection_time\n");
	UE_LOG("UE_ID\tC-RNTI\tmulti-tone\tCE_level\tPL\t\tConnection_time/data_size/index_dv\n");
	//Fill input structure
	
	FILE *file_ce=fopen("out/Out/CE.txt", "r");
	FILE *file_tone=fopen("out/Out/multitone.txt", "r");
	FILE *file_pl=fopen("out/Out/PL.txt", "r");
	FILE *file_id=fopen("out/Out/stationID.txt", "r");
	FILE *file_data_pattern=fopen("out/Out/UEdatapattern.txt", "r");
	FILE *file_dv=fopen("out/Out/UEdatavolume.txt", "r");
	//int times_request;
	for(i=0;i<NUM_TOTAL_UE;++i)
	{
		//fscanf(file_id,"%d ",&input_struct.UE_information[i].unique_id);
		input_struct.UE_information[i].unique_id=i+1;
		fscanf(file_tone,"%u ",&input_struct.UE_information[i].multi_tone);
		// Connection time
		fscanf(file_data_pattern, "%d ", &input_struct.UE_information[i].num_connection);
		for(j=0;j<input_struct.UE_information[i].num_connection;++j)
		{
			fscanf(file_data_pattern, "%d ", &input_struct.UE_information[i].request_subframe[j]);
		}
		// index of data volume [0-15]
		//input_struct.UE_information[i].data_volume=(1+i)%16;
		// data size
		for(j=0;j<input_struct.UE_information[i].num_connection;++j)
		{
			fscanf(file_dv, "%hu ", &input_struct.UE_information[i].data_size[j]);
			for(k=0;k<16;++k)
			{
				if(k==15)
				{
					input_struct.UE_information[i].data_volume[j]=k;
				}
				if(DV_table[k]>input_struct.UE_information[i].data_size[j])
				{
					input_struct.UE_information[i].data_volume[j]=k;
					break;
				}
			}
		}
		
		// Coverage enhancement level [0-2]
		fscanf(file_ce,"%d ",&input_struct.UE_information[i].CE_level);
		// PL
		fscanf(file_pl,"%f ",&input_struct.UE_information[i].path_loss);
		// mAh [0-100000]
		//input_struct.battery_report_format1;
		// Percentage [0-100] 
		//input_struct.battery_report_format2;
		
		//SCHEDULE_LOG("[%04d][simulation_initialization] ID %d connection subframe %d data volume index %d CE level %d path loss %f\n", mac_inst->current_subframe, input_struct.UE_information[i].unique_id, input_struct.UE_information[i].request_subframe[0], input_struct.UE_information[i].data_volume, input_struct.UE_information[i].CE_level, input_struct.UE_information[i].path_loss);
		UE_LOG("%d\t%d\t%d\t\t%d\t\t%f\t", input_struct.UE_information[i].unique_id, input_struct.UE_information[i].unique_id+257, input_struct.UE_information[i].multi_tone, input_struct.UE_information[i].CE_level, input_struct.UE_information[i].path_loss);
		for(j=0;j<input_struct.UE_information[i].num_connection;++j)
		{
			UE_LOG("%d/", input_struct.UE_information[i].request_subframe[j]);
			UE_LOG("%d/", input_struct.UE_information[i].data_size[j]);
			UE_LOG("%d ", input_struct.UE_information[i].data_volume[j]);
		}
		UE_LOG("\n");
		//UE_LOG("%d\n", input_struct.UE_information[i].num_connection);
	}
	fclose(file_ce);
	fclose(file_tone);
	fclose(file_pl);
	fclose(file_id);
	fclose(file_data_pattern);
	fclose(file_dv);
	// another testinig
#if 0
	for(i=0;i<NUM_TOTAL_UE;++i)
	{
		input_struct.UE_information[i].unique_id=i;
		input_struct.UE_information[i].multi_tone=1;
		// Connection time
		input_struct.UE_information[i].request_subframe[0]=0;
		// index of data volume [0-15]
		input_struct.UE_information[i].data_volume[0]=(1+i)%16;
		// Coverage enhancement level [0-2]
		//input_struct.UE_information[i].CE_level=0;
		input_struct.UE_information[i].CE_level=rand()%3;
		// mAh [0-100000]
		//input_struct.battery_report_format1;
		// Percentage [0-100] 
		//input_struct.battery_report_format2;
		input_struct.UE_information[i].num_connection=1;
		SCHEDULE_LOG("[%04d][simulation_initialization] ID %d connection subframe %d data volume index %d CE level %d\n", mac_inst->current_subframe, input_struct.UE_information[i].unique_id, input_struct.UE_information[i].request_subframe[0], input_struct.UE_information[i].data_volume[0], input_struct.UE_information[i].CE_level);
		UE_LOG("%d\t%d\t%d\t\t%d\t\t%d\t\t", input_struct.UE_information[i].unique_id, input_struct.UE_information[i].unique_id+257, input_struct.UE_information[i].multi_tone, input_struct.UE_information[i].data_volume[0], input_struct.UE_information[i].CE_level);
		for(j=0;j<input_struct.UE_information[i].num_connection;++j)
		{
			UE_LOG("%d ", input_struct.UE_information[i].request_subframe[j]);
		}
		UE_LOG("\n");
	}
#endif	

	//UE list and UE template initialization
	for(i=0;i<NUM_UE_LIST;++i)
	{
		if(i==0)
		{
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.R_max = 4;
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.G = 4;
			//Search Space offset
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.a_offset = 0;
			//T: PP=Rax*G
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.T= (uint32_t)((double)(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G);
			// Search space starting subframe
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.ss_start_uss = (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset;
		}
		else if(i==1)
		{
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.R_max = 8;
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.G = 8;
			//Search Space offset
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.a_offset = 0.25;
			//T: PP=Rax*G
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.T= (uint32_t)((double)(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G);
			// Search space starting subframe
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.ss_start_uss = (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset;
		}
		else
		{
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.R_max = 32;
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.G = 2;
			//Search Space offset
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.a_offset = 0.375;
			//T: PP=Rax*G
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.T= (uint32_t)((double)(mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G);
			// Search space starting subframe
			mac_inst->UE_list_spec[i].NPDCCH_config_dedicated.ss_start_uss = (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T * (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset;
		}
		
		mac_inst->UE_list_spec[i].head=-1;
		mac_inst->UE_list_spec[i].tail=-1;
		mac_inst->UE_list_spec[i].num_UEs=0;
		for(j=0;j<MAX_NUMBER_OF_UE_MAX_NB_IoT;++j)
		{
			(mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].active=0;
			(mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].RRC_connected=0;
			(mac_inst->UE_list_spec+i)->UE_template_NB_IoT[j].direction = -1;
		}
		SCHEDULE_LOG("[%04d][simulation_initialization][CE%d] R_max=%d, G=%.1f, a_offset=%.1f, T=%d, SS_start=%d\n", mac_inst->current_subframe, i, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss);
		printf("[simulation_initialization] List_number %d R_max %d G %.1f a_offset %.1f T %d SS_start %d\n", i, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.R_max, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.G, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.a_offset, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.T, (mac_inst->UE_list_spec+i)->NPDCCH_config_dedicated.ss_start_uss);
	}
	total_UE_count=0;
	//Setting UE_list and UE according to input structure 
	for(i=0;i<NUM_TOTAL_UE;++i)
	{
		if(input_struct.UE_information[i].request_subframe[0]==0)
		{
			if(total_UE_count==NUM_TOTAL_UE)
			{
				SCHEDULE_LOG("[%04d][simulation_initialization] UE list is full\n", mac_inst->current_subframe);
				break;
			}
			mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].num_UEs++;
			UE_template_temp1=&(mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[num_ue_in_list[input_struct.UE_information[i].CE_level]]);
			UE_template_temp1->rnti=input_struct.UE_information[i].unique_id+256;
			UE_template_temp1->CE_level=input_struct.UE_information[i].CE_level;
			UE_template_temp1->ul_total_buffer=DV_table[input_struct.UE_information[i].data_volume[0]];
			UE_template_temp1->multi_tone=input_struct.UE_information[i].multi_tone;
			UE_template_temp1->R_max=4;
			UE_template_temp1->R_dci=2;
			UE_template_temp1->R_ul=2;
			UE_template_temp1->oldNDI_UL=0;
			UE_template_temp1->PHR=2;
			UE_template_temp1->HARQ_round=0;
			UE_template_temp1->RRC_connected=1;
			UE_template_temp1->flag_schedule_success=0;
			//0 UL 1 DL
			UE_template_temp1->direction=0;
			UE_template_temp1->active=1;
			//Head UE in UE list
			if(num_ue_in_list[input_struct.UE_information[i].CE_level]==0)
			{
				mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].head=num_ue_in_list[input_struct.UE_information[i].CE_level];
				mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].tail=num_ue_in_list[input_struct.UE_information[i].CE_level];
				UE_template_temp1->next=-1;
				UE_template_temp1->prev=-1;
				
			}
			//not head UE
			else
			{	
				mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].tail=num_ue_in_list[input_struct.UE_information[i].CE_level];
				UE_template_temp1->next=-1;
				//UE_template_temp1->prev=ue_id_temp[input_struct.UE_information[i].CE_level];
				UE_template_temp1->prev=num_ue_in_list[input_struct.UE_information[i].CE_level]-1;
				UE_template_prev[input_struct.UE_information[i].CE_level]->next=num_ue_in_list[input_struct.UE_information[i].CE_level];
			}
			UE_template_prev[input_struct.UE_information[i].CE_level]=UE_template_temp1;
			//ue_id_temp[input_struct.UE_information[i].CE_level]=i;
			total_UE_count++;
			SCHEDULE_LOG("[%04d][simulation_initialization] ID %d CE level %d UL data size from DV %d\n", mac_inst->current_subframe, input_struct.UE_information[i].unique_id, mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[num_ue_in_list[input_struct.UE_information[i].CE_level]].CE_level, mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[num_ue_in_list[input_struct.UE_information[i].CE_level]].ul_total_buffer);
			num_ue_in_list[input_struct.UE_information[i].CE_level]++;
		}
		//SCHEDULE_LOG("[%04d][simulation_initialization] ID %d CE level %d UL data size from DV %d\n", mac_inst->current_subframe, i, UE_template_temp1->CE_level, mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[i].ul_total_buffer);
		//SCHEDULE_LOG("[%04d][simulation_initialization] ID %d CE level %d UL data size from DV %d\n", mac_inst->current_subframe, i, mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[i].CE_level, mac_inst->UE_list_spec[input_struct.UE_information[i].CE_level].UE_template_NB_IoT[i].ul_total_buffer);
	}
}

void simulation_check_request(eNB_MAC_INST_NB_IoT *mac_inst, uint32_t current_subframe)
{
	int i,j;
	//check UE in list or not
	for(i=0;i<NUM_TOTAL_UE;++i)
	{
		for(j=0;j<input_struct.UE_information[i].num_connection;++j)
		{
			// Find request
			if(current_subframe==input_struct.UE_information[i].request_subframe[j])
			{
				//add or update UE in UE list
				simulation_add_ue(mac_inst, i, j);
				SCHEDULE_LOG("[%04d][simulation_check_request] UE unique ID %d C-RNTI %d request transmission %d times\n", mac_inst->current_subframe, input_struct.UE_information[i].unique_id, input_struct.UE_information[i].unique_id+256,j+1);
				break;
			}
		}
	}
}

void simulation_add_ue(eNB_MAC_INST_NB_IoT *mac_inst, int input_ue_id, int request_times)
{
	int CE_level;
	// the ue id is a index of UE template, not the real UE id value
	int ue_id,ue_id_prev;
	UE_TEMPLATE_NB_IoT *UE_info;
	//check CE level for this UE
	CE_level=input_struct.UE_information[input_ue_id].CE_level;
	//check data size for this UE
	uint16_t data_size=DV_table[input_struct.UE_information[input_ue_id].data_volume[request_times]];
	//get the head UE in corresponding UE list of CE level
	ue_id = mac_inst->UE_list_spec[CE_level].head;
	ue_id_prev=ue_id;
	//check UE in list or not
	while(ue_id>-1)
	{
		UE_info = &mac_inst->UE_list_spec[CE_level].UE_template_NB_IoT[ue_id];
		if(UE_info->rnti-256==input_struct.UE_information[input_ue_id].unique_id)
		{
			// Update the UL totoal buffer value
			UE_info->ul_total_buffer+=data_size;
			// Change the state to make UE could be scheduled
			UE_info->direction=0;
			if(UE_info->flag_schedule_success==1)
			{
				UE_info->RRC_connected=0;
			}
			else
			{
				UE_info->RRC_connected=1;
			}
			return;
		}
		ue_id_prev=ue_id;
		ue_id = UE_info->next;
	}
	// if the UE not find in ue list, add new ue into ue list
	// if this is the tail UE
	if(UE_info->next==-1)
	{
		simulation_new_ue(&mac_inst->UE_list_spec[CE_level], input_ue_id, data_size, ue_id_prev);
	}
}

void simulation_new_ue(UE_list_NB_IoT_t *UE_list_target, int input_ue_id, uint16_t data_size, int UE_template_id_prev)
{
	UE_TEMPLATE_NB_IoT *UE_info;
	int ue_id;
	if(total_UE_count==NUM_TOTAL_UE)
	{
		printf("list full\n");
		return;
	}

	UE_info=&UE_list_target->UE_template_NB_IoT[UE_list_target->num_UEs];
	UE_list_target->num_UEs++;
	UE_info->rnti=input_struct.UE_information[input_ue_id].unique_id+256;
	UE_info->CE_level=input_struct.UE_information[input_ue_id].CE_level;
	UE_info->ul_total_buffer=data_size;
	UE_info->multi_tone=input_struct.UE_information[input_ue_id].multi_tone;
	UE_info->R_max=4;
	UE_info->R_dci=2;
	UE_info->R_ul=2;
	UE_info->oldNDI_UL=0;
	UE_info->PHR=2;
	UE_info->HARQ_round=0;
	UE_info->RRC_connected=1;
	UE_info->flag_schedule_success=0;
	//0 UL 1 DL
	UE_info->direction=0;
	UE_info->active=1;
	//head UE
	if(UE_template_id_prev==-1)
	{
		UE_list_target->head=0;
		UE_list_target->tail=0;
		UE_info->next=-1;
		UE_info->prev=-1;
	}
	else
	{
		UE_list_target->tail=UE_list_target->num_UEs-1;
		UE_info->next=-1;
		UE_info->prev=UE_template_id_prev;
		UE_list_target->UE_template_NB_IoT[UE_template_id_prev].next=UE_list_target->num_UEs-1;
	}
	total_UE_count++;
}
// flag_ack_nack 1:ack 0 : nack

// maintain scheduling success list
// initialize scheduling success list
void simulation_init_schedule_success_list()
{
	schedule_success_list.head=(SCHEDULE_SUCCESS_UE_t*)0;
	schedule_success_list.tail=(SCHEDULE_SUCCESS_UE_t*)0;
	schedule_success_list.number_ue_in_list=0;
}
//add UE into list when ue finishing schedulinig
void simulation_add_ue_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID, uint32_t start_sf, uint32_t end_sf)
{
	SCHEDULE_SUCCESS_UE_t *node;
	if(MAX_NUMBER_OF_UE_MAX_NB_IoT==schedule_success_list.number_ue_in_list)
	{
		DEBUG("[%04d][simulation_add_schedule_success_list][UE%d] add UE to schedule_success_list failed!\n", mac_inst->current_subframe, UE_ID);
		return;
	}
	else
	{
		node=(SCHEDULE_SUCCESS_UE_t*)malloc(sizeof(SCHEDULE_SUCCESS_UE_t));
		node->UE_template_id=UE_ID;
		node->crnti=UE_info->rnti;
		node->CE_level=UE_info->CE_level;
		node->transmission_direction=UE_info->direction;
		//UL
		if(node->transmission_direction==0)
		{
			node->data_end_sf_ul=end_sf+1;
		}
		//DL
		else
		{
			node->ack_nack_start_sf_dl=start_sf;
			node->ack_nack_end_sf_dl=end_sf+1;
		}
		node->next=(SCHEDULE_SUCCESS_UE_t*)0;
		if(schedule_success_list.head==(SCHEDULE_SUCCESS_UE_t*)0)
		{
			node->prev=(SCHEDULE_SUCCESS_UE_t*)0;
			schedule_success_list.head=node;
		}
		else
		{
			node->prev=schedule_success_list.tail;
			schedule_success_list.tail->next=node;
		}
		schedule_success_list.tail=node;
		schedule_success_list.number_ue_in_list++;
	}
}
//remove ue from ue list
void simulation_remove_ue_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID, uint8_t CE_level)
{
	SCHEDULE_SUCCESS_UE_t *node_temp=schedule_success_list.head;

	while(node_temp!=(SCHEDULE_SUCCESS_UE_t*)0)
	{
		if((UE_ID==node_temp->UE_template_id)&&(CE_level==node_temp->CE_level))
			break;
		node_temp=node_temp->next;
	}
	// only one UE in list
	if(schedule_success_list.number_ue_in_list==1)
	{
		schedule_success_list.head=(SCHEDULE_SUCCESS_UE_t*)0;
		schedule_success_list.tail=(SCHEDULE_SUCCESS_UE_t*)0;
	}
	// UE is head UE
	else if(schedule_success_list.head==node_temp)
	{
		schedule_success_list.head=node_temp->next;
		node_temp->next->prev=(SCHEDULE_SUCCESS_UE_t*)0;
	}
	//UE is tail UE
	else if(schedule_success_list.tail==node_temp)
	{
		node_temp->prev->next=(SCHEDULE_SUCCESS_UE_t*)0;
		schedule_success_list.tail=node_temp->prev;
	}
	else
	{
		node_temp->prev->next=node_temp->next;
		node_temp->next->prev=node_temp->prev;
	}
	free(node_temp);
	schedule_success_list.number_ue_in_list--;
}
// print list content
void simulation_print_UE_schedule_success_list(eNB_MAC_INST_NB_IoT *mac_inst)
{
	SCHEDULE_SUCCESS_UE_t *node_temp=schedule_success_list.head;
	while(node_temp!=(SCHEDULE_SUCCESS_UE_t*)0)
	{
		if(node_temp->transmission_direction==1)
		{
			DEBUG("[%04d][print_UE_schedule_success_list][UE%d] Direction %d UE_template_id %d CE level %d Feedback check start %d end %d\n", mac_inst->current_subframe, node_temp->crnti, node_temp->transmission_direction, node_temp->UE_template_id, node_temp->CE_level, node_temp->ack_nack_start_sf_dl, node_temp->ack_nack_end_sf_dl);
		}
		else if(node_temp->transmission_direction==0)
		{
			DEBUG("[%04d][print_UE_schedule_success_list][UE%d] Direction %d UE_template_id %d CE level %d Feedback check end %d\n", mac_inst->current_subframe, node_temp->crnti,  node_temp->transmission_direction, node_temp->UE_template_id, node_temp->CE_level, node_temp->data_end_sf_ul);
			//DEBUG("[%04d][print_UE_schedule_success_list][UE%d][Direction%d] Feedback check end %d\n", mac_inst->current_subframe, node_temp->UE_template_id, node_temp->transmission_direction, node_temp->data_end_sf_ul);
		}
		node_temp=node_temp->next;
	}
}
// simulate recieve ack or nack in correct timing
void simulation_check_retransmission_timing(eNB_MAC_INST_NB_IoT *mac_inst)
{
	SCHEDULE_SUCCESS_UE_t *node_temp=schedule_success_list.head;
	while(node_temp!=(SCHEDULE_SUCCESS_UE_t*)0)
	{
		if(node_temp->transmission_direction==1)
		{
			if(mac_inst->current_subframe==node_temp->ack_nack_end_sf_dl)
			{
				simulation_USS_HARQ_feedback(mac_inst, rand()%2, &((mac_inst->UE_list_spec+node_temp->CE_level)->UE_template_NB_IoT[node_temp->UE_template_id]), node_temp->UE_template_id);
			}

		}
		else if(node_temp->transmission_direction==0)
		{
			if(mac_inst->current_subframe==node_temp->data_end_sf_ul)
			{
				simulation_USS_HARQ_feedback(mac_inst, rand()%2, &((mac_inst->UE_list_spec+node_temp->CE_level)->UE_template_NB_IoT[node_temp->UE_template_id]), node_temp->UE_template_id);
			}
				
		}
		node_temp=node_temp->next;
	}
}
// transmission_direction 0:UL 1:DL
// simulate when receiving ack or nack, ue have to leave the scheduling success list
void simulation_USS_HARQ_feedback(eNB_MAC_INST_NB_IoT *mac_inst, uint8_t flag_ack_nack, UE_TEMPLATE_NB_IoT *UE_info, uint32_t UE_ID)
{
	UE_info->flag_schedule_success=0;
	if(flag_ack_nack==1)
	{
		if(UE_info->direction==1)
		{

		}
		else if(UE_info->direction==0)
		{
			if(UE_info->ul_total_buffer <= UE_info->allocated_data_size_ul)
			{
				UE_info->ul_total_buffer=0;
				UE_info->direction=-1;
			}
			else
			{
				UE_info->ul_total_buffer-=UE_info->allocated_data_size_ul;
				UE_info->direction=0;
				UE_info->RRC_connected=1;
			}

		}

		DEBUG("[%04d][simulation_USS_HARQ_feedback][UE%d][CE%d] UE template ID %d Receive ACK! Transmission is successful\n", mac_inst->current_subframe, UE_info->rnti, UE_info->CE_level, UE_ID);
		DEBUG("[%04d][simulation_USS_HARQ_feedback][UE%d][CE%d] UE template ID %d Remaining UL data size %d\n", mac_inst->current_subframe, UE_info->rnti, UE_info->CE_level, UE_ID, UE_info->ul_total_buffer);
		UE_info->HARQ_round=0;
	}
	else
	{
		UE_info->RRC_connected=1;
		UE_info->HARQ_round++;
		DEBUG("[%04d][simulation_USS_HARQ_feedback][UE%d][CE%d] UE template ID %d Receive NACK! Transmission is failed\n", mac_inst->current_subframe, UE_info->rnti, UE_info->CE_level, UE_ID);
	}

	simulation_remove_ue_schedule_success_list(mac_inst, UE_info, UE_ID, UE_info->CE_level);
}

#endif
