function loading_finish(){
	//window.alert("..");
	document.getElementsByClassName("loading")[0].style.display = "none";
	document.getElementsByTagName("body")[0].style.margin = "12px";
	
}

function scrollWin(pos) {
    //window.scrollTo(pos*40, 0);
	//window.alert(document.getElementsByName("FRAME_125")[0].offsetLeft);
	td_frame = document.getElementsByName("FRAME");
	if(pos < (td_frame.length/2)){
		window.scrollTo(td_frame[pos].offsetLeft, 0);
	}else{
		window.alert("out of range!\nmaximum is "+((td_frame.length/2)-1));
	}
	
	//window.scrollTo(13814,0);
}

function press_key(e){
	if(e.keyCode == 13){
		scrollWin(document.getElementsByName('configname')[0].value);
		document.getElementsByName('configname')[0].setSelectionRange(0, document.getElementsByName('configname')[0].value.length);
		
	}
}


var channel_color = [];
channel_color["NSSS"] = "#0070C0";
channel_color["NPSS"] = "red";
channel_color["NPBCH"] = "yellow";
channel_color["NPRACH"] = "green";
channel_color["NPDCCH"] = "#CC00CC";
channel_color["NPDSCH"] = "#EE7700";
channel_color["NPDSCH_SIB"] = "#69C18A";
channel_color["NPUSCH"] = "800080";

var channel_color_light = [];
channel_color_light["NPDCCH"] = "#FF00FF";
channel_color_light["NPDSCH"] = "#FFBB22";
channel_color_light["NPDSCH_SIB"] = "#A0FFFF";
channel_color_light["NPUSCH"] = "FF00FF";

function handleClick(flag, str){
	
	var x = document.getElementsByClassName(str);
	var i;
	//window.alert(flag);
	if(flag){
		for (i = 0; i < x.length; i++) {
			x[i].style.backgroundColor = channel_color[str];
		}
	}else{
		for (i = 0; i < x.length; i++) {
			x[i].style.backgroundColor = "white";
		}
	}
	
}

function touch_function_dl(e, flag, tag) {
	//var x = e.clientX;
    //var y = e.clientY;
	
	var table = document.getElementById("dltable");
	
	var x = e.offsetLeft + table.offsetLeft + 40;
	var y = e.offsetTop + table.offsetTop + 21;
	var classes = e.className.split(" ");
	
    //cursor="Your Mouse Position Is : " + x + " and " + y + "  className : " + e.className ;
	//window.alert(cursor);
	//document.getElementById("displayArea").innerHTML=cursor;
	
	var ctx_table = document.getElementById("ctx-table");
	var schedule_table = document.getElementById("schedule-delay-table");
	
	var table_channel_tr = document.getElementById("table-channel-tr");
	var table_rnti_tr = document.getElementById("table-rnti-tr");
	var table_ctx_tr = document.getElementById("table-ctx-tr");
	
	var table_channel = document.getElementById("table-channel");
	var table_rnti = document.getElementById("table-rnti");
	var table_ctx = document.getElementById("table-ctx");
	
	if(flag){
		if(tag != "DL_0" && e.style.backgroundColor != "white"){
			var elements = document.getElementsByClassName(tag);
			var i;
			var ch = elements[0].className.split(" ")[0];
			for(i=0; i<elements.length; ++i){
				elements[i].style.backgroundColor = channel_color_light[ch];
		//		elements[i].style.border = "1px solid black";
			}
		}
		
		if(ctx_table){
			ctx_table.style.display = "block";
			ctx_table.style.left = x + "px";
			ctx_table.style.top  = y + "px";
		}
		
		if(classes && table_channel){
			//	channel
			table_channel.innerHTML = classes[0];
			
			//	rnti
			if(classes[1]){
				table_rnti_tr.style.display = "";
			}else{
				table_rnti_tr.style.display = "none";
			}
			table_rnti.innerHTML = classes[1];
			
			//	ctx
			if(classes[1]){
				table_ctx_tr.style.display = "";
			}else{
				table_ctx_tr.style.display = "none";
			}
			table_ctx.innerHTML = classes[2];
		}
		
		//window.alert(classes.length);
		
		if(classes[0] == "NPDCCH" && classes[classes.length-2] != "" && schedule_table){
			var group_tag = classes[classes.length-1];
			var group = document.getElementsByClassName(group_tag);
		
			schedule_table.style.display = "block";
			schedule_table.style.top = document.getElementsByClassName("dl")[0].offsetTop + document.getElementsByClassName("dl")[0].offsetHeight + "px";
			schedule_table.style.left = group[group.length-1].offsetLeft + table.offsetLeft + e.offsetWidth + "px";
			
			var param = classes[classes.length-2].split("A");
			var ack_k0 = 0;
			//var gap = document.getElementById("s1").offsetWidth*2;
			
			if(param.length == 1){
				//DCI N0
				document.getElementById("s4").style.borderRight = "1px solid red";
				document.getElementById("s1").style.borderRight = "0px";
				document.getElementById("s1").innerHTML = param[0];
				document.getElementById("s1").style.minWidth = (param[0]-1)*e.offsetWidth+"px";
				
				document.getElementById("s3").style.borderLeft = "0px";
				document.getElementById("s3").style.borderBottom = "0px";
				document.getElementById("s6").style.borderRight = "0px";
				document.getElementById("s3").innerHTML = "";
			}else{
				//DCI N1
				//DLSCH
				document.getElementById("s4").style.borderRight = "0px";
				document.getElementById("s1").style.borderRight = "1px solid red";
				document.getElementById("s1").innerHTML = param[0];
				document.getElementById("s1").style.minWidth = (param[0]-1)*e.offsetWidth+"px";
				//REP
				document.getElementById("s2").style.minWidth = (param[1]*e.offsetWidth)-2+"px";
				
				//ULSCH
				document.getElementById("s3").style.borderBottom = "1px solid red";
				document.getElementById("s3").style.borderRight = "0px";
				document.getElementById("s3").style.borderLeft = "1px solid red";
				document.getElementById("s6").style.borderRight = "1px solid red";
				document.getElementById("s3").innerHTML = param[2];
				document.getElementById("s3").style.minWidth = (param[2]-1)*e.offsetWidth+"px";
			}
			
			
			
		}
	}else{
		if(classes[0] != "NPDCCH" && schedule_table){
			schedule_table.style.display = "none";
		}
	
		if(ctx_table){
			ctx_table.style.display = "none";
		}
	
		if(tag != "DL_0" && e.style.backgroundColor != "white"){
			var elements = document.getElementsByClassName(tag);
			var i;
			var ch = elements[0].className.split(" ")[0];
			for(i=0; i<elements.length; ++i){
				elements[i].style.backgroundColor = channel_color[ch];
		//		elements[i].style.border = "1px solid #00";
			}
		}
	}
	
	
	
}

function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
  handleClick(source.checked, "NSSS");
  handleClick(source.checked, "NPSS");
  handleClick(source.checked, "NPBCH");
  handleClick(source.checked, "NPDCCH");
  handleClick(source.checked, "NPDSCH");
  handleClick(source.checked, "NPUSCH");
  handleClick(source.checked, "NPRACH");
  handleClick(source.checked, "NPDSCH_SIB");
  
}

function touch_function_ul(e, flag, tag) {
	//var x = e.clientX;
    //var y = e.clientY;
	
	var table = document.getElementById("ultable");
	
	var x = e.offsetLeft + table.offsetLeft + 40;
	var y = e.offsetTop + table.offsetTop + 21;
	var classes = e.className.split(" ");
	
    //cursor="Your Mouse Position Is : " + x + " and " + y + "  className : " + e.className ;
	//window.alert(cursor);
	//document.getElementById("displayArea").innerHTML=cursor;
	
	var ctx_table = document.getElementById ("ctx-table");
	
	var table_channel_tr = document.getElementById("table-channel-tr");
	var table_rnti_tr = document.getElementById("table-rnti-tr");
	var table_ctx_tr = document.getElementById("table-ctx-tr");
	
	var table_channel = document.getElementById("table-channel");
	var table_rnti = document.getElementById("table-rnti");
	var table_ctx = document.getElementById("table-ctx");
	
	
	
	if(flag){
	
		if(ctx_table){
			ctx_table.style.display = "block";
			ctx_table.style.left = x + "px";
			ctx_table.style.top  = y + "px";
		}
		
		if(classes && table_channel){
		
			//	channel
			table_channel.innerHTML = classes[0];
			
			
			//	rnti
			if(classes[1]){
				table_rnti_tr.style.display = "";
			}else{
				table_rnti_tr.style.display = "none";
			}
			table_rnti.innerHTML = classes[1];
			
			//	ctx
			if(classes[1]){
				table_ctx_tr.style.display = "";
			}else{
				table_ctx_tr.style.display = "none";
			}
			table_ctx.innerHTML = classes[2];
		}
		
		if(tag != "UL_0" && e.style.backgroundColor != "white"){
			var elements = document.getElementsByClassName(tag);
			var i;
			var ch = elements[0].className.split(" ")[0];
			for(i=0; i<elements.length; ++i){
				elements[i].style.backgroundColor = channel_color_light[ch];
			}
		}
		
	}else{
		if(ctx_table){
			ctx_table.style.display = "none";
		}
		if(tag != "UL_0" && e.style.backgroundColor != "white"){
			var elements = document.getElementsByClassName(tag);
			var i;
			var ch = elements[0].className.split(" ")[0];
			for(i=0; i<elements.length; ++i){
				elements[i].style.backgroundColor = channel_color[ch];
			}
		}
	}
	
}