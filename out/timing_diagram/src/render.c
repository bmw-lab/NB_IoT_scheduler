#include "render.h"

//#define UL_ALL_3750HZ

resource_t *dl_scheduled_bitmap;
resource_t *ul_scheduled_bitmap[48];
uint8_t carrier_has_nprach[12] = {0};

uint8_t channel_name[channels_length][12] = {
	"NA",
	//DL
	"NPSS",
	"NSSS",
	"NPBCH",
	"NPDCCH",
	"NPDSCH",
	"NPDSCH_SIB",
	//UL
	"NPRACH",
	"NPUSCH"
};

uint8_t printer_char[num_printer][4] = {
	"$DL",	//	reserve for DL
	"$UL",	//	reserve for UL
	"$RF",
	"$SF",
	"$F1",
	"$1"
};

void enqueue(printer_t **head, uint8_t *value){
	printer_t *new_node, *iterator;
	new_node = (printer_t *)malloc(sizeof(printer_t));
	new_node->next = (printer_t *)0;
	//	memcpy from input to queue!
	memcpy(new_node->str, value, strlen(value)+1);
	
	if((printer_t *)0 == *head){
	    *head = new_node;
    }else{
        iterator = *head;
       // printf("%d %d", iterator, iterator->next);
        while((printer_t *)0 != iterator->next){
            iterator = iterator->next;
        }
        iterator->next = new_node;
    }
}

uint32_t dequeue(printer_t **head, uint8_t *o_str){
	printer_t *del;
	
	del = *head;
	if((printer_t *)0 == del) return 0;
	memcpy(o_str, del->str, strlen(del->str));
	*head = (*head)->next;
	free(del);
	return 1;
}

void load_frames_info(uint32_t frames, FILE *fp){
    uint32_t frame;
    
	for(frame=0;frame<frames;++frame){
		fprintf(fp, "<td colspan=\"10\" class=\"FRAME\" name=\"FRAME\">%d</td>\n", frame);
	}
}

void load_subframes_info(uint32_t frames, FILE *fp){
    uint32_t frame;
	uint32_t subframe;
	
	for(frame=0;frame<frames;++frame)
	for(subframe=0;subframe<num_subframe_per_frame;++subframe){
		fprintf(fp, "<td class=\"SUBFRAME\">%d</td>\n", subframe);
	}
}

void load_dl_frames(uint32_t end_time, FILE *fp){
	uint32_t abs_subframe;
	
	uint8_t sdly_str[10] = {'\0'};
	uint8_t ctx[50] = {'\0'}, rnti_str[10] = {'\0'};
	//uint16_t rnti;
	
	
	for(abs_subframe=0;abs_subframe<end_time;++abs_subframe){
	//for(subframe=0;subframe<num_subframe_per_frame;++subframe){
	//	printf("%d", abs_subframe);
	    if((uint8_t *)0 != dl_scheduled_bitmap[abs_subframe].ctx){
	    	if(dl_scheduled_bitmap[abs_subframe].ctx_num != 0){
	    		sprintf(ctx, "%s(%d)", dl_scheduled_bitmap[abs_subframe].ctx, dl_scheduled_bitmap[abs_subframe].ctx_num);
			}else{
				sprintf(ctx, "%s", dl_scheduled_bitmap[abs_subframe].ctx);
			}
	    	
	        //ctx = dl_scheduled_bitmap[abs_subframe].ctx;
        }else{
            sprintf(ctx, " ");
        }
        
        if(0 != dl_scheduled_bitmap[abs_subframe].rnti){
            sprintf(rnti_str, "%04X", dl_scheduled_bitmap[abs_subframe].rnti);
        }else{
            rnti_str[0] = '\0';
        }
        
        
   // 	printf("*");
   // 	printf("%s", *rnti);
		if(dl_scheduled_bitmap[abs_subframe].sdly > 0){
			if(dl_scheduled_bitmap[abs_subframe].ack_sdly > 0){
				sprintf(sdly_str, "%dA%dA%d", dl_scheduled_bitmap[abs_subframe].sdly, dl_scheduled_bitmap[abs_subframe].num_sf, dl_scheduled_bitmap[abs_subframe].ack_sdly);
			}else{
				sprintf(sdly_str, "%d", dl_scheduled_bitmap[abs_subframe].sdly);
			}
		}else{
			sdly_str[0] = '\0';
		}
		//sprintf(sdly_str, "%dA%d", dl_scheduled_bitmap[abs_subframe].sdly, dl_scheduled_bitmap[abs_subframe].ack_sdly);
			
		fprintf(fp, "<td onmouseover=\"touch_function_dl(this, 1, 'DL_%d')\" onmouseout=\"touch_function_dl(this, 0, 'DL_%d')\" class=\"%s %s %s %s DL_%d\"></td>\n", dl_scheduled_bitmap[abs_subframe].tag, dl_scheduled_bitmap[abs_subframe].tag, channel_name[dl_scheduled_bitmap[abs_subframe].channel], rnti_str, ctx, sdly_str, dl_scheduled_bitmap[abs_subframe].tag);
	//	fprintf(fp, "<td onmouseover=\"touch_function_dl(this)\" class=\"%s %s %s\"></td>\n", channel_name[dl_scheduled_bitmap[abs_subframe].channel], rnti_str, ctx);
		//printf("%s\n", dl_scheduled_bitmap[subframe][frame].ctx);
	//	printf("end\n");
	}
}

void load_ul_frames(uint32_t end_time, FILE *fp){
    uint32_t abs_subframe;
	int8_t subcarrier, subcarrier_15k;
	
	uint8_t *ctx, rnti_str[10] = {'\0'};
	
	for(subcarrier=47;subcarrier>=0;--subcarrier){
	    subcarrier_15k=subcarrier/4;
        
        #ifdef UL_ALL_3750HZ
        fprintf(fp, "<tr>\n");
        if(0==((subcarrier+1)%4))
            fprintf(fp, "<td rowspan=\"4\">%d-%d</td>\n", subcarrier-3, subcarrier);
        #else
        if(carrier_has_nprach[subcarrier_15k]){
            fprintf(fp, "<tr>\n");
            if(0==((subcarrier+1)%4))
                fprintf(fp, "<td rowspan=\"4\">%d-%d</td>\n", subcarrier-3, subcarrier);
        }else{
            if(0==((subcarrier+1)%4))
                fprintf(fp, "<tr>\n<td>%d-%d</td>\n", subcarrier-3, subcarrier);
        }
        #endif
    	for(abs_subframe=0;abs_subframe<end_time;++abs_subframe){
    	    
    	    #ifdef UL_ALL_3750HZ
    	    fprintf(fp, "<td class=%s></td>\n", channel_name[ul_scheduled_bitmap[subcarrier][abs_subframe].channel]);
            #else
            
            if((uint8_t *)0 != ul_scheduled_bitmap[subcarrier][abs_subframe].ctx){
                ctx = ul_scheduled_bitmap[subcarrier][abs_subframe].ctx;
            }else{
                ctx = " ";
            }


            if(0 != ul_scheduled_bitmap[subcarrier][abs_subframe].rnti){
                sprintf(rnti_str, "%04X", ul_scheduled_bitmap[subcarrier][abs_subframe].rnti);
            }else{
                rnti_str[0] = '\0';
            }
            
            if( carrier_has_nprach[subcarrier_15k] ){
    	        //if(_NPRACH==ul_scheduled_bitmap[subcarrier][abs_subframe].channel){
    	        //    fprintf(fp, "<td onmouseover=\"touch_function_ul(this)\" class=NPRACH ></td>\n");
                //}else{
                    if(0==((subcarrier+1)%4)){
                        
                        fprintf(fp, "<td rowspan=\"4\" onmouseover=\"touch_function_ul(this, 1, 'UL_%d')\" onmouseout=\"touch_function_ul(this, 0, 'UL_%d')\" class=\"%s %s %s UL_%d\"></td>\n", ul_scheduled_bitmap[subcarrier][abs_subframe].tag, ul_scheduled_bitmap[subcarrier][abs_subframe].tag, channel_name[ul_scheduled_bitmap[subcarrier][abs_subframe].channel], rnti_str, ctx, ul_scheduled_bitmap[subcarrier][abs_subframe].tag);
                    }
                //}
            }else{
                if(0==((subcarrier+1)%4))
                    fprintf(fp, "<td onmouseover=\"touch_function_ul(this, 1, 'UL_%d')\" onmouseout=\"touch_function_ul(this, 0, 'UL_%d')\" class=\"%s %s %s UL_%d\"></td>\n", ul_scheduled_bitmap[subcarrier][abs_subframe].tag, ul_scheduled_bitmap[subcarrier][abs_subframe].tag, channel_name[ul_scheduled_bitmap[subcarrier][abs_subframe].channel], rnti_str, ctx, ul_scheduled_bitmap[subcarrier][abs_subframe].tag);
            } 
    	    #endif
        }
        
        #ifdef UL_ALL_3750HZ
        fprintf(fp, "</tr>\n");
        #else
        if(carrier_has_nprach[subcarrier_15k]){
            fprintf(fp, "</tr>\n");
        }else{
            if(0==((subcarrier+1)%4))
                fprintf(fp, "</tr>\n");
        }
        #endif
    }
}

void init_render(render_t *render){
    uint32_t i;
    for(i=0;i<num_PRINTER;++i)
        render->printer[i] = (printer_t *)0;
}

void render_html(render_t *render, printer_e target, uint8_t *str){
	enqueue(&render->printer[target], str);
}

void output_html(render_t *render, uint32_t end_abs_subframe, FILE *fi, FILE *fo){
	uint8_t str[200], *p, *p2, *p3, *p4;
	uint8_t i;
	printer_t *iterator;
	uint8_t len;
	while(!feof(fi)){
		//fscanf(fi, "%s\n", str);
		fgets(str, 200, fi);
		
		len=strlen(str);
		
		p = strstr(str, printer_char[PRINTER_DL]);
		if((uint8_t *)0 != p){
			fwrite(str, 1, p-str, fo);
			load_dl_frames(end_abs_subframe, fo);
			fprintf(fo, "%s", p+3);
			continue;
		}
		p = strstr(str, printer_char[PRINTER_UL]);
		if((uint8_t *)0 != p){
			fwrite(str, 1, p-str, fo);
			load_ul_frames(end_abs_subframe, fo);
			fprintf(fo, "%s", p+3);
			continue;
		}
		
		p = strstr(str, printer_char[PRINTER_FRAME_INFO]);
		if((uint8_t *)0 != p){
			fwrite(str, 1, p-str, fo);
			load_frames_info(end_abs_subframe/10, fo);
			fprintf(fo, "%s", p+3);
			continue;
		}
		p = strstr(str, printer_char[PRINTER_SUBFRAME_INFO]);
		if((uint8_t *)0 != p){
			fwrite(str, 1, p-str, fo);
			load_subframes_info(end_abs_subframe/10, fo);
			fprintf(fo, "%s", p+3);
			continue;
		}
		for(i=PRINTER_1;i<num_PRINTER;i++){
			p = strstr(str, printer_char[i]);
			if((uint8_t *)0 != p){
				fwrite(str, 1, p-str, fo);
				iterator = render->printer[i];
				
				while((printer_t *)0 != iterator){
					fprintf(fo, "%s", iterator->str);
					iterator=iterator->next;
				}
				fprintf(fo, "%s", p+2);
				break;
			}
		}
		if((uint8_t *)0 == p)
			fprintf(fo, "%s", str);
	}
}

void dl_scheduled(uint32_t abs_subframe, uint32_t tag, channel_t channel, uint16_t rnti, uint8_t *ctx, int32_t ctx_num, int16_t sdly, int16_t ack_sdly, int16_t num_sf){
	
	dl_scheduled_bitmap[abs_subframe].channel = channel;
	dl_scheduled_bitmap[abs_subframe].rnti = rnti;
	dl_scheduled_bitmap[abs_subframe].ctx = ctx;
	dl_scheduled_bitmap[abs_subframe].tag = tag;
	dl_scheduled_bitmap[abs_subframe].sdly = sdly;
	dl_scheduled_bitmap[abs_subframe].ack_sdly = ack_sdly;
	dl_scheduled_bitmap[abs_subframe].num_sf = num_sf;
	dl_scheduled_bitmap[abs_subframe].ctx_num = ctx_num;
	/*
	switch(channel){
		case _NPSS:
			break;
		case _NSSS:
			break;
		case _NPBCH:
			break;
		case _NPDCCH:
			dl_scheduled_bitmap[abs_subframe].channel = _NPDCCH;
			dl_scheduled_bitmap[abs_subframe].rnti = rnti;
			dl_scheduled_bitmap[abs_subframe].ctx = ctx;
			break;
		case _NPDSCH:
			if(SI_RNTI == rnti){
				dl_scheduled_bitmap[abs_subframe].channel = _NPDSCH_SIB;
				dl_scheduled_bitmap[abs_subframe].rnti = "SI_RNTI";
				dl_scheduled_bitmap[abs_subframe].ctx = ctx;
			}else{
			    dl_scheduled_bitmap[abs_subframe].channel = _NPDSCH;
			    dl_scheduled_bitmap[abs_subframe].rnti = rnti;
			    dl_scheduled_bitmap[abs_subframe].ctx = ctx;
            }
			break;

		default:
			break;
	}*/
}

void ul_scheduled(uint32_t abs_subframe, uint32_t tag, uint32_t carrier, channel_t channel, uint16_t rnti, uint8_t *ctx){
    
	ul_scheduled_bitmap[carrier][abs_subframe].channel = channel;
	ul_scheduled_bitmap[carrier][abs_subframe].rnti = rnti;
	ul_scheduled_bitmap[carrier][abs_subframe].ctx = ctx;
	ul_scheduled_bitmap[carrier][abs_subframe].tag = tag;
    switch(channel){
		case _NPRACH:
		    carrier_has_nprach[carrier/4] = 1;
			break;
		case _NPUSCH:
			break;
		default:
			break;
	}
}


