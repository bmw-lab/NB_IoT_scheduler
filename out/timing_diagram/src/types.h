
#ifndef _TYPES_H_
#define _TYPES_H_

#define num_subframe_per_frame 10

#define SI_RNTI 0xffff
/*
typedef unsigned char uint8_t;
typedef char int8_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned long uint32_t;
typedef long int32_t;

*/

typedef enum channel_e{
	_NA=0,
	//DL
	_NPSS,
	_NSSS,
	_NPBCH,
	_NPDCCH,
	_NPDSCH,
	_NPDSCH_SIB,
	//UL
	_NPRACH,
	_NPUSCH,
		 
	channels_length
}channel_t;






#endif
