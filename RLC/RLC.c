
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "RLC.h"

 char RLC_data[20]={"Calvin is a man HaHa"};
extern mac_status_resp *rlc_state;

mac_status_resp rlc_am_mac_status_indication(const uint32_t tb_sizeP)
{
    mac_status_resp status_resp;

    rlc_tb_sizeP = tb_sizeP;

    status_resp.buffer_occupancy_in_bytes = rlc_state->buffer_occupancy_in_bytes;
    status_resp.buffer_occupancy_in_pdus = rlc_state->buffer_occupancy_in_pdus;
    status_resp.head_sdu_creation_time = rlc_state->head_sdu_creation_time;
    status_resp.head_sdu_is_segmented = rlc_state->head_sdu_is_segmented;
    status_resp.head_sdu_remaining_size_to_send = rlc_state->head_sdu_remaining_size_to_send;
   // status_resp.rlc_info

    return status_resp;
}

/*
int main(void)
{
    char *buffer;
    mac_rlc_status_resp_t *check_rlc_state;
    tb_size_t data_tb;
    int i,count=0;

    buffer = (char*)malloc(sizeof(char));
    check_rlc_state = (mac_rlc_status_resp_t*)malloc(sizeof(mac_rlc_status_resp_t));

    //*buffer = 0;


    ///initial
    initial_rlc();
    ///transmit MAC TBS
    *check_rlc_state = NB_mac_rlc_status_ind(0,0,0,0,0,0,0,0,6);
    ///MAC get the data from RLC

    printf("%s\n",p);

    while(data_tb!=0){

    data_tb = NB_mac_rlc_data_req_eNB(0,0,0,0,0,0,0,rlc_tb_sizeP,buffer);

    printf("data_tb = %d\n",data_tb);
    }

    for(i=0;i<20;i++){
    printf("%c",*(buffer+i));
    }



    return 0;
}
*/

void initial_rlc(void)
{
    rlc_state = (mac_status_resp*)malloc(sizeof(mac_status_resp));


    rlc_state->buffer_occupancy_in_bytes = sizeof(RLC_data);
    rlc_state->buffer_occupancy_in_pdus = 1;
    rlc_state->head_sdu_creation_time=0;
    rlc_state->head_sdu_is_segmented=0;
    rlc_state->head_sdu_remaining_size_to_send=sizeof(RLC_data);
    //rlc_state->rlc_info= NULL ;

}


mac_rlc_status_resp_t mac_rlc_status_ind_NB_IoT(
  const module_id_t       module_idP,
  const rnti_t            rntiP,
  const eNB_index_t       eNB_index,
  const frame_t           frameP,
  const sub_frame_t 	  subframeP,
  const eNB_flag_t        enb_flagP,
  const MBMS_flag_t       MBMS_flagP,
  const logical_chan_id_t channel_idP,
  const tb_size_t         tb_sizeP)
  {

        mac_rlc_status_resp_t  mac_rlc_status_resp;
        mac_status_resp status_resp;
        rlc_mode_t      rlc_mode    = RLC_MODE_AM;

        switch (rlc_mode)
        {
            case RLC_MODE_NONE:
            //handle_event(WARNING,"FILE %s FONCTION mac_rlc_data_ind() LINE %s : no radio bearer configured :%d\n", __FILE__, __LINE__, channel_idP);
            mac_rlc_status_resp.bytes_in_buffer                 = 0;
            break;

            case RLC_MODE_AM:
 //           status_resp = rlc_am_mac_status_indication(&ctxt, &rlc_union_p->rlc.am, tb_sizeP, tx_status,enb_flagP);
            status_resp = rlc_am_mac_status_indication(tb_sizeP);
            mac_rlc_status_resp.bytes_in_buffer                 = status_resp.buffer_occupancy_in_bytes;
            mac_rlc_status_resp.head_sdu_creation_time          = status_resp.head_sdu_creation_time;
            mac_rlc_status_resp.head_sdu_remaining_size_to_send = status_resp.head_sdu_remaining_size_to_send;
            mac_rlc_status_resp.head_sdu_is_segmented           = status_resp.head_sdu_is_segmented;
            //return mac_rlc_status_resp;
            break;

            case RLC_MODE_TM:
 //          status_resp = rlc_tm_mac_status_indication(&ctxt, &rlc_union_p->rlc.tm, tb_sizeP, tx_status);
  //          status_resp = rlc_tm_mac_status_indication(tb_sizeP);
            mac_rlc_status_resp.bytes_in_buffer = status_resp.buffer_occupancy_in_bytes;
            mac_rlc_status_resp.pdus_in_buffer  = status_resp.buffer_occupancy_in_pdus;
            // return mac_rlc_status_resp;
            break;
        }

    return mac_rlc_status_resp;

  }

tb_size_t mac_rlc_data_req_eNB_NB_IoT(
  const module_id_t       module_idP,
  const rnti_t            rntiP,
  const eNB_index_t       eNB_index,
  const frame_t           frameP,
  const eNB_flag_t        enb_flagP,
  const MBMS_flag_t       MBMS_flagP,
  const logical_chan_id_t channel_idP,
  const tb_size_t         tb_sizeP,
  char             *buffer_pP)
  {
        rlc_mode_t      rlc_mode    = RLC_MODE_AM;
        tbs_size_t             ret_tb_size         = 0;

        switch (rlc_mode)
        {
            case RLC_MODE_NONE:
            ret_tb_size =0;
            break;

            case RLC_MODE_AM:
                printf("RlC remain size %d\n",rlc_state->head_sdu_remaining_size_to_send);

                if( rlc_state->head_sdu_remaining_size_to_send >= 0 && rlc_state->head_sdu_remaining_size_to_send >=tb_sizeP )
                {
                    // printf("buffer %d\n",sizeof(*buffer_pP));
                    memcpy(buffer_pP+rlc_state->head_sdu_is_segmented,RLC_data+rlc_state->head_sdu_is_segmented,tb_sizeP);
                    rlc_state->buffer_occupancy_in_bytes = rlc_state->buffer_occupancy_in_bytes - tb_sizeP;
                    rlc_state->buffer_occupancy_in_pdus = 1;
                    rlc_state->head_sdu_creation_time =0;
                    rlc_state->head_sdu_is_segmented =rlc_state->head_sdu_is_segmented+tb_sizeP;
                   // printf("RlC remain size %d\n",rlc_state->head_sdu_remaining_size_to_send);
                    rlc_state->head_sdu_remaining_size_to_send =rlc_state->head_sdu_remaining_size_to_send - tb_sizeP;

                   // printf("source %d\n",sizeof(p));
                   // printf("buffer %d\n",sizeof();
                   // printf("tb_sizeP %d\n",tb_sizeP);

                    ret_tb_size = tb_sizeP;
                }
                else if(rlc_state->head_sdu_remaining_size_to_send >= 0 && rlc_state->head_sdu_remaining_size_to_send < tb_sizeP)
                {
                    memcpy(buffer_pP+rlc_state->head_sdu_is_segmented,RLC_data+rlc_state->head_sdu_is_segmented,rlc_state->head_sdu_remaining_size_to_send);
                    rlc_state->buffer_occupancy_in_bytes = rlc_state->buffer_occupancy_in_bytes - rlc_state->head_sdu_remaining_size_to_send;
                    rlc_state->buffer_occupancy_in_pdus = 1;
                    rlc_state->head_sdu_creation_time =0;
                    rlc_state->head_sdu_is_segmented =rlc_state->head_sdu_is_segmented+rlc_state->head_sdu_remaining_size_to_send;
                   // printf("RlC remain size %d\n",rlc_state->head_sdu_remaining_size_to_send);
                    rlc_state->head_sdu_remaining_size_to_send =0;

                    ret_tb_size = rlc_state->head_sdu_remaining_size_to_send;
                }
   //         if (!enb_flagP) rlc_am_set_nb_bytes_requested_by_mac(&rlc_union_p->rlc.am,tb_sizeP);
   //         data_request = rlc_am_mac_data_request(&ctxt, &rlc_union_p->rlc.am,enb_flagP);
   //         ret_tb_size =mac_rlc_serialize_tb(buffer_pP, data_request.data);
            break;

            case RLC_MODE_UM:
   //         if (!enb_flagP) rlc_um_set_nb_bytes_requested_by_mac(&rlc_union_p->rlc.um,tb_sizeP);
   //         data_request = rlc_um_mac_data_request(&ctxt, &rlc_union_p->rlc.um,enb_flagP);
   //         ret_tb_size = mac_rlc_serialize_tb(buffer_pP, data_request.data);
            break;

            case RLC_MODE_TM:
   //         data_request = rlc_tm_mac_data_request(&ctxt, &rlc_union_p->rlc.tm);
   //         ret_tb_size = mac_rlc_serialize_tb(buffer_pP, data_request.data);
            break;
        }

        return ret_tb_size;
  }



/*
struct mac_status_resp rlc_tm_mac_status_indication(const uint16_t tb_sizeP)
{
    struct mac_status_resp status_resp;

    rlc_tb_sizeP = tb_sizeP;

    status_resp.buffer_occupancy_in_bytes
    status_resp.buffer_occupancy_in_pdus
    status_resp.head_sdu_creation_time
    status_resp.head_sdu_is_segmented
    status_resp.head_sdu_remaining_size_to_send
   // status_resp.rlc_info

    return status_resp;
}
*/


