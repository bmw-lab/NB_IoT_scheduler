#ifndef _RLC_H_
#define _RLC_H_

//Type definition in OAI
typedef unsigned int	uint32_t;
typedef unsigned short	uint16_t;
typedef unsigned char	uint8_t;
typedef signed int		int32_t;
typedef signed short	int16_t;
typedef signed char		int8_t;
typedef uint16_t        rnti_t;
typedef signed char     boolean_t;
typedef uint8_t         module_id_t;
typedef uint32_t        frame_t;
typedef uint32_t        sub_frame_t;
typedef unsigned int    rb_id_t;
typedef uint32_t				tb_size_t;
typedef int32_t               sdu_size_t;
typedef sdu_size_t         tbs_size_t;
typedef signed char			MBMS_flag_t;
typedef uint8_t					eNB_index_t;
typedef boolean_t       eNB_flag_t;
typedef uint32_t 				logical_chan_id_t;
typedef uint8_t         mib_flag_t ;



uint16_t rlc_tb_sizeP;

typedef enum rlc_mode_e {
  RLC_MODE_NONE    = 0,
  RLC_MODE_AM      = 1,
  RLC_MODE_UM      = 2,
  RLC_MODE_TM      = 4
} rlc_mode_t;


/*! \struct  mac_rlc_status_resp_t
* \brief Primitive exchanged between RLC and MAC informing about the buffer occupancy of the RLC protocol instance.
*/
typedef  struct {
  uint32_t       bytes_in_buffer; /*!< \brief Bytes buffered in RLC protocol instance. */
  uint32_t       pdus_in_buffer;  /*!< \brief Number of PDUs buffered in RLC protocol instance (OBSOLETE). */
  uint32_t       head_sdu_creation_time;           /*!< \brief Head SDU creation time. */
  uint32_t       head_sdu_remaining_size_to_send;  /*!< \brief remaining size of sdu: could be the total size or the remaining size of already segmented sdu */
  boolean_t      head_sdu_is_segmented;     /*!< \brief 0 if head SDU has not been segmented, 1 if already segmented */
} mac_rlc_status_resp_t;


typedef struct {
  unsigned int
  buffer_occupancy_in_bytes;   /*!< \brief the parameter Buffer Occupancy (BO) indicates for each logical channel the amount of data in number of bytes that is available for transmission and retransmission in RLC layer. */
  unsigned short             buffer_occupancy_in_pdus;    /*!< xxx*/
  uint32_t             head_sdu_creation_time;
  uint32_t             head_sdu_remaining_size_to_send;
  unsigned char     head_sdu_is_segmented;
  //struct rlc_entity_info rlc_info;             /*!< xxx*/
}mac_status_resp;

mac_status_resp *rlc_state;




#endif
