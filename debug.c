#include "defs_NB_IoT.h"

extern eNB_MAC_INST_NB_IoT *___mac_inst;

int init_debug(eNB_MAC_INST_NB_IoT *inst){
	if((eNB_MAC_INST_NB_IoT *)0 == inst){
		return 1;
	}
	
	___mac_inst = inst;
	
	return 0;
}
