#include "IF.h"

uint8_t *parse_ulsch_header( uint8_t *mac_header,
                             uint8_t *num_ce,
                             uint8_t *num_sdu,
                             uint8_t *rx_ces,
                             uint8_t *rx_lcids,
                             uint16_t *rx_lengths,
                             uint16_t tb_length ){

uint8_t not_done=1, num_ces=0, num_sdus=0, lcid,num_sdu_cnt;
uint8_t *mac_header_ptr = mac_header;
uint16_t length, ce_len=0;

  while(not_done==1){

    if(((SCH_SUBHEADER_FIXED*)mac_header_ptr)->E == 0){
      not_done = 0;
    }

    lcid = ((SCH_SUBHEADER_FIXED *)mac_header_ptr)->LCID;

    if(lcid < EXTENDED_POWER_HEADROOM){
      if (not_done==0) { // last MAC SDU, length is implicit
        mac_header_ptr++;
        length = tb_length-(mac_header_ptr-mac_header)-ce_len;

        for(num_sdu_cnt=0; num_sdu_cnt < num_sdus ; num_sdu_cnt++){
          length -= rx_lengths[num_sdu_cnt];
        }
      }else{
        if(((SCH_SUBHEADER_SHORT *)mac_header_ptr)->F == 0){
          length = ((SCH_SUBHEADER_SHORT *)mac_header_ptr)->L;
          mac_header_ptr += 2;//sizeof(SCH_SUBHEADER_SHORT);
        }else{ // F = 1
          length = ((((SCH_SUBHEADER_LONG *)mac_header_ptr)->L_MSB & 0x7f ) << 8 ) | (((SCH_SUBHEADER_LONG *)mac_header_ptr)->L_LSB & 0xff);
          mac_header_ptr += 3;//sizeof(SCH_SUBHEADER_LONG);
        }
      }

      rx_lcids[num_sdus] = lcid;
      rx_lengths[num_sdus] = length;
      num_sdus++;
    }else{ // This is a control element subheader POWER_HEADROOM, BSR and CRNTI
      if(lcid == SHORT_PADDING){
        mac_header_ptr++;
      }else{
        rx_ces[num_ces] = lcid;
        num_ces++;
        mac_header_ptr++;

        if(lcid==LONG_BSR){
          ce_len+=3;
        }else if(lcid==CRNTI){
          ce_len+=2;
        }else if((lcid==POWER_HEADROOM) || (lcid==TRUNCATED_BSR)|| (lcid== SHORT_BSR)) {
          ce_len++;
        }else{
          // wrong lcid
        }
      }
    }
  }

  *num_ce = num_ces;
  *num_sdu = num_sdus;

  return(mac_header_ptr);
}

//  calvin
//  maybe we can try to use hash table to enhance searching time.
UE_TEMPLATE_NB_IoT *get_ue_from_rnti(eNB_MAC_INST_NB_IoT *inst, rnti_t rnti){
  uint32_t i;
  for(i=0; i<MAX_NUMBER_OF_UE_MAX_NB_IoT; ++i){
    if(inst->UE_list_spec->UE_template_NB_IoT[i].active == 1){
      if(inst->UE_list_spec->UE_template_NB_IoT[i].rnti == rnti){
        return &inst->UE_list_spec->UE_template_NB_IoT[i];
      }
    }
  }
  return (UE_TEMPLATE_NB_IoT *)0;
}




void NB_IoT_mac_rrc_data_ind(uint8_t *data, eNB_MAC_INST_NB_IoT *mac_inst, uint16_t rnti)
{
  // decode CCCH

  uint8_t *RRC_buffer;
  int i;

  RRC_buffer = data;

  printf("[CCCH]Data PDU : ");

  for(i=0;i<2;i++)
  {
    printf("%x ",RRC_buffer[i]);
  }
  //generate_msg4();
  printf("\n");

}

void NB_IoT_mac_rlc_data_ind(uint8_t *data, eNB_MAC_INST_NB_IoT *mac_inst, uint16_t rnti)
{
  uint8_t *RLC_buffer;

  RLC_buffer = data;
}

void rx_sdu_NB_IoT(module_id_t module_id, int CC_id, frame_t frame, sub_frame_t subframe, uint16_t rnti, uint8_t *sdu, uint16_t  length)
{
	unsigned char  rx_ces[5], num_ce = 0, num_sdu = 0, *payload_ptr, i; // MAX Control element
	unsigned char  rx_lcids[5];//for NB_IoT-IoT, NB_IoT_RB_MAX should be fixed to 5 (2 DRB+ 3SRB) 
  unsigned short rx_lengths[5];
  int UE_id = 0;
  int BSR_index=0;
  int DVI_index = 0;
  int PHR = 0;
  int ul_total_buffer = 0;
  mac_NB_IoT_t *mac_inst;
  UE_TEMPLATE_NB_IoT *UE_info;

  //mac_inst = get_mac_inst(module_id);

  // note: if lcid < 25 this is sdu, otherwise this is CE
  payload_ptr = parse_ulsch_header(sdu, &num_ce, &num_sdu,rx_ces, rx_lcids, rx_lengths, length);

  //printf("num_CE= %d, num_sdu= %d, rx_ces[0] = %d, rx_lcids =  %d, rx_lengths[0] = %d, length = %d\n",num_ce,num_sdu,rx_ces[0],rx_lcids[0],rx_lengths[0],length);

  for (i = 0; i < num_ce; i++)
  {
  	switch(rx_ces[i])
  	{
  		case CRNTI:
  		  // find UE id again, confirm the UE, intial some ue specific parameters
  		  payload_ptr+=2;
  			break;
  		case SHORT_BSR:
  			// update BSR here
        UE_info = get_ue_from_rnti(mac_inst, rnti);
        BSR_index = payload_ptr[0] & 0x3f;
        UE_info->ul_total_buffer = BSR_table[BSR_index];
  			payload_ptr+=1;
  			break;
  		default:
      	printf("Received unknown MAC header (0x%02x)\n", rx_ces[i]);
      			break;
  		}
  	}
  	for (i = 0; i < num_sdu; i++)
  	{
  		switch(rx_lcids[i])
  		{
  			case CCCH_NB_IoT:
  				
  				// MSG3 content: |R|R|PHR|PHR|DVI|DVI|DVI|DVI|CCCH payload
  				PHR = ((payload_ptr[0] >> 5) & 0x01)*2+((payload_ptr[0]>>4) & 0x01);
  				DVI_index = (payload_ptr[0] >>3 & 0x01)*8+ (payload_ptr[0] >>2 & 0x01)*4 + (payload_ptr[0] >>1 & 0x01)*2 +(payload_ptr[0] >>0 & 0x01);
          //printf("DVI_index= %d\n",DVI_index);
  				ul_total_buffer = DV_table[DVI_index];
          printf("PHR = %d, ul_total_buffer = %d\n",PHR,ul_total_buffer);
  				// go to payload
  				payload_ptr+=1; 

          rx_lengths[i]-=1;
          printf("rx_lengths : %d\n", rx_lengths[i]);
  				NB_IoT_mac_rrc_data_ind(payload_ptr,mac_inst,rnti);
  				//NB_IoT_receive_msg3(mac_inst,rnti,PHR,ul_total_buffer);
          break;
  			case DCCH0_NB_IoT:
    		case DCCH1_NB_IoT:
    			// UE specific here
    			NB_IoT_mac_rlc_data_ind(payload_ptr,mac_inst,rnti);
    		
          break;
    		// all the DRBS
    		case DTCH0_NB_IoT:
    		default:
    			NB_IoT_mac_rlc_data_ind(payload_ptr,mac_inst,rnti);
          break;
  		}
  		payload_ptr+=rx_lengths[i];
  	}

   
}


// Sched_INFO as a input for the scheduler
void UL_indication(UL_IND_t *UL_INFO)
{
    int i=0;
    UE_TEMPLATE_NB_IoT *UE_info;
    mac_NB_IoT_t *mac_inst;

      //If there is a preamble, do the initiate RA procedure
      if(UL_INFO->NRACH.number_of_initial_scs_detected>0)
        {
          for(i=0;i<UL_INFO->NRACH.number_of_initial_scs_detected;i++)
            {
            	// initiate_ra here, some useful inforamtion : 
            	//(UL_INFO->NRACH.nrach_pdu_list+i)->nrach_indication_rel13.initial_sc
            	//(UL_INFO->NRACH.nrach_pdu_list+i)->nrach_indication_rel13.timing_advance
            }
        }

        // crc indication if there is error for this round UL transmission

        if(UL_INFO->crc_ind.number_of_crcs>0)
        {
          for(i=0;i<UL_INFO->crc_ind.number_of_crcs;i++)
          {
            if((UL_INFO->crc_ind.crc_pdu_list+i)->crc_indication_rel8.crc_flag == 0)
            {
              //unsuccessfully received this UE PDU
              UE_info = get_ue_from_rnti(mac_inst,((UL_INFO->crc_ind.crc_pdu_list)+i)->rx_ue_information.rnti);
              UE_info->HARQ_round++;
            }
          }
        }

        /*If there is a Uplink SDU which needs to send to MAC*/

        if(UL_INFO->RX_NPUSCH.number_of_pdus>0)
          {
            for(i=0;i<UL_INFO->RX_NPUSCH.number_of_pdus;i++)
              {
                /*For MSG3, Normal Uplink Data, NAK*/
                rx_sdu_NB_IoT(UL_INFO->module_id,
                              UL_INFO->CC_id,
                              UL_INFO->frame,
                              UL_INFO->subframe,
                              (UL_INFO->RX_NPUSCH.rx_pdu_list+i)->rx_ue_information.rnti,
                              (UL_INFO->RX_NPUSCH.rx_pdu_list+i)->data,
                              (UL_INFO->RX_NPUSCH.rx_pdu_list+i)->rx_indication_rel8.length
                             );

              }

          }

    //scheduler here
    printf("Enter scheduler\n");
}

// test function
int main()
{
  UL_IND_t *UL_INFO = (UL_IND_t*) malloc(sizeof(UL_IND_t)) ;

  UL_INFO->module_id = 0;
  UL_INFO->CC_id = 0;
  UL_INFO->frame = 0;
  UL_INFO->subframe = 0;
  UL_INFO->RX_NPUSCH.rx_pdu_list = (nfapi_rx_indication_pdu_t*) malloc (2*sizeof(nfapi_rx_indication_pdu_t));
  UL_INFO->RX_NPUSCH.rx_pdu_list->rx_ue_information.rnti = 1;
  UL_INFO->RX_NPUSCH.rx_pdu_list->rx_indication_rel8.length = 4;

  (UL_INFO->RX_NPUSCH.rx_pdu_list+1)->rx_ue_information.rnti = 2;
  (UL_INFO->RX_NPUSCH.rx_pdu_list+1)->rx_indication_rel8.length = 4;

  uint8_t *data = (uint8_t *) malloc(4*sizeof(uint8_t));

  data[0] = 0x00;
  data[1] = 0x2b;
  data[2] = 0x22;
  data[3] = 0xff;

  uint8_t *data_s = (uint8_t *)malloc(4*sizeof(uint8_t));

  data_s[0] = 0x00;
  data_s[1] = 0xb8;
  data_s[2] = 0x32;
  data_s[3] = 0xfe;

  UL_INFO->RX_NPUSCH.rx_pdu_list->data = data;
  (UL_INFO->RX_NPUSCH.rx_pdu_list+1)->data = data_s;

  UL_INFO->NRACH.number_of_initial_scs_detected = 0;
  UL_INFO->RX_NPUSCH.number_of_pdus = 2;

  UL_indication(UL_INFO);

}
