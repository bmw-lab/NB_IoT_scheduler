#include "../defs_NB_IoT.h"
#include "nfapi_interface.h"

// ULSCH LCHAN IDs
/*!\brief LCID of extended power headroom for ULSCH */
#define EXTENDED_POWER_HEADROOM 25
/*!\brief LCID of power headroom for ULSCH */
#define POWER_HEADROOM 26
/*!\brief LCID of CRNTI for ULSCH */
#define CRNTI 27
/*!\brief LCID of truncated BSR for ULSCH */
#define TRUNCATED_BSR 28
/*!\brief LCID of short BSR for ULSCH */
#define SHORT_BSR 29
/*!\brief LCID of long BSR for ULSCH */
#define LONG_BSR 30

// Downlink subframe P7

typedef struct{

 	//Module ID
	module_id_t module_id; 
 	//CC ID
 	int CC_id;

 	// hypersubframe
 	uint32_t hypersfn;
 	//frame
 	frame_t frame;
 	//subframe
 	sub_frame_t subframe;

  /// nFAPI DL Config Request
  nfapi_dl_config_request_t *DL_req;
  /// nFAPI UL Config Request
  nfapi_ul_config_request_t *UL_req;
  /// nFAPI HI_DCI Request
  nfapi_hi_dci0_request_t *HI_DCI0_req;
  /// nFAPI TX Request
  nfapi_tx_request_t        *TX_req;
  /// Pointers to DL SDUs
  //uint8_t **sdu;

}Sched_Rsp_t;

// uplink subframe P7

typedef struct{

  int test;

  //Module ID
  module_id_t module_id;
  //CC ID
  int CC_id;
  //frame 
  frame_t frame;
  //subframe
  sub_frame_t subframe;

  /*preamble part*/

  nfapi_nrach_indication_body_t NRACH;

  /*Uplink data part*/

  /*indication of the harq feedback*/
  nfapi_nb_harq_indication_t nb_harq_ind;
  /*indication of the uplink data PDU*/
  nfapi_rx_indication_body_t RX_NPUSCH;
  /*crc_indication*/
  nfapi_crc_indication_body_t crc_ind;

 }UL_IND_t;

 // function delaration

 void UL_indication(UL_IND_t *UL_INFO);

 void rx_sdu_NB_IoT(module_id_t module_id, int CC_id, frame_t frame, sub_frame_t subframe, uint16_t rnti, uint8_t *data, uint16_t  length);

